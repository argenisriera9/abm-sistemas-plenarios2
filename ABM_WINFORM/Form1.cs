﻿using ConexionBD;
using Models;
using Services.PersonasServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.SqlServer;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ABM_WINFORM
{
    public partial class Form1 : Form
    {

        private bool edit = false;
        private string IdPersona = null;

        public Form1()
        {


            InitializeComponent();


            GetPersonas();




        }

        private void GetPersonas()
        {

            using (var db = new Conexion_BD())
            {
                List<Personas> personas = db.Personas.ToList();
                dataGridView1.DataSource = personas;
                dataGridView1.Columns["PersonaID"].Visible = false;

            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            if (dataGridView1.SelectedRows.Count > 0)
            {
                edit = true;
                NombreText.Text = dataGridView1.CurrentRow.Cells["Nombre"].Value.ToString();
                CreditoText.Text = dataGridView1.CurrentRow.Cells["CreditoMaximo"].Value.ToString();
                FechaText.Text = dataGridView1.CurrentRow.Cells["FechaNacimiento"].Value.ToString();
                IdPersona = dataGridView1.CurrentRow.Cells["PersonaID"].Value.ToString();


            }

            else
                MessageBox.Show("seleccione una fila por favor");
        }
        private void limpiarForm()
        {
            NombreText.Clear();
            CreditoText.Clear();
            //FechaText.();
            TelefonoText.Clear();

        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            {
                //INSERTAR
                if (edit == false)
                {
                    try
                    {

                        using (var db = new Conexion_BD())
                        {

                            var resultado = validar();
                            var NombreRepetido = ValidarNombreRepetido();
                            if(!NombreRepetido)
                            {

                                if (resultado)
                                {
                                    Personas persona = new Personas();
                                    persona.Nombre = NombreText.Text;
                                    persona.CreditoMaximo = Convert.ToDecimal(CreditoText.Text);
                                    persona.FechaNacimiento = Convert.ToDateTime(FechaText.Text);


                                    var insertar = db.Personas.Add(persona);
                                    if (TelefonoText.Text != "")
                                    {
                                        Telefonos telefono = new Telefonos();
                                        telefono.Telefono = TelefonoText.Text;
                                        telefono.PersonaID = insertar.PersonaID;
                                        db.Telefonos.Add(telefono);
                                    }

                                    db.SaveChanges();
                                    GetPersonas();
                                    limpiarForm();
                                    MessageBox.Show("se inserto correctamente");
                                }
                                else
                                {
                                    if (NombreText.Text == "")
                                    {
                                        MessageBox.Show("Debe Ingresar un nombre");
                                    }
                                    else
                                    {
                                        MessageBox.Show("Debe Ingresar Credito Maximo");
                                    }
                                }
                                

                            }
                            else
                            {
                                MessageBox.Show("El Nombre Ingresado ya existe");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("no se pudo insertar los datos por: " + ex);
                    }
                }
                //EDITAR
                if (edit == true)
                {
                    try
                    {
                        using (var db = new Conexion_BD())
                        {
                            Personas persona = new Personas();
                            int id = Convert.ToInt32(IdPersona);
                            persona.Nombre = NombreText.Text;
                            persona.CreditoMaximo = Convert.ToDecimal(CreditoText.Text);
                            persona.FechaNacimiento = Convert.ToDateTime(FechaText.Text);
                            persona.PersonaID = id;
                            db.Entry(persona).State = System.Data.Entity.EntityState.Modified;

                            db.SaveChanges();
                            GetPersonas();
                            limpiarForm();
                            MessageBox.Show("Se Ingreso Correctamente");

                            edit = false;

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("no se pudo editar los datos por: " + ex);
                    }
                }
            }

        }

        bool validar()
        {
            if (NombreText.Text == "" || CreditoText.Text == "")
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        bool ValidarNombreRepetido()
        {
            bool name = true;
            using (var db = new Conexion_BD())
            {
                var result = db.Personas.Where(x => x.Nombre == NombreText.Text).ToList() ;
                if (result.Count==0)
                {
                    name = false;
                }
                return name;
            }
        }

        public void MostrarTelefonos()
        {
            using (var db = new Conexion_BD())
            {

                IdPersona = dataGridView1.CurrentRow.Cells["PersonaID"].Value.ToString();
              
                int  id = Convert.ToInt32(IdPersona);
                List<Telefonos>  telefonos = db.Telefonos.Where(x => x.PersonaID == id).ToList();


                dataGridView2.DataSource = telefonos;
                dataGridView2.Columns["TelefonoID"].Visible = false;
                dataGridView2.Columns["PersonaID"].Visible = false;
                dataGridView2.Columns["Persona"].Visible = false;
            }
        }

        private void AgregarTel_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
               


                    using (var db = new Conexion_BD())
                    {
                    if (TelefonoText.Text != "")
                     {
                        IdPersona = dataGridView1.CurrentRow.Cells["PersonaID"].Value.ToString();
                        int id = Convert.ToInt32(IdPersona);
                        Telefonos telefonos = new Telefonos();
                        telefonos.Telefono = TelefonoText.Text;
                        telefonos.PersonaID = Convert.ToInt32(id);
                        db.Telefonos.Add(telefonos);
                        db.SaveChanges();
                        limpiarForm();
                        MostrarTelefonos();
                        MessageBox.Show("Se Ingreso Correctamente");

                    }
                    else
                    {
                        MessageBox.Show("Ingrese un telefono");
                    }


                }
                
            }
            else
            {
                MessageBox.Show("seleccione una fila por favor");
            }


        }

        private void VerTelefono_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                MostrarTelefonos();

            }
            else
            {
                MessageBox.Show("Por favor selecionar una fila");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                
                using (var db = new Conexion_BD())
                {
                    IdPersona = dataGridView1.CurrentRow.Cells["PersonaID"].Value.ToString();
                    int id = Convert.ToInt32(IdPersona);
                    var result = db.Personas.Find(id);
                    db.Personas.Remove(result);
                    List<Telefonos> deteleTelefonos = db.Telefonos.Where(x => x.PersonaID == id).ToList();
                    foreach(var d in deteleTelefonos)
                    {
                        db.Telefonos.Remove(d);
                       
                    }


                    db.SaveChanges();


                    dataGridView2.DataSource = "";
                    GetPersonas();
                
                    MessageBox.Show("Registro Eliminado");
                }
            }

            }

        private void EliminarTelefono_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count > 0)
            {
                using (var db =  new Conexion_BD())
                {
                   var IdTelefono = dataGridView2.CurrentRow.Cells["TelefonoID"].Value.ToString();
                    int id = Convert.ToInt32(IdTelefono);
                    var result = db.Telefonos.Find(id);
                    db.Telefonos.Remove(result);
                    db.SaveChanges();
                    MostrarTelefonos();
                    MessageBox.Show("Registro Eliminado");
                }
            }
            else
            {
                MessageBox.Show("seleccione una fila por favor");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count > 0)
            {
                using (var db = new Conexion_BD())
                {
                    if (TelefonoText.Text != "")
                    {



                        var IdTelefono = dataGridView2.CurrentRow.Cells["TelefonoID"].Value.ToString();
                        int id = Convert.ToInt32(IdTelefono);
                        Telefonos telefonos = new Telefonos();
                        telefonos.Telefono = TelefonoText.Text;
                        telefonos.TelefonoID = id;
                        db.Entry(telefonos).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        MostrarTelefonos();
                        MessageBox.Show("Se edito correctamente");
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un telefono");
                    }

                }

            }
        }

        private void Buscar_Click(object sender, EventArgs e)
        {
            if (BuscarNombre.Text!="")
            {
                using (var db = new Conexion_BD())
                {
                    var result = (from c in db.Personas
                                  where c.Nombre.Contains(BuscarNombre.Text)
                                  select c).ToList();

                    dataGridView1.DataSource = result;

                }
            }
            else
            {
                MessageBox.Show("Introduzca un nombre");
            }
        }


       
    }
}

    

