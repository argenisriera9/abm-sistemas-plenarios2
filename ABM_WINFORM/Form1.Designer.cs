﻿namespace ABM_WINFORM
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.NombreText = new System.Windows.Forms.TextBox();
            this.CreditoText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Guardar = new System.Windows.Forms.Button();
            this.VerTelefono = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.AgregarTel = new System.Windows.Forms.Button();
            this.TelefonoText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.EliminarPersona = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.FechaText = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.Buscar = new System.Windows.Forms.Button();
            this.BuscarNombre = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 226);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(437, 279);
            this.dataGridView1.TabIndex = 0;
            // 
            // NombreText
            // 
            this.NombreText.Location = new System.Drawing.Point(943, 52);
            this.NombreText.Name = "NombreText";
            this.NombreText.Size = new System.Drawing.Size(126, 20);
            this.NombreText.TabIndex = 1;
            // 
            // CreditoText
            // 
            this.CreditoText.Location = new System.Drawing.Point(943, 88);
            this.CreditoText.Name = "CreditoText";
            this.CreditoText.Size = new System.Drawing.Size(126, 20);
            this.CreditoText.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(864, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 4;
            this.label1.Tag = "";
            this.label1.Text = "Nombre";
            this.label1.UseMnemonic = false;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(836, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Credito Máxmo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(816, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Fecha Nacimiento";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(943, 219);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Editar Persona";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Guardar
            // 
            this.Guardar.Location = new System.Drawing.Point(943, 190);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(126, 23);
            this.Guardar.TabIndex = 9;
            this.Guardar.Text = "Ingresar Persona";
            this.Guardar.UseVisualStyleBackColor = true;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // VerTelefono
            // 
            this.VerTelefono.Location = new System.Drawing.Point(943, 277);
            this.VerTelefono.Name = "VerTelefono";
            this.VerTelefono.Size = new System.Drawing.Size(126, 23);
            this.VerTelefono.TabIndex = 10;
            this.VerTelefono.Text = "Ver Telefonos";
            this.VerTelefono.UseVisualStyleBackColor = true;
            this.VerTelefono.Click += new System.EventHandler(this.VerTelefono_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(479, 226);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(232, 279);
            this.dataGridView2.TabIndex = 11;
            // 
            // AgregarTel
            // 
            this.AgregarTel.Location = new System.Drawing.Point(943, 306);
            this.AgregarTel.Name = "AgregarTel";
            this.AgregarTel.Size = new System.Drawing.Size(126, 23);
            this.AgregarTel.TabIndex = 12;
            this.AgregarTel.Text = "AgregarTelefono";
            this.AgregarTel.UseVisualStyleBackColor = true;
            this.AgregarTel.Click += new System.EventHandler(this.AgregarTel_Click);
            // 
            // TelefonoText
            // 
            this.TelefonoText.Location = new System.Drawing.Point(943, 153);
            this.TelefonoText.Name = "TelefonoText";
            this.TelefonoText.Size = new System.Drawing.Size(126, 20);
            this.TelefonoText.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(873, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Telefono";
            // 
            // EliminarPersona
            // 
            this.EliminarPersona.Location = new System.Drawing.Point(943, 248);
            this.EliminarPersona.Name = "EliminarPersona";
            this.EliminarPersona.Size = new System.Drawing.Size(126, 23);
            this.EliminarPersona.TabIndex = 15;
            this.EliminarPersona.Text = "Eliminar Persona";
            this.EliminarPersona.UseVisualStyleBackColor = true;
            this.EliminarPersona.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(943, 364);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(126, 23);
            this.button4.TabIndex = 16;
            this.button4.Text = "Eliminar Telefono";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.EliminarTelefono_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(943, 335);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(126, 23);
            this.button5.TabIndex = 17;
            this.button5.Text = "Editar Teléfono";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // FechaText
            // 
            this.FechaText.Location = new System.Drawing.Point(943, 123);
            this.FechaText.Name = "FechaText";
            this.FechaText.Size = new System.Drawing.Size(126, 20);
            this.FechaText.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(263, 33);
            this.label5.TabIndex = 30;
            this.label5.Text = "Listado de Personas";
            // 
            // Buscar
            // 
            this.Buscar.Location = new System.Drawing.Point(179, 514);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(75, 21);
            this.Buscar.TabIndex = 31;
            this.Buscar.Text = "button2";
            this.Buscar.UseVisualStyleBackColor = true;
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // BuscarNombre
            // 
            this.BuscarNombre.Location = new System.Drawing.Point(73, 514);
            this.BuscarNombre.Name = "BuscarNombre";
            this.BuscarNombre.Size = new System.Drawing.Size(100, 20);
            this.BuscarNombre.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 514);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 33;
            this.label6.Text = "Nombre";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 723);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BuscarNombre);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FechaText);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.EliminarPersona);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TelefonoText);
            this.Controls.Add(this.AgregarTel);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.VerTelefono);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreditoText);
            this.Controls.Add(this.NombreText);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox NombreText;
        private System.Windows.Forms.TextBox CreditoText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Button VerTelefono;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button AgregarTel;
        private System.Windows.Forms.TextBox TelefonoText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button EliminarPersona;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DateTimePicker FechaText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Buscar;
        private System.Windows.Forms.TextBox BuscarNombre;
        private System.Windows.Forms.Label label6;
    }
}

