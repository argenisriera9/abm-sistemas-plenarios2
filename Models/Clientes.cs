﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Clientes
    {
        [Key]
        public int IdCliente { get; set; }
        public string RazonSocial { get; set; }
        [StringLength(250)]
        public string Direccion { get; set; }
        [Required]
        public string EmailCliente { get; set; }

        public int IdEmpresa{ get; set; }
        public string Telefono { get; set; }

        public string CodigoCliente { get; set; }

        public int Activo { get; set; }
        public string NumeroDocumento { get; set; }
        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }
        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }
         [ForeignKey("IdEmpresa")]
        public Empresa empresa { get; set; }

      




    }
}
