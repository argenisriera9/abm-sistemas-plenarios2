﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Requerimiento
    {
        [Key]
        public int IdRequerimiento { get; set; }
        public string CodigoRequerimiento { get; set; }

        public DateTime FechaRequerimiento { get; set; }

        public DateTime FechaEntrega { get; set; }
        public int IdProveedor { get; set; }
        public int IdMoneda { get; set; }
        public int Estado { get; set; }

        public string LugarEntrega { get; set; }

        public int FormaPago { get; set; }
        public string Observacion { get; set; }
        public decimal Subtotal { get; set; }

        public decimal Impuesto { get; set; }

        public decimal Total { get; set; }

       
        public DateTime ? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }
        public string UsuarioCreacion { get; set; }

        public string UsuarioModifcio { get; set; }

        public string UsuarioElimino { get; set; }
        [ForeignKey("IdRequerimiento")]
        public List<RequerimientoDetalle>  RequerimienodDet { get; set; }
        [ForeignKey("IdProveedor")]
        public Proveedor proveedor { get; set; }
        [ForeignKey("IdMoneda")]
        public Moneda moneda { get; set; }

    }
}
