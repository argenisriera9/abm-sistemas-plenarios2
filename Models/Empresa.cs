﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public  class Empresa
    {

        [Key]
        public int  IdEmpresa { get; set; }

        public bool EsPrincipal { get; set; }

        public string Nombre { get; set; }
        public string NumeroDocumento { get; set; }
        public string Correo { get; set; }

        public string Web { get;set; }

        public string Telefono { get; set; }
       
        public string Direccion { get; set; }
        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }
        
        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }

        public string Logo { get; set; }
        public int Activo { get; set; }
        
       

      


    }
}
