﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public  class Productos
    {
        [Key]
        public int IdProducto { get; set; }
        public string CodigoProducto { get; set; }
        public int Item { get; set; }
         public int IdEmpresa { get; set; }
        public string NombreProducto { get; set; }
       
        public decimal Stock { get; set; }
        public decimal StockMinimo { get; set; }
        public decimal StockMaximo { get; set; }

        public decimal Precio1 { get; set; }

        public decimal Precio2 { get; set; }
        public decimal Precio3 { get; set; }
        public decimal IVA { get; set; }
        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }

        public int Seleccionado { get; set; }
        public int Activo { get; set; }
        [ForeignKey("IdEmpresa")]
        public Empresa empresa { get; set; }
     



    }
}
