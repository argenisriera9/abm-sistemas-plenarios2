﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Usuario
    {
        [Key]
        public int IdUsuario { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
       
        public string Nombre { get; set; }
        public int IdRol  { get; set; }
        public string Apellido { get; set; }
        public string Token { get; set; }

        public int Flag { get; set; }
        [ForeignKey("IdUsuario")]
        public List<EstadoDespacho> despachos { get; set; }

        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }
        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }
        [ForeignKey("IdRol")]
        public UsuarioRol UsuarioRol { get; set; }

        public int Activo { get; set; }

        public string Photo { get; set; }




    }

}
