﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
  public   class RequerimientoDetalle
    {
        [Key]
        public int IdRequerimientoDetalle { get; set; }

        public int IdRequerimiento  { get; set; }
        public int IdProducto { get; set; }
        public string CodigoProducto { get; set; }

        public decimal Cantidad { get; set; }

        public decimal PrecioUnitario { get; set; }

        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public DateTime?  FechaEliminacion { get; set; }

        public string UsuarioCreacion { get; set; }

        public string UsuarioModifica { get; set; }

        public string UsuarioElimina { get; set; }
        
        [ForeignKey("IdProducto")]
        public Productos Producto { get; set; }

    }
}
