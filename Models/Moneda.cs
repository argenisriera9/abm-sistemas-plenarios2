﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Moneda
    {
        [Key]
        public int IdMoneda { get; set; }

        public int IdEmpresa { get; set; }
        public string NombreMoneda { get; set; }

        public string Abreviatura { get; set; }

        public int Activo { get; set; }

        public string UsuarioModifico { get; set; }

        public string UsuarioRegistro { get; set; }

        public string UsuarioElimino { get; set; }

        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaEliminacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
    }
}
