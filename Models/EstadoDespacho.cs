﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class  EstadoDespacho
    {[Key]
        public int IdEstadoDespacho{ get; set; }
        public int IdProducto { get; set; }
        public int IdEstado{ get; set; }

        public DateTime Recibido { get; set; }

        public  DateTime Salida { get; set; }

        public int IdCliente { get; set; }
        public int IdUsuario { get; set; }

        public string Observacion { get; set; }

        [ForeignKey("IdEstado")]
        public EstadoProductos EstadoProductos { get; set; }

        [ForeignKey("IdProducto")]
        public Productos Productos { get; set; }

        [ForeignKey("IdCliente")]
        public Clientes Clientes { get; set; }

        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }
        public int Activo { get; set; }





    }
}
