﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UsuarioRol
    {
        
        [Key]
        public int IdRol { get; set; } 
        public  string NombreRol  {get;set;}
        public int Activo { get; set; }

        public DateTime ? FechaRegsitro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime ? FechaEliminacion { get; set; }
     
    }
}
