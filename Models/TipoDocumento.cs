﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public  class TipoDocumento
    {
        [Key]
        public int IdTipoDocumento { get; set; }
        public string Nombre { get; set; }

        public string Abreviatura { get; set; }

        public int Activo { get; set; }
    }
}
