﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Personas
    {
        [Key]
        public int PersonaID { get; set; }
        [StringLength(50)]
        public string Nombre { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public decimal CreditoMaximo  { get; set; }

        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }
        public DateTime FechaRegistro { get; set; }

        public DateTime FechaModificacion { get; set; }

        public DateTime FechaEliminacion { get; set; }
        
    }

}
