﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public  class Proveedor
    {
        [Key]
        public int IdProveedor { get; set; }

        public string Codigo { get;set; }

        public string Direccion { get; set; }
        public int IdTipoDocumento { get; set; }
        public int IdEmpresa { get; set; }
        public string NumeroDocumento { get; set; }

        public  string NombreProveedor { get; set; }
       
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }

        public string  Correo { get; set; }

        public string WebSite { get; set; }

        public int IdTipoContribuyente { get; set; }

        public  bool FlagActivo { get; set; }

        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }
        public string Telefono { get; set; }
        

        public DateTime? FechaEliminacion { get; set; }
      
        public int Activo { get; set; }
        [ForeignKey("IdEmpresa")]
        public Empresa empresa { get; set; }
        //[ForeignKey("IdTipoDocumento")]
        //public TipoDocumento TipoDocumento { get; set; }

    }
}
