﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Telefonos
    {
        [Key]
        public int TelefonoID { get; set; }
        [StringLength(50)]
        public string Telefono { get; set; }

        public int PersonaID { get; set; }
        [ForeignKey("PersonaID")]
        public Personas persona { get; set; }
    }
}
