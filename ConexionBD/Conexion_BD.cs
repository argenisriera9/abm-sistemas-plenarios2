﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace ConexionBD
{
    public class Conexion_BD : DbContext
    {
        public Conexion_BD() : base("name=Default")
        {

        }


        public DbSet<Personas> Personas { get; set; }

        public DbSet<Telefonos> Telefonos { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<UsuarioRol> UsuarioRol {get;set;}

        public DbSet<Productos> Productos { get; set; }

        public DbSet<Clientes> Clientes { get; set; }

        public DbSet<EstadoProductos> EstadoProductos { get; set; }

        public DbSet<EstadoDespacho>  EstadoDespacho { get; set; }

        public DbSet<Proveedor> Proveedores { get; set; }

        public DbSet<Empresa> Empresa { get; set; }

        public DbSet<TipoDocumento> TipoDocumento { get; set; }
        public DbSet<Requerimiento> Requerimiento { get; set; }

        public DbSet<RequerimientoDetalle> RequerimientoDet { get; set; }

        public DbSet<UnidadMedida>  unidadMedida { get; set; }

        public DbSet<Moneda> Moneda { get; set; }
    }
}
