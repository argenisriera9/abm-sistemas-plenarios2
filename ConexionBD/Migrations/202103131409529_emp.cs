﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Proveedors", "IdProveedor", "dbo.Productos");
            DropIndex("dbo.Proveedors", new[] { "IdProveedor" });
            DropPrimaryKey("dbo.Proveedors");
            AddColumn("dbo.Productos", "IdEmpresa", c => c.Int(nullable: false));
            AlterColumn("dbo.Proveedors", "IdProveedor", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Proveedors", "IdProveedor");
            CreateIndex("dbo.Productos", "IdEmpresa");
            CreateIndex("dbo.Productos", "IdProveedor");
            AddForeignKey("dbo.Productos", "IdEmpresa", "dbo.Empresas", "IdEmpresa", cascadeDelete: false);
            AddForeignKey("dbo.Productos", "IdProveedor", "dbo.Proveedors", "IdProveedor", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productos", "IdProveedor", "dbo.Proveedors");
            DropForeignKey("dbo.Productos", "IdEmpresa", "dbo.Empresas");
            DropIndex("dbo.Productos", new[] { "IdProveedor" });
            DropIndex("dbo.Productos", new[] { "IdEmpresa" });
            DropPrimaryKey("dbo.Proveedors");
            AlterColumn("dbo.Proveedors", "IdProveedor", c => c.Int(nullable: false));
            DropColumn("dbo.Productos", "IdEmpresa");
            AddPrimaryKey("dbo.Proveedors", "IdProveedor");
            CreateIndex("dbo.Proveedors", "IdProveedor");
            AddForeignKey("dbo.Proveedors", "IdProveedor", "dbo.Productos", "IdProducto");
        }
    }
}
