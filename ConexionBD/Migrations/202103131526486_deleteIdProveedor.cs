﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deleteIdProveedor : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Productos", "IdProveedor", "dbo.Proveedors");
            DropIndex("dbo.Productos", new[] { "IdProveedor" });
            DropColumn("dbo.Productos", "IdProveedor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productos", "IdProveedor", c => c.Int(nullable: false));
            CreateIndex("dbo.Productos", "IdProveedor");
            AddForeignKey("dbo.Productos", "IdProveedor", "dbo.Proveedors", "IdProveedor", cascadeDelete: true);
        }
    }
}
