﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class proveedorfinal : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Proveedors");
            AddColumn("dbo.Proveedors", "IdProveedor", c => c.Int(nullable: false, identity: false));
            AddPrimaryKey("dbo.Proveedors", "IdProveedor");
            DropColumn("dbo.Proveedors", "Id");
            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Proveedors", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Proveedors");
            DropColumn("dbo.Proveedors", "IdProveedor");
            AddPrimaryKey("dbo.Proveedors", "Id");
        }
    }
}
