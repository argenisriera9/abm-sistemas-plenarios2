﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productoid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RequerimientoDetalles", "IdProducto", c => c.Int(nullable: false));
            CreateIndex("dbo.RequerimientoDetalles", "IdProducto");
            AddForeignKey("dbo.RequerimientoDetalles", "IdProducto", "dbo.Productos", "IdProducto", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequerimientoDetalles", "IdProducto", "dbo.Productos");
            DropIndex("dbo.RequerimientoDetalles", new[] { "IdProducto" });
            DropColumn("dbo.RequerimientoDetalles", "IdProducto");
        }
    }
}
