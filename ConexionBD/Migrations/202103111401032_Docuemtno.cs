﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Docuemtno : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empresas", "NumeroDocumento", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empresas", "NumeroDocumento");
        }
    }
}
