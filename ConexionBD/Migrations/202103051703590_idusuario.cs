﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idusuario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EstadoDespachoes", "IdUsuario", c => c.Int(nullable: true));
            AddColumn("dbo.Usuarios", "Flag", c => c.Boolean(nullable: false));
            CreateIndex("dbo.EstadoDespachoes", "IdUsuario");
            AddForeignKey("dbo.EstadoDespachoes", "IdUsuario", "dbo.Usuarios", "IdUsuario", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EstadoDespachoes", "IdUsuario", "dbo.Usuarios");
            DropIndex("dbo.EstadoDespachoes", new[] { "IdUsuario" });
            DropColumn("dbo.Usuarios", "Flag");
            DropColumn("dbo.EstadoDespachoes", "IdUsuario");
        }
    }
}
