﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class d : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Proveedors", "IdEmpresa");
            AddForeignKey("dbo.Proveedors", "IdEmpresa", "dbo.Empresas", "IdEmpresa", cascadeDelete: true);
            DropColumn("dbo.Productos", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productos", "EmpresaId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Proveedors", "IdEmpresa", "dbo.Empresas");
            DropIndex("dbo.Proveedors", new[] { "IdEmpresa" });
        }
    }
}
