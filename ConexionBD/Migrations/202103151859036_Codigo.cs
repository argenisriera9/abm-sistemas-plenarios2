﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Codigo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Proveedors", "Codigo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Proveedors", "Codigo");
        }
    }
}
