﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _null : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Empresas", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.Empresas", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.Empresas", "FechaEliminacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Empresas", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Empresas", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Empresas", "FechaRegistro", c => c.DateTime(nullable: false));
        }
    }
}
