﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Roles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuarios", "IdRol", c => c.Int(nullable: false));
            DropColumn("dbo.Clientes", "IdEmpresa");
            DropColumn("dbo.EstadoDespachoes", "IdEmpresa");
            DropColumn("dbo.EstadoProductos", "IdEmpresa");
            DropColumn("dbo.Productos", "IdEmpresa");
            DropColumn("dbo.Personas", "IdEmpresa");
            DropColumn("dbo.Proveedors", "IdEmpresa");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Proveedors", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Personas", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Productos", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.EstadoProductos", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.EstadoDespachoes", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Clientes", "IdEmpresa", c => c.Int(nullable: false));
            DropColumn("dbo.Usuarios", "IdRol");
        }
    }
}
