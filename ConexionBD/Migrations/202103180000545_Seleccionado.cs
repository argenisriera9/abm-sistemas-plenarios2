﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seleccionado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productos", "Seleccionado", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productos", "Seleccionado");
        }
    }
}
