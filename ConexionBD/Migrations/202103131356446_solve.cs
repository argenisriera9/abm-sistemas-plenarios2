﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class solve : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Clientes");
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Productos");
            DropIndex("dbo.Empresas", new[] { "IdEmpresa" });
            DropPrimaryKey("dbo.Empresas");
            AddColumn("dbo.Productos", "EmpresaId", c => c.Int(nullable: false));
            AlterColumn("dbo.Empresas", "IdEmpresa", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Empresas", "IdEmpresa");
            CreateIndex("dbo.Clientes", "IdEmpresa");
            AddForeignKey("dbo.Clientes", "IdEmpresa", "dbo.Empresas", "IdEmpresa", cascadeDelete: true);
            DropColumn("dbo.Productos", "IdEmpresa");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productos", "IdEmpresa", c => c.Int(nullable: false));
            DropForeignKey("dbo.Clientes", "IdEmpresa", "dbo.Empresas");
            DropIndex("dbo.Clientes", new[] { "IdEmpresa" });
            DropPrimaryKey("dbo.Empresas");
            AlterColumn("dbo.Empresas", "IdEmpresa", c => c.Int(nullable: false));
            DropColumn("dbo.Productos", "EmpresaId");
            AddPrimaryKey("dbo.Empresas", "IdEmpresa");
            CreateIndex("dbo.Empresas", "IdEmpresa");
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Productos", "IdProducto");
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Clientes", "IdCliente");
        }
    }
}
