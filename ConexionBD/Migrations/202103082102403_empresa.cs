﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class empresa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Proveedors",
                c => new
                    {
                        IdProveedor = c.Int(nullable: false, identity:true),
                        IdTipoDocumento = c.Int(nullable: false),
                        NumeroDocumento = c.String(),
                        NombreProveedor = c.String(),
                        ApellidoPaterno = c.String(),
                        ApellidoMaterno = c.String(),
                        Correo = c.String(),
                        WebSite = c.String(),
                        IdTipoContribuyente = c.Int(nullable: false),
                        FlagActivo = c.Boolean(nullable: false),
                        UsuarioElimino = c.String(),
                        UsuarioActivo = c.String(),
                        UsuarioCreo = c.String(),
                        UsuarioEdito = c.String(),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        Telefono = c.String(),
                        IdEmpresa = c.Int(nullable: false),
                        FechaEliminacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdProveedor)
                .ForeignKey("dbo.Productos", t => t.IdProveedor)
                .Index(t => t.IdProveedor);
            
            AddColumn("dbo.Clientes", "IdEmpresa", c => c.Int(nullable: true));
            AddColumn("dbo.Clientes", "UsuarioElimino", c => c.String());
            AddColumn("dbo.Clientes", "UsuarioActivo", c => c.String());
            AddColumn("dbo.Clientes", "UsuarioCreo", c => c.String());
            AddColumn("dbo.Clientes", "UsuarioEdito", c => c.String());
            AddColumn("dbo.Clientes", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clientes", "FechaModificacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clientes", "FechaEliminacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoDespachoes", "UsuarioElimino", c => c.String());
            AddColumn("dbo.EstadoDespachoes", "UsuarioActivo", c => c.String());
            AddColumn("dbo.EstadoDespachoes", "UsuarioCreo", c => c.String());
            AddColumn("dbo.EstadoDespachoes", "UsuarioEdito", c => c.String());
            AddColumn("dbo.EstadoDespachoes", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoDespachoes", "FechaModificacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoDespachoes", "FechaEliminacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoDespachoes", "IdEmpresa", c => c.Int(nullable: true));
            AddColumn("dbo.EstadoProductos", "UsuarioElimino", c => c.String());
            AddColumn("dbo.EstadoProductos", "UsuarioActivo", c => c.String());
            AddColumn("dbo.EstadoProductos", "UsuarioCreo", c => c.String());
            AddColumn("dbo.EstadoProductos", "UsuarioEdito", c => c.String());
            AddColumn("dbo.EstadoProductos", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoProductos", "FechaModificacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoProductos", "FechaEliminacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.EstadoProductos", "IdEmpresa", c => c.Int(nullable: true));
            AddColumn("dbo.Productos", "IdProveedor", c => c.Int(nullable: false));
            AddColumn("dbo.Productos", "Stock", c => c.String());
            AddColumn("dbo.Productos", "StockMinimo", c => c.String());
            AddColumn("dbo.Productos", "StockMaximo", c => c.String());
            AddColumn("dbo.Productos", "Precio1", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Productos", "Precio2", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Productos", "Precio3", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Productos", "IVA", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Productos", "UsuarioElimino", c => c.String());
            AddColumn("dbo.Productos", "UsuarioActivo", c => c.String());
            AddColumn("dbo.Productos", "UsuarioCreo", c => c.String());
            AddColumn("dbo.Productos", "UsuarioEdito", c => c.String());
            AddColumn("dbo.Productos", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.Productos", "FechaModificacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Productos", "FechaEliminacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Productos", "IdEmpresa", c => c.Int(nullable: true));
            AddColumn("dbo.Personas", "UsuarioElimino", c => c.String());
            AddColumn("dbo.Personas", "UsuarioActivo", c => c.String());
            AddColumn("dbo.Personas", "UsuarioCreo", c => c.String());
            AddColumn("dbo.Personas", "UsuarioEdito", c => c.String());
            AddColumn("dbo.Personas", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.Personas", "FechaModificacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Personas", "FechaEliminacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Personas", "IdEmpresa", c => c.Int(nullable: true));
            AddColumn("dbo.Usuarios", "UsuarioElimino", c => c.String());
            AddColumn("dbo.Usuarios", "UsuarioActivo", c => c.String());
            AddColumn("dbo.Usuarios", "UsuarioCreo", c => c.String());
            AddColumn("dbo.Usuarios", "UsuarioEdito", c => c.String());
            AddColumn("dbo.Usuarios", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.Usuarios", "FechaModificacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Usuarios", "FechaEliminacion", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proveedors", "IdProveedor", "dbo.Productos");
            DropIndex("dbo.Proveedors", new[] { "IdProveedor" });
            DropColumn("dbo.Usuarios", "FechaEliminacion");
            DropColumn("dbo.Usuarios", "FechaModificacion");
            DropColumn("dbo.Usuarios", "FechaRegistro");
            DropColumn("dbo.Usuarios", "UsuarioEdito");
            DropColumn("dbo.Usuarios", "UsuarioCreo");
            DropColumn("dbo.Usuarios", "UsuarioActivo");
            DropColumn("dbo.Usuarios", "UsuarioElimino");
            DropColumn("dbo.Personas", "IdEmpresa");
            DropColumn("dbo.Personas", "FechaEliminacion");
            DropColumn("dbo.Personas", "FechaModificacion");
            DropColumn("dbo.Personas", "FechaRegistro");
            DropColumn("dbo.Personas", "UsuarioEdito");
            DropColumn("dbo.Personas", "UsuarioCreo");
            DropColumn("dbo.Personas", "UsuarioActivo");
            DropColumn("dbo.Personas", "UsuarioElimino");
            DropColumn("dbo.Productos", "IdEmpresa");
            DropColumn("dbo.Productos", "FechaEliminacion");
            DropColumn("dbo.Productos", "FechaModificacion");
            DropColumn("dbo.Productos", "FechaRegistro");
            DropColumn("dbo.Productos", "UsuarioEdito");
            DropColumn("dbo.Productos", "UsuarioCreo");
            DropColumn("dbo.Productos", "UsuarioActivo");
            DropColumn("dbo.Productos", "UsuarioElimino");
            DropColumn("dbo.Productos", "IVA");
            DropColumn("dbo.Productos", "Precio3");
            DropColumn("dbo.Productos", "Precio2");
            DropColumn("dbo.Productos", "Precio1");
            DropColumn("dbo.Productos", "StockMaximo");
            DropColumn("dbo.Productos", "StockMinimo");
            DropColumn("dbo.Productos", "Stock");
            DropColumn("dbo.Productos", "IdProveedor");
            DropColumn("dbo.EstadoProductos", "IdEmpresa");
            DropColumn("dbo.EstadoProductos", "FechaEliminacion");
            DropColumn("dbo.EstadoProductos", "FechaModificacion");
            DropColumn("dbo.EstadoProductos", "FechaRegistro");
            DropColumn("dbo.EstadoProductos", "UsuarioEdito");
            DropColumn("dbo.EstadoProductos", "UsuarioCreo");
            DropColumn("dbo.EstadoProductos", "UsuarioActivo");
            DropColumn("dbo.EstadoProductos", "UsuarioElimino");
            DropColumn("dbo.EstadoDespachoes", "IdEmpresa");
            DropColumn("dbo.EstadoDespachoes", "FechaEliminacion");
            DropColumn("dbo.EstadoDespachoes", "FechaModificacion");
            DropColumn("dbo.EstadoDespachoes", "FechaRegistro");
            DropColumn("dbo.EstadoDespachoes", "UsuarioEdito");
            DropColumn("dbo.EstadoDespachoes", "UsuarioCreo");
            DropColumn("dbo.EstadoDespachoes", "UsuarioActivo");
            DropColumn("dbo.EstadoDespachoes", "UsuarioElimino");
            DropColumn("dbo.Clientes", "FechaEliminacion");
            DropColumn("dbo.Clientes", "FechaModificacion");
            DropColumn("dbo.Clientes", "FechaRegistro");
            DropColumn("dbo.Clientes", "UsuarioEdito");
            DropColumn("dbo.Clientes", "UsuarioCreo");
            DropColumn("dbo.Clientes", "UsuarioActivo");
            DropColumn("dbo.Clientes", "UsuarioElimino");
            DropColumn("dbo.Clientes", "IdEmpresa");
            DropTable("dbo.Proveedors");
        }
    }
}
