﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rebuildreque : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Requerimientoes",
                c => new
                    {
                        IdRequerimiento = c.Int(nullable: false, identity: true),
                        CodigoRequerimiento = c.String(),
                        FechaRequerimiento = c.DateTime(nullable: false),
                        FechaEntrega = c.DateTime(nullable: false),
                        IdProveedor = c.Int(nullable: false),
                        IdMoneda = c.Int(nullable: false),
                        Estado = c.Int(nullable: false),
                        LugarEntrega = c.String(),
                        FormaPago = c.Int(nullable: false),
                        Observacion = c.String(),
                        Subtotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Impuesto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Moneda = c.Int(nullable: false),
                        FechaCreacion = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        FechaEliminacion = c.DateTime(),
                        UsuarioCreacion = c.String(),
                        UsuarioModifcio = c.String(),
                        UsuarioElimino = c.String(),
                    })
                .PrimaryKey(t => t.IdRequerimiento)
                .ForeignKey("dbo.Monedas", t => t.IdMoneda, cascadeDelete: true)
                .ForeignKey("dbo.Proveedors", t => t.IdProveedor, cascadeDelete: true)
                .Index(t => t.IdProveedor)
                .Index(t => t.IdMoneda);
            
            CreateTable(
                "dbo.RequerimientoDetalles",
                c => new
                    {
                        IdRequerimientoDetalle = c.Int(nullable: false, identity: true),
                        IdRequerimiento = c.Int(nullable: false),
                        CodigoProducto = c.String(),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FechaCreacion = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        FechaEliminacion = c.DateTime(),
                        UsuarioCreacion = c.String(),
                        UsuarioModifica = c.String(),
                        UsuarioElimina = c.String(),
                    })
                .PrimaryKey(t => t.IdRequerimientoDetalle)
                .ForeignKey("dbo.Requerimientoes", t => t.IdRequerimiento, cascadeDelete: true)
                .Index(t => t.IdRequerimiento);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequerimientoDetalles", "IdRequerimiento", "dbo.Requerimientoes");
            DropForeignKey("dbo.Requerimientoes", "IdProveedor", "dbo.Proveedors");
            DropForeignKey("dbo.Requerimientoes", "IdMoneda", "dbo.Monedas");
            DropIndex("dbo.RequerimientoDetalles", new[] { "IdRequerimiento" });
            DropIndex("dbo.Requerimientoes", new[] { "IdMoneda" });
            DropIndex("dbo.Requerimientoes", new[] { "IdProveedor" });
            DropTable("dbo.RequerimientoDetalles");
            DropTable("dbo.Requerimientoes");
        }
    }
}
