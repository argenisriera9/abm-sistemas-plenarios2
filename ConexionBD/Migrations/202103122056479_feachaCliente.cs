﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class feachaCliente : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clientes", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.Clientes", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.Clientes", "FechaEliminacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Clientes", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Clientes", "FechaRegistro", c => c.DateTime(nullable: false));
        }
    }
}
