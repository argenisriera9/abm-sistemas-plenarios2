﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class identity : DbMigration
    {
        public override void Up()
        {

            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Proveedors");
            AlterColumn("dbo.Empresas", "IdEmpresa", c => c.Int(nullable: false, identity:true));
        }
        
        public override void Down()
        {
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Proveedors", "IdProveedor");
        }
    }
}
