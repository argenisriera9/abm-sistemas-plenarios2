﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class unidadmedida2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.UnidadMedidas");
            DropColumn("dbo.UnidadMedidas", "IdUnidadMedidad");
            AddColumn("dbo.UnidadMedidas", "IdUnidadMedida", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.UnidadMedidas", "IdUnidadMedida");
           
        }
        
        public override void Down()
        {
            AddColumn("dbo.UnidadMedidas", "IdUnidadMedidad", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.UnidadMedidas");
            DropColumn("dbo.UnidadMedidas", "IdUnidadMedida");
            AddPrimaryKey("dbo.UnidadMedidas", "IdUnidadMedidad");
        }
    }
}
