﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmigrationupdate1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clientes", "EmailCliente", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "EmailCliente", c => c.String());
        }
    }
}
