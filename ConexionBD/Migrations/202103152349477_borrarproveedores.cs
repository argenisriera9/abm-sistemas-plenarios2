﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class borrarproveedores : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Proveedors", "IdEmpresa", "dbo.Empresas");
            DropForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes");
            DropIndex("dbo.Proveedors", new[] { "IdTipoDocumento" });
            DropIndex("dbo.Proveedors", new[] { "IdEmpresa" });
            DropTable("dbo.Proveedors");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Proveedors",
                c => new
                    {
                        IdProveedor = c.Int(nullable: false, identity: true),
                        Codigo = c.String(),
                        Direccion = c.String(),
                        IdTipoDocumento = c.Int(nullable: false),
                        IdEmpresa = c.Int(nullable: false),
                        NumeroDocumento = c.String(),
                        NombreProveedor = c.String(),
                        ApellidoPaterno = c.String(),
                        ApellidoMaterno = c.String(),
                        Correo = c.String(),
                        WebSite = c.String(),
                        IdTipoContribuyente = c.Int(nullable: false),
                        FlagActivo = c.Boolean(nullable: false),
                        UsuarioElimino = c.String(),
                        UsuarioActivo = c.String(),
                        UsuarioCreo = c.String(),
                        UsuarioEdito = c.String(),
                        FechaRegistro = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        Telefono = c.String(),
                        FechaEliminacion = c.DateTime(),
                        Activo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdProveedor);
            
            CreateIndex("dbo.Proveedors", "IdEmpresa");
            CreateIndex("dbo.Proveedors", "IdTipoDocumento");
            AddForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes", "IdTipoDocumento", cascadeDelete: true);
            AddForeignKey("dbo.Proveedors", "IdEmpresa", "dbo.Empresas", "IdEmpresa", cascadeDelete: true);
        }
    }
}
