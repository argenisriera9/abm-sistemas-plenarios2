﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class proveedorsolve : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Proveedors",
                c => new
                    {
                        IdProveedor = c.Int(nullable: false, identity: true),
                        Codigo = c.String(),
                        Direccion = c.String(),
                        IdTipoDocumento = c.Int(nullable: false),
                        IdEmpresa = c.Int(nullable: false),
                        NumeroDocumento = c.String(),
                        NombreProveedor = c.String(),
                        ApellidoPaterno = c.String(),
                        ApellidoMaterno = c.String(),
                        Correo = c.String(),
                        WebSite = c.String(),
                        IdTipoContribuyente = c.Int(nullable: false),
                        FlagActivo = c.Boolean(nullable: false),
                        UsuarioElimino = c.String(),
                        UsuarioActivo = c.String(),
                        UsuarioCreo = c.String(),
                        UsuarioEdito = c.String(),
                        FechaRegistro = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        Telefono = c.String(),
                        FechaEliminacion = c.DateTime(),
                        Activo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdProveedor)
                .ForeignKey("dbo.Empresas", t => t.IdEmpresa, cascadeDelete: true)
                .ForeignKey("dbo.TipoDocumentoes", t => t.IdTipoDocumento, cascadeDelete: true)
                .Index(t => t.IdTipoDocumento)
                .Index(t => t.IdEmpresa);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes");
            DropForeignKey("dbo.Proveedors", "IdEmpresa", "dbo.Empresas");
            DropIndex("dbo.Proveedors", new[] { "IdEmpresa" });
            DropIndex("dbo.Proveedors", new[] { "IdTipoDocumento" });
            DropTable("dbo.Proveedors");
        }
    }
}
