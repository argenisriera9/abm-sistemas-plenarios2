﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pro : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Proveedors");
            AddColumn("dbo.Proveedors", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Proveedors", "Id");
            DropColumn("dbo.Proveedors", "IdProveedor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Proveedors", "IdProveedor", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Proveedors");
            DropColumn("dbo.Proveedors", "Id");
            AddPrimaryKey("dbo.Proveedors", "IdProveedor");
        }
    }
}
