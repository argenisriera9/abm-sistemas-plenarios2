﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fechas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UsuarioRols", "FechaRegsitro", c => c.DateTime());
            AddColumn("dbo.UsuarioRols", "FechaModificacion", c => c.DateTime());
            AddColumn("dbo.UsuarioRols", "FechaEliminacion", c => c.DateTime());
            AlterColumn("dbo.EstadoDespachoes", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.EstadoDespachoes", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.EstadoDespachoes", "FechaEliminacion", c => c.DateTime());
            AlterColumn("dbo.EstadoProductos", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.EstadoProductos", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.EstadoProductos", "FechaEliminacion", c => c.DateTime());
            AlterColumn("dbo.Productos", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.Productos", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.Productos", "FechaEliminacion", c => c.DateTime());
            AlterColumn("dbo.Proveedors", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.Proveedors", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.Proveedors", "FechaEliminacion", c => c.DateTime());
            AlterColumn("dbo.Usuarios", "FechaRegistro", c => c.DateTime());
            AlterColumn("dbo.Usuarios", "FechaModificacion", c => c.DateTime());
            AlterColumn("dbo.Usuarios", "FechaEliminacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Usuarios", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Usuarios", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Usuarios", "FechaRegistro", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Proveedors", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Proveedors", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Proveedors", "FechaRegistro", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Productos", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Productos", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Productos", "FechaRegistro", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EstadoProductos", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EstadoProductos", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EstadoProductos", "FechaRegistro", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EstadoDespachoes", "FechaEliminacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EstadoDespachoes", "FechaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.EstadoDespachoes", "FechaRegistro", c => c.DateTime(nullable: false));
            DropColumn("dbo.UsuarioRols", "FechaEliminacion");
            DropColumn("dbo.UsuarioRols", "FechaModificacion");
            DropColumn("dbo.UsuarioRols", "FechaRegsitro");
        }
    }
}
