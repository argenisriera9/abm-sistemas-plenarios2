﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LogoEmpresa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empresas", "Logo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empresas", "Logo");
        }
    }
}
