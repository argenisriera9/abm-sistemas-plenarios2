﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tipodocu : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes");
            DropIndex("dbo.Proveedors", new[] { "IdTipoDocumento" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Proveedors", "IdTipoDocumento");
            AddForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes", "IdTipoDocumento", cascadeDelete: true);
        }
    }
}
