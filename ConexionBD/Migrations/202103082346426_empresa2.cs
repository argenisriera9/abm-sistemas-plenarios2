﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class empresa2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        IdEmpresa = c.Int(nullable: false, identity: true),
                        EsPrincipal = c.Boolean(nullable: false),
                        Nombre = c.String(),
                        Correo = c.String(),
                        Web = c.String(),
                        Telefono = c.String(),
                        UsuarioElimino = c.String(),
                        UsuarioActivo = c.String(),
                        UsuarioCreo = c.String(),
                        UsuarioEdito = c.String(),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        FechaEliminacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdEmpresa);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Empresas");
        }
    }
}
