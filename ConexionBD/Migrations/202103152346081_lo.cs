﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Proveedors", "IdProveedor", c => c.Int(nullable: false, identity: true));
        }
        
        public override void Down()
        {
            AddColumn("dbo.Proveedors", "IdProveedor", c => c.Int(nullable: false, identity: false));
        }
    }
}
