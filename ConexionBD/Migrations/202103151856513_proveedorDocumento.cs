﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class proveedorDocumento : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Proveedors", "IdTipoDocumento");
            AddForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes", "IdTipoDocumento", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proveedors", "IdTipoDocumento", "dbo.TipoDocumentoes");
            DropIndex("dbo.Proveedors", new[] { "IdTipoDocumento" });
        }
    }
}
