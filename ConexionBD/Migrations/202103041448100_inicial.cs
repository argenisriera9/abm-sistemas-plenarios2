﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        IdCliente = c.Int(nullable: false, identity: true),
                        RazonSocial = c.String(),
                        Direccion = c.String(maxLength: 250),
                        EmailCliente = c.String(),
                        Telefono = c.String(),
                        CodigoCliente = c.String(),
                        Activo = c.Boolean(nullable: false),
                        NumeroDocumento = c.String(),
                    })
                .PrimaryKey(t => t.IdCliente);
            
            CreateTable(
                "dbo.EstadoDespachoes",
                c => new
                    {
                        IdEstadoDespacho = c.Int(nullable: false, identity: true),
                        IdProducto = c.Int(nullable: false),
                        IdEstado = c.Int(nullable: false),
                        Recibido = c.DateTime(nullable: false),
                        Salida = c.DateTime(nullable: false),
                        IdCliente = c.Int(nullable: false),
                        Observacion = c.String(),
                    })
                .PrimaryKey(t => t.IdEstadoDespacho)
                .ForeignKey("dbo.Clientes", t => t.IdCliente, cascadeDelete: true)
                .ForeignKey("dbo.EstadoProductos", t => t.IdEstado, cascadeDelete: true)
                .ForeignKey("dbo.Productos", t => t.IdProducto, cascadeDelete: true)
                .Index(t => t.IdProducto)
                .Index(t => t.IdEstado)
                .Index(t => t.IdCliente);
            
            CreateTable(
                "dbo.EstadoProductos",
                c => new
                    {
                        IdEstado = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.IdEstado);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        IdProducto = c.Int(nullable: false, identity: true),
                        CodigoProducto = c.String(),
                        NombreProducto = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto);
            
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        PersonaID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50),
                        FechaNacimiento = c.DateTime(nullable: false),
                        CreditoMaximo = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PersonaID);
            
            CreateTable(
                "dbo.Telefonos",
                c => new
                    {
                        TelefonoID = c.Int(nullable: false, identity: true),
                        Telefono = c.String(maxLength: 50),
                        PersonaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TelefonoID)
                .ForeignKey("dbo.Personas", t => t.PersonaID, cascadeDelete: true)
                .Index(t => t.PersonaID);
            
            CreateTable(
                "dbo.UsuarioRols",
                c => new
                    {
                        IdRol = c.Int(nullable: false, identity: true),
                        IdUsuario = c.Int(nullable: false),
                        NombreRol = c.String(),
                    })
                .PrimaryKey(t => t.IdRol)
                .ForeignKey("dbo.Usuarios", t => t.IdUsuario, cascadeDelete: true)
                .Index(t => t.IdUsuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        IdUsuario = c.Int(nullable: false, identity: true),
                        Correo = c.String(),
                        Password = c.String(),
                        Token = c.String(),
                    })
                .PrimaryKey(t => t.IdUsuario);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsuarioRols", "IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Telefonos", "PersonaID", "dbo.Personas");
            DropForeignKey("dbo.EstadoDespachoes", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.EstadoDespachoes", "IdEstado", "dbo.EstadoProductos");
            DropForeignKey("dbo.EstadoDespachoes", "IdCliente", "dbo.Clientes");
            DropIndex("dbo.UsuarioRols", new[] { "IdUsuario" });
            DropIndex("dbo.Telefonos", new[] { "PersonaID" });
            DropIndex("dbo.EstadoDespachoes", new[] { "IdCliente" });
            DropIndex("dbo.EstadoDespachoes", new[] { "IdEstado" });
            DropIndex("dbo.EstadoDespachoes", new[] { "IdProducto" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.UsuarioRols");
            DropTable("dbo.Telefonos");
            DropTable("dbo.Personas");
            DropTable("dbo.Productos");
            DropTable("dbo.EstadoProductos");
            DropTable("dbo.EstadoDespachoes");
            DropTable("dbo.Clientes");
        }
    }
}
