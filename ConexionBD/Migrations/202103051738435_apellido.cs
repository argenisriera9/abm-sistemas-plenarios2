﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class apellido : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuarios", "Apellido", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuarios", "Apellido");
        }
    }
}
