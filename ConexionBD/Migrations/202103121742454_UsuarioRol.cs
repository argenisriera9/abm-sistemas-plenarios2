﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsuarioRol : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UsuarioRols", "IdUsuario", "dbo.Usuarios");
            DropIndex("dbo.UsuarioRols", new[] { "IdUsuario" });
            CreateIndex("dbo.Usuarios", "IdRol");
            AddForeignKey("dbo.Usuarios", "IdRol", "dbo.UsuarioRols", "IdRol", cascadeDelete: true);
            DropColumn("dbo.UsuarioRols", "IdUsuario");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UsuarioRols", "IdUsuario", c => c.Int(nullable: false));
            DropForeignKey("dbo.Usuarios", "IdRol", "dbo.UsuarioRols");
            DropIndex("dbo.Usuarios", new[] { "IdRol" });
            CreateIndex("dbo.UsuarioRols", "IdUsuario");
            AddForeignKey("dbo.UsuarioRols", "IdUsuario", "dbo.Usuarios", "IdUsuario", cascadeDelete: true);
        }
    }
}
