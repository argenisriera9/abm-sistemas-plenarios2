﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class item : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productos", "Item", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productos", "Item");
        }
    }
}
