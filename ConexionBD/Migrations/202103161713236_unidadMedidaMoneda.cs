﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class unidadMedidaMoneda : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Monedas", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Monedas", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.Monedas", "UsuarioModifico", c => c.String());
            AddColumn("dbo.Monedas", "UsuarioRegistro", c => c.String());
            AddColumn("dbo.Monedas", "UsuarioElimino", c => c.String());
            AddColumn("dbo.Monedas", "FechaRegistro", c => c.DateTime());
            AddColumn("dbo.Monedas", "FechaEliminacion", c => c.DateTime());
            AddColumn("dbo.Monedas", "FechaModificacion", c => c.DateTime());
            AddColumn("dbo.UnidadMedidas", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.UnidadMedidas", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.UnidadMedidas", "UsuarioModifico", c => c.String());
            AddColumn("dbo.UnidadMedidas", "UsuarioRegistro", c => c.String());
            AddColumn("dbo.UnidadMedidas", "UsuarioElimino", c => c.String());
            AddColumn("dbo.UnidadMedidas", "FechaRegistro", c => c.DateTime());
            AddColumn("dbo.UnidadMedidas", "FechaEliminacion", c => c.DateTime());
            AddColumn("dbo.UnidadMedidas", "FechaModificacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UnidadMedidas", "FechaModificacion");
            DropColumn("dbo.UnidadMedidas", "FechaEliminacion");
            DropColumn("dbo.UnidadMedidas", "FechaRegistro");
            DropColumn("dbo.UnidadMedidas", "UsuarioElimino");
            DropColumn("dbo.UnidadMedidas", "UsuarioRegistro");
            DropColumn("dbo.UnidadMedidas", "UsuarioModifico");
            DropColumn("dbo.UnidadMedidas", "Activo");
            DropColumn("dbo.UnidadMedidas", "IdEmpresa");
            DropColumn("dbo.Monedas", "FechaModificacion");
            DropColumn("dbo.Monedas", "FechaEliminacion");
            DropColumn("dbo.Monedas", "FechaRegistro");
            DropColumn("dbo.Monedas", "UsuarioElimino");
            DropColumn("dbo.Monedas", "UsuarioRegistro");
            DropColumn("dbo.Monedas", "UsuarioModifico");
            DropColumn("dbo.Monedas", "Activo");
            DropColumn("dbo.Monedas", "IdEmpresa");
        }
    }
}
