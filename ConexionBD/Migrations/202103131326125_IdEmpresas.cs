﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdEmpresas : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Empresas");
            AddColumn("dbo.Clientes", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Productos", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Proveedors", "IdEmpresa", c => c.Int(nullable: false));
            AddColumn("dbo.Usuarios", "IdEmpresa", c => c.Int(nullable: true));
            AlterColumn("dbo.Empresas", "IdEmpresa", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Empresas", "IdEmpresa");
            CreateIndex("dbo.Empresas", "IdEmpresa");
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Clientes", "IdCliente");
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Productos", "IdProducto");
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Proveedors", "IdProveedor");
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Usuarios", "IdUsuario");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Usuarios");
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Proveedors");
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Productos");
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Clientes");
            DropIndex("dbo.Empresas", new[] { "IdEmpresa" });
            DropPrimaryKey("dbo.Empresas");
            AlterColumn("dbo.Empresas", "IdEmpresa", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Usuarios", "IdEmpresa");
            DropColumn("dbo.Proveedors", "IdEmpresa");
            DropColumn("dbo.Productos", "IdEmpresa");
            DropColumn("dbo.Clientes", "IdEmpresa");
            AddPrimaryKey("dbo.Empresas", "IdEmpresa");
        }
    }
}
