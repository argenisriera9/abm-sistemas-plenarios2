﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rollbackUsuario : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Usuarios");
            DropColumn("dbo.Usuarios", "IdEmpresa");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Usuarios", "IdEmpresa", c => c.Int(nullable: false));
            AddForeignKey("dbo.Empresas", "IdEmpresa", "dbo.Usuarios", "IdUsuario");
        }
    }
}
