﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removereque : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requerimientos", "IdMoneda", "dbo.Monedas");
            DropForeignKey("dbo.Requerimientos", "IdProveedor", "dbo.Proveedors");
            DropForeignKey("dbo.RequerimientoDetalles", "IdRequerimiento", "dbo.Requerimientos");
            DropIndex("dbo.Requerimientos", new[] { "IdProveedor" });
            DropIndex("dbo.Requerimientos", new[] { "IdMoneda" });
            DropIndex("dbo.RequerimientoDetalles", new[] { "IdRequerimiento" });
            DropTable("dbo.Requerimientos");
            DropTable("dbo.RequerimientoDetalles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RequerimientoDetalles",
                c => new
                    {
                        IdRequerimientoDetalle = c.Int(nullable: false, identity: true),
                        IdRequerimiento = c.Int(nullable: false),
                        CodigoProducto = c.String(),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FechaCreacion = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        FechaEliminacion = c.DateTime(),
                        UsuarioCreacion = c.String(),
                        UsuarioModifica = c.String(),
                        UsuarioElimina = c.String(),
                    })
                .PrimaryKey(t => t.IdRequerimientoDetalle);
            
            CreateTable(
                "dbo.Requerimientos",
                c => new
                    {
                        IdRequerimiento = c.Int(nullable: false, identity: true),
                        CodigoRequerimiento = c.String(),
                        FechaRequerimiento = c.DateTime(nullable: false),
                        FechaEntrega = c.DateTime(nullable: false),
                        IdProveedor = c.Int(nullable: false),
                        IdMoneda = c.Int(nullable: false),
                        Estado = c.Int(nullable: false),
                        LugarEntrega = c.String(),
                        FormaPago = c.Int(nullable: false),
                        Observacion = c.String(),
                        Subtotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Impuesto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Moneda = c.Int(nullable: false),
                        FechaCreacion = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        FechaEliminacion = c.DateTime(),
                        UsuarioCreacion = c.String(),
                        UsuarioModifcio = c.String(),
                        UsuarioElimino = c.String(),
                    })
                .PrimaryKey(t => t.IdRequerimiento);
            
            CreateIndex("dbo.RequerimientoDetalles", "IdRequerimiento");
            CreateIndex("dbo.Requerimientos", "IdMoneda");
            CreateIndex("dbo.Requerimientos", "IdProveedor");
            AddForeignKey("dbo.RequerimientoDetalles", "IdRequerimiento", "dbo.Requerimientos", "IdRequerimiento", cascadeDelete: true);
            AddForeignKey("dbo.Requerimientos", "IdProveedor", "dbo.Proveedors", "IdProveedor", cascadeDelete: true);
            AddForeignKey("dbo.Requerimientos", "IdMoneda", "dbo.Monedas", "IdMoneda", cascadeDelete: true);
        }
    }
}
