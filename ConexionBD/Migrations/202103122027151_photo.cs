﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class photo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuarios", "Photo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuarios", "Photo");
        }
    }
}
