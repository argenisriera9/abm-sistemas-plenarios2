﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequerimientoTablas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requerimientos", "IdRequerimiento", "dbo.RequerimientoDetalles");
            DropIndex("dbo.Requerimientos", new[] { "IdRequerimiento" });
            DropPrimaryKey("dbo.Requerimientos");
            AddColumn("dbo.Requerimientos", "IdProveedor", c => c.Int(nullable: false));
            AddColumn("dbo.Requerimientos", "IdMoneda", c => c.Int(nullable: false));
            AlterColumn("dbo.Requerimientos", "IdRequerimiento", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Requerimientos", "IdRequerimiento");
            CreateIndex("dbo.Requerimientos", "IdProveedor");
            CreateIndex("dbo.Requerimientos", "IdMoneda");
            CreateIndex("dbo.RequerimientoDetalles", "IdRequerimiento");
            AddForeignKey("dbo.Requerimientos", "IdMoneda", "dbo.Monedas", "IdMoneda", cascadeDelete: true);
            AddForeignKey("dbo.Requerimientos", "IdProveedor", "dbo.Proveedors", "IdProveedor", cascadeDelete: true);
            AddForeignKey("dbo.RequerimientoDetalles", "IdRequerimiento", "dbo.Requerimientos", "IdRequerimiento", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RequerimientoDetalles", "IdRequerimiento", "dbo.Requerimientos");
            DropForeignKey("dbo.Requerimientos", "IdProveedor", "dbo.Proveedors");
            DropForeignKey("dbo.Requerimientos", "IdMoneda", "dbo.Monedas");
            DropIndex("dbo.RequerimientoDetalles", new[] { "IdRequerimiento" });
            DropIndex("dbo.Requerimientos", new[] { "IdMoneda" });
            DropIndex("dbo.Requerimientos", new[] { "IdProveedor" });
            DropPrimaryKey("dbo.Requerimientos");
            AlterColumn("dbo.Requerimientos", "IdRequerimiento", c => c.Int(nullable: false));
            DropColumn("dbo.Requerimientos", "IdMoneda");
            DropColumn("dbo.Requerimientos", "IdProveedor");
            AddPrimaryKey("dbo.Requerimientos", "IdRequerimiento");
            CreateIndex("dbo.Requerimientos", "IdRequerimiento");
            AddForeignKey("dbo.Requerimientos", "IdRequerimiento", "dbo.RequerimientoDetalles", "IdRequerimientoDetalle");
        }
    }
}
