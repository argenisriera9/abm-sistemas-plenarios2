﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moneda : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Requerimientoes", "Moneda");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Requerimientoes", "Moneda", c => c.Int(nullable: false));
        }
    }
}
