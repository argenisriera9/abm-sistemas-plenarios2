﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Requerimiento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Monedas",
                c => new
                    {
                        IdMoneda = c.Int(nullable: false, identity: true),
                        NombreMoneda = c.String(),
                        Abreviatura = c.String(),
                    })
                .PrimaryKey(t => t.IdMoneda);
            
            CreateTable(
                "dbo.Requerimientos",
                c => new
                    {
                        IdRequerimiento = c.Int(nullable: false,identity:true),
                        CodigoRequerimiento = c.String(),
                        FechaRequerimiento = c.DateTime(nullable: false),
                        FechaEntrega = c.DateTime(nullable: false),
                        Estado = c.Int(nullable: false),
                        LugarEntrega = c.String(),
                        FormaPago = c.Int(nullable: false),
                        Observacion = c.String(),
                        Subtotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Impuesto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Moneda = c.Int(nullable: false),
                        FechaCreacion = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        FechaEliminacion = c.DateTime(),
                        UsuarioCreacion = c.String(),
                        UsuarioModifcio = c.String(),
                        UsuarioElimino = c.String(),
                    })
                .PrimaryKey(t => t.IdRequerimiento)
                .ForeignKey("dbo.RequerimientoDetalles", t => t.IdRequerimiento)
                .Index(t => t.IdRequerimiento);
            
            CreateTable(
                "dbo.RequerimientoDetalles",
                c => new
                    {
                        IdRequerimientoDetalle = c.Int(nullable: false, identity: true),
                        IdRequerimiento = c.Int(nullable: false),
                        CodigoProducto = c.String(),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FechaCreacion = c.DateTime(),
                        FechaModificacion = c.DateTime(),
                        FechaEliminacion = c.DateTime(),
                        UsuarioCreacion = c.String(),
                        UsuarioModifica = c.String(),
                        UsuarioElimina = c.String(),
                    })
                .PrimaryKey(t => t.IdRequerimientoDetalle);
            
            CreateTable(
                "dbo.UnidadMedidas",
                c => new
                    {
                        IdUnidadMedidad = c.Int(nullable: false, identity: true),
                        CodigoUnidadMedida = c.String(),
                        NombreUnidadMedida = c.String(),
                        abreviatura = c.String(),
                    })
                .PrimaryKey(t => t.IdUnidadMedidad);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requerimientoes", "IdRequerimiento", "dbo.RequerimientoDetalles");
            DropIndex("dbo.Requerimientoes", new[] { "IdRequerimiento" });
            DropTable("dbo.UnidadMedidas");
            DropTable("dbo.RequerimientoDetalles");
            DropTable("dbo.Requerimientoes");
            DropTable("dbo.Monedas");
        }
    }
}
