﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class flag2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Usuarios", "Flag", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Usuarios", "Flag", c => c.Boolean(nullable: false));
        }
    }
}
