﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdTipoDocumento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TipoDocumentoes",
                c => new
                    {
                        IdTipoDocumento = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Abreviatura = c.String(),
                        Activo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdTipoDocumento);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TipoDocumentoes");
        }
    }
}
