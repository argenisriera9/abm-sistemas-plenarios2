﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nombreUsuario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuarios", "Nombre", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuarios", "Nombre");
        }
    }
}
