﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stock : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Productos", "Stock", c => c.Decimal(nullable: true, precision: 18, scale: 2));
            AlterColumn("dbo.Productos", "StockMinimo", c => c.Decimal(nullable: true, precision: 18, scale: 2));
            AlterColumn("dbo.Productos", "StockMaximo", c => c.Decimal(nullable: true, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Productos", "StockMaximo", c => c.String());
            AlterColumn("dbo.Productos", "StockMinimo", c => c.String());
            AlterColumn("dbo.Productos", "Stock", c => c.String());
        }
    }
}
