﻿namespace ConexionBD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class registrosActivos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empresas", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.EstadoDespachoes", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.EstadoProductos", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.Productos", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.Proveedors", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.UsuarioRols", "Activo", c => c.Int(nullable: false));
            AddColumn("dbo.Usuarios", "Activo", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "Activo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "Activo", c => c.Boolean(nullable: false));
            DropColumn("dbo.Usuarios", "Activo");
            DropColumn("dbo.UsuarioRols", "Activo");
            DropColumn("dbo.Proveedors", "Activo");
            DropColumn("dbo.Productos", "Activo");
            DropColumn("dbo.EstadoProductos", "Activo");
            DropColumn("dbo.EstadoDespachoes", "Activo");
            DropColumn("dbo.Empresas", "Activo");
        }
    }
}
