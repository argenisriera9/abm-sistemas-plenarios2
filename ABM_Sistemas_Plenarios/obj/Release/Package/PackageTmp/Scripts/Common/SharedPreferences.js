﻿(function () {

    var app = angular.module('app', []);

    app.service('Scopes', ['$window', function ($window) {

        var service = {
            store: store,
            get: get,
            getRemove: getRemove,
            remove: remove,
            clearAll: clearAll
        };
        return service;


        function store(key, value) {
            $window.sessionStorage.setItem(key, angular.toJson(value, false));
        }

        function get(key) {
            return angular.fromJson($window.sessionStorage.getItem(key));
        }

        function getRemove(key) {
            var value = angular.fromJson($window.sessionStorage.getItem(key));
            this.remove(key);
            return value;
        }

        function remove(key) {
            $window.sessionStorage.removeItem(key);
        }

        function clearAll() {
            $window.sessionStorage.clear();
        }


    }]);

}());