﻿(function () {

    var app = angular.module('app', []);
    new WOW().init();
    $('[data-toggle="tooltip"]').tooltip();
    $('.dropdown-toggle').dropdown();

}());

//=================================
// [ CONFIGURATION ]
//=================================

// RestService for get client IP, Example response: { ip: 192.168.0.1 }
var getApiJsonGetIP = function () {
    return "/api/Config/GetIpApiUrl";
};

// API Domain URL (This is local)
var getApiRoot = function () {
    return "/Config/GetApiRoot";
};

var createApiUrl = function (action) {
    return getApiRoot() + action;
}

//=================================
//=================================

var HasUserRole = function (user, role) {
    var indexFound = -1;

    if (user.Roles != null) {
        for (var i = 0; i < user.Roles.length; i++) {
            var IdRole = user.Roles[i].Rol;
            if (IdRole == role) {
                indexFound = i;
                break;
            }
        }
    } else {
        window.location.href = "/Login/LogOut";
    }

    return (indexFound != -1);
}

var IsFechaValida = function (fecha) {
    var f = new Date(fecha);
    return (f.getFullYear() > 1970);
};

var GetCurrentDate = function () {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    return today;
}

function parseRound(num, dec) {
    var d = Math.pow(10, dec);
    return (Math.round(num * d) / d).toFixed(dec);
}

//==============================================================================================

var GetPDF = function(bytes) {
    var pdfData = null;
    if (bytes != null) {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE workaround
            var byteCharacters = atob(bytes);
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            var blob = new Blob([byteArray], { type: 'application/pdf' });
            pdfData = blob;
        }
        else { // much easier if not IE
            pdfData = "data:application/pdf;base64, " + bytes, '', "height=600,width=800";
        }
    }
    return pdfData;
}

var FILE_TYPE_PDF = "PDF";
var FILE_TYPE_EXCEL = "EXCEL";
var FILE_TYPE_WORD = "WORD";

var DownloadFile = function (type, bytes) {
    var contentType = ''; // MIME-TYPE
    if (type == FILE_TYPE_PDF) contentType = 'application/pdf'; // .pdf
    else if (type == FILE_TYPE_EXCEL) contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'; //.xlsx
    else if (type == FILE_TYPE_WORD) contentType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'; //.docx

    var sliceSize = sliceSize || 512;

    var byteCharacters = atob(bytes);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    var blobUrl = URL.createObjectURL(blob);

    if (type == FILE_TYPE_PDF) {
        window.open(blobUrl);
    }
    else if (type == FILE_TYPE_EXCEL || FILE_TYPE_WORD){
        window.location = blobUrl;
    }
}
//==================================
var dateToString = function (date) {
    if (date == "" || date == null) {
        return '';
    }
    var fechaInicio = date.toLocaleDateString().split("/");
    return fechaInicio[2] + '-' + fechaInicio[1] + '-' + fechaInicio[0];
}
var stringToDate = function (fecha) {
    var fechainicio = fecha.split('-');
    return new Date(fechainicio[2], fechainicio[1] - 1, fechainicio[0]);
}
//==============================================================================================

$(document).ready(function () {
    $(".only-integer").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, and enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".only-decimal").keydown(function (e) {

        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".only-letters").on("keydown", function (event) {
        // Allow controls such as backspace, tab etc.
        var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];

        // Allow letters
        for (var i = 65; i <= 90; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault();
        }
    });

    $(".only-letters").on("input", function () {
        var regexp = /[^a-zA-Z]/g;
        if ($(this).val().match(regexp)) {
            $(this).val($(this).val().replace(regexp, ''));
        }
    });
});