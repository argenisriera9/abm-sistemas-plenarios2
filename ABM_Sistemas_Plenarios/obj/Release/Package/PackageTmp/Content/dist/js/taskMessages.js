﻿function windowErrorDialog(message, onConfirm) {
    var fClose = function () {
        modal.modal("hide");
    };
    var modal = $("#windowErrorModal");
    modal.modal("show");
    $("#windowErrorMessage").empty().append(message);
    $("#windowErrorOk").one('click', onConfirm);
    $("#windowErrorOk").one('click', fClose);
}

function OnError(xhr, errorType, exception) {
    var responseText;
    var html;
    try {
        responseText = $.parseJSON(xhr.responseText);
        
        html = "<div><red><h2><b>" + xhr.status + " " + exception + "</b><h2></red></div>";
        html += "<div><b><u>Url</u>:</b><br>" + this.url + "<br></div>";
        html+= "<div><b><u>Mensaje</u>:</b><br>" + responseText.Message + "<br></div>";
        html+="<div><b><u>Traza</u>:</b><br>" + responseText.StackTrace + "<br></div>";
        
    } catch (e) {
        html = "Error: " + xhr.status + " " + exception;
    }
    windowErrorDialog(html);
}

function linkUserTasks() {
    window.location.href=$("#task_user_page").val();
}
    

//Carga Tareas en pagina de ver todas
function loadUserTasks() {
    var html = "";
    var checked = "";
    $.ajax({
        type: "GET",
        url: $("#task_user").val(),
        dataType: "json",
        success: function (data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    //var fecha = new Date(parseInt(data[i].FechCreaTask.substr(6)));
                    checked = "";
                    html += "<div class=\"box box-solid\">\n";
                    html += "    <div class=\"box-header with-border\">\n";
                    html += "      <div class=\"pull-right\"><i class=\"fa fa-clock-o\"></i>&nbsp;" + parseJsonDate(data[i].FechCreaTask) + "</small></div>\n";
                    html += "    </div>\n";
                    html += "    <div class=\"box-body\">\n";
                    html += "      <dl>\n";

                    if (data[i].CompTask == 1) {
                        //Tarea marcada como realizada
                        html += "        <input type=\"checkbox\" value=\"" + data[i].IdTask + "\" checked onclick=\"javascript:updateTasksUserMain(this);\">\n";
                        html += "        <span class=\"text\"><strike>" + data[i].DescTask + "</strike></span>\n";
                    } else {
                        html += "        <input type=\"checkbox\" value=\"" + data[i].IdTask + "\" onclick=\"javascript:updateTasksUserMain(this);\">\n";
                        html += "        <span class=\"text\">" + data[i].DescTask + "</span>\n";
                    }
                    
                    html += "      </dl>\n";
                    html += "     </div>\n";
                    html += "</div>\n";
                }
            }
            $("#main_task_page").html(html);
        },
        error: OnError
    });
}

//icono de tareas pendientes
function listTasksUser() {
    var html = "";
    $.ajax({
        type: "GET",
        url: $("#task_user").val(),
        dataType: "json",
        success: function (data) {
            if (data.length == 0) {
                //Sin tareas
                $("#total_tasks_user").html("");
                $("#header_tasks").html("Usted no tiene tareas pendientes");
                $("#list_tasks_user").html("");
                $("#header_footer").html("<a href=\"#\"><i class\"fa fa-search\"></i>Ver todas2</a>");
            } else {
                var total = data.length;
                var nroPendTask = 0;
                if (data.length >= 5) {
                    total = 5; // Se muestran las 5 últimas tareas
                }

                for (i = 0; i < total; i++) {
                    nroPendTask = data[i].noPendTask;
                    html += "<li><a href=\"#\">\n";
                    //html += "<i class=\"fa fa-bell\" style=\"font-size:0.7em;\"></i>";
                    if (data[i].CompTask == 1) {
                        html += "   <input type=\"checkbox\" value=\"" + data[i].IdTask + "\" checked onclick=\"javascript:updateTasksUserMain(this);\">&nbsp;<span class=\"text\"><strike>" + data[i].DescTask + "</strike></span>\n";
                    } else {
                        html += "   <input type=\"checkbox\" value=\"" + data[i].IdTask + "\" onclick=\"javascript:updateTasksUserMain(this);\">&nbsp;<span class=\"text\">" + data[i].DescTask + "</span>\n";
                    }
                    html += "</a></li>\n";
                }

                if (nroPendTask == 0) {
                    $("#header_tasks").html("Usted no tiene tareas pendientes");
                    $("#total_tasks_user").html("");
                } else {
                    $("#header_tasks").html("Usted tiene " + nroPendTask + " tarea(s) pendiente(s)");
                    $("#total_tasks_user").html(nroPendTask);
                }

                $("#list_tasks_user").html(html);
                $("#header_footer").html("<a href=\"" + $("#task_user_page").val() + "\">Ver todas</a>");
            }
        },
        error: OnError
    });
}

//Seccion de Tareas Pendientes página de inicio
function listTasksUserMain() {
    var html = "";
    $.ajax({
        type: "GET",
        url: $("#task_user_main").val(),
        dataType: "json",
        success: function (data) {
            if (data.length == 0) {
                html = "<li>Usted no posee tareas pendientes</li>";
                $("#list_task_user_footer").html("");
            } else {
                for (i = 0; i < data.length; i++) {
                    html += "<li>\n";
                    if (data[i].CompTask == 1) {
                        //Tarea marcada como realizada
                        html += "   <input type=\"checkbox\" value=\"" + data[i].IdTask + "\" checked onclick=\"javascript:updateTasksUserMain(this);\">\n";
                        html += "   <span class=\"text\"><strike>" + data[i].DescTask + "</strike></span>\n";
                    } else {
                        html += "   <input type=\"checkbox\" value=\"" + data[i].IdTask + "\" onclick=\"javascript:updateTasksUserMain(this);\">\n";
                        html += "   <span class=\"text\">" + data[i].DescTask + "</span>\n";
                    }
                    html+= "</li>\n";                        
                }
                $("#list_task_user_footer").html("<button type=\"button\" class=\"btn btn-primary btn-sm pull-right\" id=\"btn_tasks\" onclick=\"javascript:linkUserTasks();\"><i class=\"fa fa-search\"></i>&nbsp;Ver todas</button>");
            }
            $("#list_task_user_main").html(html);
            
        },
        error: OnError
    });
}

//Actualiza tarea pendiente
function updateTasksUserMain(obj) {
    var checked = 0;
    if (obj.checked) {
        checked = 1;
    }
    var formData = { comp_task: checked, id_task: obj.value };
    $.ajax({
        type: "GET",
        url: $("#update_task_user").val(),
        dataType: "json",
        data: formData,
        success: function (data) {
            if (data!= "OK") {
                alert("Error actualizando BD");
            } else {
                //Actualizando Lista de Tareas pantalla principal/menu
                listTasksUserMain();
                listTasksUser();
                if (typeof ($('main_task_page')) == "object") {
                    loadUserTasks();
                }
                
            }
        },
        error: OnError
    });
}