﻿function validatePublicationsActivities() {
    var error = false;

    clearActivities();

    if ($("#substractionPointActivity").val() == null || $("#substractionPointActivity").val() == "") {
        $("#substractionPointActivity").parent().attr("class", "form-group has-error");
        $("#substractionPointActivity").parent().children("span").text("Por favor indique la puntuación");
        error = true;
    }
    return !error;
}


function clearActivities() {
    $("#substractionPointActivity").parent().attr("class", "form-group");
    $("#substractionPointActivity").parent().children("span").text("");
}

function validatePublications() {
    var error = false;

    clearPublications();

    if ($("#substractionPoint").val() == null || $("#substractionPoint").val() == "") {
        $("#substractionPoint").parent().attr("class", "form-group has-error");
        $("#substractionPoint").parent().children("span").text("Por favor indique la puntuación");
        error = true;
    }

    return !error;
}

function clearPublications() {
    $("#substractionPoint").parent().attr("class", "form-group");
    $("#substractionPoint").parent().children("span").text("");
}
