﻿function updateNotiUserMain() {
    $.ajax({
        type: "GET",
        url: $("#update_noti_user").val(),
        dataType: "json",
        data: "",
        success: function (data) {
            if (data != "OK") {
                alert("Error actualizando BD");
            } else {
                //Actualizando Lista de Tareas pantalla principal/menu
                listNotiUser();
            }
        },
        error: OnError
    });
}

function loadUserNotis() {
    var html = "";
    $.ajax({
        type: "GET",
        url: $("#noti_user_main").val(),
        dataType: "json",
        success: function (data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    html += "<div class=\"box box-solid\">\n";
                    html += "    <div class=\"box-header with-border\">\n";
                    html += "      <div class=\"pull-left\"><b>" + data[i].TitleNoti + "</b></div>\n";
                    html += "      <div class=\"pull-right\"><i class=\"fa fa-clock-o\"></i>&nbsp;" + parseJsonDate(data[i].FechCreaNoti) + "</small></div>\n";
                    html += "    </div>\n";
                    html += "    <div class=\"box-body\">\n";
                    html += "      <dl>\n";
                    html += "        <span class=\"text\">" + data[i].DescNoti+ "</span>\n";
                    html += "      </dl>\n";
                    html += "     </div>\n";
                    html += "</div>\n";
                }
            }
            $("#main_noti_page").html(html);
            listNotiUser();//Actualizando icono de mensajes menu principal
        },
        error: OnError
    });
}

//Icono de mensajes menu principal
function listNotiUser() {
    var html = "";
    $.ajax({
        type: "GET",
        url: $("#noti_user").val(),
        dataType: "json",
        success: function (data) {
            if (data.length == 0) {
                //Sin tareas
                $("#total_noti_user").html("");
                $("#header_noti_user").html("Usted no tiene notificaciones");
                $("#list_noti_user").html("");
                //$("#footer_noti_user").html("<a href=\"#\">Ver todas</a>");
            } else {
                var total = data.length;
                $("#total_noti_user").html(total);
                $("#header_noti_user").html("Usted tiene " + total + " notificacion(es)");
                for (i = 0; i < total; i++) {
                    html += "<li><a href=\"#\">\n";
                    html += "   <span class=\"text\" style=\"color:black;\"><b>" + data[i].TitleNoti + "</b></span><br>\n";
                    html += "   <span class=\"text\" style=\"color:black;\">" + data[i].DescNoti + "</span>\n";
                    html += "</a></li>\n";
                }
                $("#list_noti_user").html(html);
                //$("#footer_noti_user").html("<a href=\"/Notifications/listNotiUserPage\">Ver todas</a>");
            }
            $("#footer_noti_user").html("<a href=\"" + $("#noti_user_page").val() + "\">Ver todas</a>");
        },
        error: OnError
    });
}