﻿function validateDesc() {
    var error = false;

    clearDesc();

    if ($("#selectStatusTable").val() == null || $("#selectStatusTable").val() == 0) {
        $("#selectStatusTable").parent().attr("class", "form-group has-error");
        $("#selectStatusTable").parent().children("span").text("Por favor seleccione un estatus");
        error = true;
    }

    if ($("#descTable").val() == null || $("#descTable").val().length == 0) {
        $("#descTable").parent().attr("class", "form-group has-error");
        $("#descTable").parent().children("span").text("Por favor especifique una descripción");
        error = true;
    }

    return !error;
}

function clearDesc() {
    $("#selectStatusTable").parent().attr("class", "form-group");
    $("#selectStatusTable").parent().children("span").text("");

    $("#descTable").parent().attr("class", "form-group");
    $("#descTable").parent().children("span").text("");

}

