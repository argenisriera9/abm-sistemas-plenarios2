﻿using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new Filters.LoginFilters());
        }
    }
}
