[assembly: WebActivator.PostApplicationStartMethod(typeof(ABM_Sistemas_Plenarios.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace ABM_Sistemas_Plenarios.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;
    using ConexionBD;
    using global::Models;
    using Services.ClienteServices;
    using Services.EmpresasServices;
    using Services.EstadoDespachoServices;
    using Services.EstadoProductosServices;
    using Services.MonedaServices;
    using Services.PerfilesServices;
    using Services.PersonasServices;
    using Services.ProductosServices;
    using Services.ProveedoresServices;
    using Services.RequerimientoServices;
    using Services.TelefonosServices;
    using Services.UnidadMedidaServices;
    using Services.UsuariosServices;
    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {


            // For instance:
            // container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Scoped);
            container.Register<IPersonasServices,PersonasServices>(Lifestyle.Scoped);
            container.Register<ITelefonosServices, TelefonosServices>(Lifestyle.Scoped);
            container.Register<IClienteServices, ClienteServices>(Lifestyle.Scoped);
            container.Register<IProductoServices, ProductosServices>(Lifestyle.Scoped);
            container.Register<IEstadoProductosServices, EstadoProductosServices>(Lifestyle.Scoped);
            container.Register<IEstadoDespachoServices, EstadoDespachoServices>(Lifestyle.Scoped);
            container.Register<IEmpresaServices, EmpresaServices>(Lifestyle.Scoped);
            container.Register<IUsuariosServices, UsuariosServices>(Lifestyle.Scoped);
            container.Register<IPerfilesServices, PerfilesServices>(Lifestyle.Scoped);
            container.Register<IProveedoresServices, ProveedoresServices>(Lifestyle.Scoped);
            container.Register<IUnidadMedidaService, UnidadMedidaService>(Lifestyle.Scoped);
            container.Register<IMonedaServices, MonedaServices>(Lifestyle.Scoped);

            container.Register<IRequerimientoServices, RequerimientoServices>(Lifestyle.Scoped);
            container.RegisterSingleton<Conexion_BD>();
        }
    }
}