﻿using Models;
using Services.ClienteServices;
using Services.EstadoDespachoServices;
using Services.EstadoProductosServices;
using Services.ProductosServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{

    
    public class EstadoDespachosController : Controller
    {
        static string result = "OK";
        private readonly IEstadoDespachoServices _IestadoDespachoServices;
        private readonly IProductoServices _iProductosServices;
        private readonly IClienteServices _iclienteServices;
        private readonly IEstadoProductosServices _estadoProductosServices;
       
        public EstadoDespachosController(IEstadoDespachoServices IestadoDespachoServices, IProductoServices iProductosServices, IClienteServices iclienteServices, IEstadoProductosServices estadoProductosServices)
        {
            _IestadoDespachoServices = IestadoDespachoServices;
            _iclienteServices = iclienteServices;
            _iProductosServices = iProductosServices;
            _estadoProductosServices = estadoProductosServices;
         
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {


            Usuario oUsuario = (Usuario)Session["Usuario"];
            var result = _IestadoDespachoServices.Get(oUsuario.IdUsuario); 
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetById(int id)
        {
            var result = _IestadoDespachoServices.GeById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Insert(EstadoDespacho despacho)
        {


            Usuario oUsuario = (Usuario)Session["Usuario"];
            despacho.Recibido = Convert.ToDateTime(despacho.Recibido);
            despacho.Salida = Convert.ToDateTime(despacho.Salida);
            despacho.IdUsuario = oUsuario.IdUsuario;
            _IestadoDespachoServices.Insert(despacho);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(EstadoDespacho despacho)
        {
            _IestadoDespachoServices.Edit(despacho);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            _IestadoDespachoServices.delete(id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductos()
        {
            var result = _iProductosServices.GetProductos();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetClientes()
        {
            var result = _iclienteServices.GetClientes(); 
            return Json( result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetEstados()
        {
            var result = _estadoProductosServices.GetEstadoPro();
            return Json( result, JsonRequestBehavior.AllowGet);
        }


    }
}