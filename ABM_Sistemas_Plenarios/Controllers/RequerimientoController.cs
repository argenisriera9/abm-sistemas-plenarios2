﻿using Models;
using Services.ProductosServices;
using Services.ProveedoresServices;
using Services.RequerimientoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class RequerimientoController : Controller
    {
        private readonly IRequerimientoServices _IrequerimientoService;
        private readonly IProductoServices _IproductoServices;
        private readonly IProveedoresServices _IproveedoresServices;

        public RequerimientoController(IRequerimientoServices IrequerimientoService, IProductoServices IproductoServices, IProveedoresServices IproveedoresServices)
        {
            _IrequerimientoService = IrequerimientoService;
            _IproductoServices = IproductoServices;
            _IproveedoresServices = IproveedoresServices;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            try
            {
                var result = _IrequerimientoService.Get();
                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                string error = "error";
                return Json(error + e.Message, JsonRequestBehavior.AllowGet);
            }

           
        }

        public ActionResult GetProductoById(int id,string NombreProducto, string CodigoProducto)
        {
            
                List<RequerimientoDetalle> Req = (List<RequerimientoDetalle>)Session["ReqDetalle"];
                List<RequerimientoDetalle> requeDet = new List<RequerimientoDetalle>();
                if (Session["ReqDetalle"] == null)
                 {
                        Req = requeDet;
                 }
                
            Productos pro = new Productos();
            pro.CodigoProducto = CodigoProducto;
            pro.NombreProducto = NombreProducto;
            pro.IdProducto = id;
            pro.Seleccionado = 1;
            Req.Add(new RequerimientoDetalle { Producto = pro, CodigoProducto = CodigoProducto, Cantidad = 1.00M });
            Session["ReqDetalle"] = Req;
            return Json(new { data = Req }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidarProductoDetalle(int id, string NombreProducto, string CodigoProducto)
        {
            List<RequerimientoDetalle> Req = (List<RequerimientoDetalle>)Session["ReqDetalle"];
            List<RequerimientoDetalle> requeDet = new List<RequerimientoDetalle>();
            if (Session["ReqDetalle"] == null)
            {
                Req = requeDet;
            }
            var seleccionado = Req.FirstOrDefault(x => x.Producto.IdProducto == id);
            if (seleccionado != null)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActualizarCantidadProducto(int id , decimal cantidad)
        {
            if (cantidad <= 0)
            {
                return Json("ERROR", JsonRequestBehavior.AllowGet);
            }

            List<RequerimientoDetalle> Req = (List<RequerimientoDetalle>)Session["ReqDetalle"];
            var ActCantidad = Req.FirstOrDefault(x=>x.Producto.IdProducto==id);
            ActCantidad.Cantidad = cantidad;
            Session["ReqDetalle"] = Req;

            return Json("OK", JsonRequestBehavior.AllowGet);
            
        }
        public ActionResult LimpiarTabla()
        {
            Session["ReqDetalle"] = null;
            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProveedores()
        {
            var result = _IproveedoresServices.Get();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }


    }
}