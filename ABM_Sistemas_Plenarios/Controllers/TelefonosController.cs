﻿using Models;
using Services.TelefonosServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class TelefonosController : Controller
    {
        private readonly ITelefonosServices _telefonosServices;
        public TelefonosController(ITelefonosServices telefonosServices)
        {
            _telefonosServices = telefonosServices;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTelefonos(int PersonaID)
        {
            var result = _telefonosServices.GetTelefonos(PersonaID);

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult InsertarTelefonos(Telefonos telefonos)
        {
            string result = "OK";
           _telefonosServices.InsertarTel(telefonos);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTelByID(int id)
        {
            var result = _telefonosServices.GetById(id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Editar(Telefonos telefonos)
        {  
            string result = "OK";
            _telefonosServices.ModificarTel(telefonos);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult delete(int id)
        {
            string result = "OK";
            _telefonosServices.delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}