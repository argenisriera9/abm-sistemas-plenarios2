﻿using Models;
using Services.EstadoProductosServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class EstadoProductosController : Controller
    {
        private readonly IEstadoProductosServices _estadoProductosServices;
        static string result = "OK";
        public EstadoProductosController(IEstadoProductosServices estadoProductosServices)
        {
            _estadoProductosServices = estadoProductosServices;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            var result = _estadoProductosServices.GetEstadoPro();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetById(int id)
        {
            var result = _estadoProductosServices.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Insert(EstadoProductos estado )

        {
            _estadoProductosServices.InserEstadoPro(estado);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(EstadoProductos estado)
        {
            _estadoProductosServices.Edit(estado);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            _estadoProductosServices.delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}