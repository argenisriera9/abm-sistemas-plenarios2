﻿using ABM_Sistemas_Plenarios.Models;
using ConexionBD;
using Models;
using Services.EmpresasServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class HomeController : Controller
    {

        private readonly IEmpresaServices _IempresaServices;
        
        public HomeController(IEmpresaServices IempresaServices)
        {
            _IempresaServices = IempresaServices;
        }
        public ActionResult Index()
        {


            List<EmpresaViewModel> model = new List<EmpresaViewModel>();
            List<Empresa> empresa = _IempresaServices.Get();

            
            foreach(var emp in empresa )
            {
                EmpresaViewModel com = new EmpresaViewModel();
                com.Logo = emp.Logo;
                com.IdEmpresa = emp.IdEmpresa;
                com.Nombre = emp.Nombre;
                model.Add(com);
               
                     

            }
            //Session["Empresa"] = model;
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult InsertarFirst(Empresa empresa)
        {
            Usuario osuario = (Usuario)Session["Usuario"];
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            UploadImages file = (UploadImages)Session["Upload"];
            empresa.Logo = file.ImageFile.FileName;
            empresa.FechaRegistro = Convert.ToDateTime(fecha);
            empresa.UsuarioCreo = osuario.Nombre;


            int id = _IempresaServices.Insert(empresa);
            var InicioEmpresa = _IempresaServices.GetById(id);
            Session["Empresa"] = InicioEmpresa;
            return Json("OK", JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public ActionResult Upload(UploadImages file)
        {
            if (file != null)
            {
                try
                {
                    string Filename = Path.GetFileNameWithoutExtension(file.ImageFile.FileName);
                    string extension = Path.GetExtension(file.ImageFile.FileName);

                    if (extension == ".jpg" || extension == ".PNG" || extension == ".png")

                    {
                        //Filename = Filename + DateTime.Now.ToString("ddmmyyyyy") + extension;
                        //file.path = "/ImagenesEmpresa/" + Filename + extension;
                        file.ImageFile.SaveAs(Server.MapPath("/ImagenesEmpresa/" + Filename + extension));


                        Session["Upload"] = file;


                        return Json("OK", JsonRequestBehavior.AllowGet);
                    }
                    else return Json("Error", JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    string r = "No se pudo cargar el archivo" + e;
                    return Json(r, JsonRequestBehavior.AllowGet);
                }
            }
            else return Json("null", JsonRequestBehavior.AllowGet);

        }


    }
}