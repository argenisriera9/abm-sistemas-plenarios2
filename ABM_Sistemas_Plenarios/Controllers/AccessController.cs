﻿using ABM_Sistemas_Plenarios.Models;
using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class AccessController : Controller
    {
        private readonly  Conexion_BD db;

        public AccessController(Conexion_BD _db)
        {
            db = _db;
        }


        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string correo, string password)
        {

            try
            {
                Usuario Ousuario = new Usuario();
                Empresa empresa = new Empresa();
                List<Usuario> ousuario = db.Usuarios.ToList();
                 Ousuario = (from u in db.Usuarios
                                where u.Correo == correo && u.Password == password
                                select u).FirstOrDefault();

               // empresa = (from e in db.Empresa select e).FirstOrDefault();
             
               //if(empresa!=null)
               // {
               //     Session["Empresa"]= empresa;
               // }

                if (Ousuario == null)
                {
                    ViewBag.error = "Usuario o Contraseña incorrecta";
                    return Json("Error",JsonRequestBehavior.AllowGet);
                }

                Session["usuario"] = Ousuario;

                
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return RedirectToAction("Login", "Access");
            }
        }

        public ActionResult Logout()
        {

            Session["usuario"] = null;
            Session["Empresa"] = null;
           return RedirectToAction("Login", "Access");
        }


    }
}