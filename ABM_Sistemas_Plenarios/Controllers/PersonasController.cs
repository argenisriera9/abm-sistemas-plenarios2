﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Services.PersonasServices;
using System.Threading.Tasks;
using Services.TelefonosServices;

namespace ABM_Sistemas_Plenarios.Controllers
{

    public class PersonasController : Controller
    {
        private readonly IPersonasServices _personasServices;
        private readonly ITelefonosServices _telefonosServices;
        public PersonasController(IPersonasServices personasServices, ITelefonosServices telefonosServices)
        {
            _personasServices = personasServices;
            _telefonosServices = telefonosServices;
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPersonas(string nombre)
        {

            if(nombre=="")
            {
                
                var persona = _personasServices.GetPersonas().ToList();
                return Json(new { data = persona }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var personaByNombre = _personasServices.GetPersonasByNombre(nombre);
                return Json(new { data = personaByNombre }, JsonRequestBehavior.AllowGet);
            }
            
           
        }

        public ActionResult GetPersonasByID(int id)
        {
            var result = _personasServices.GetPersonasByID(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Insertar(string Nombre, string FechaNacimiento, string CreditoMaximo)

        {
            
            var Existe = _personasServices.GetPersonasByNombre(Nombre);

            if (Existe.Count ==0)
            {
                string result = "OK";
                Personas personas = new Personas();
                personas.Nombre = Nombre;
                personas.FechaNacimiento = Convert.ToDateTime(FechaNacimiento);
                personas.CreditoMaximo = Convert.ToDecimal(CreditoMaximo);
                _personasServices.Insertar(personas);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            string Result = "Error";
            return Json(Result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Editar(Personas personas)
        {
            string result = "OK";

            _personasServices.Modificar(personas);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {

            var result = _personasServices.delete(id);

            return Json(result, JsonRequestBehavior.AllowGet);


        }

       
    }
}