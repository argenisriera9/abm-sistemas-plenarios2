﻿using Models;
using Services.ProductosServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class ProductosController : Controller
    {
        private readonly IProductoServices _iProductosServices;
        static string result = "OK";
       
        public ProductosController(IProductoServices iProductosServices)
        {
            _iProductosServices = iProductosServices;
          
            

        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetProductos(string codigo)
        {
           
            var result = _iProductosServices.GetProductos().Where(x=>x.IdEmpresa == ((Empresa)Session["Empresa"]).IdEmpresa );
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductosByiD(int id)
        {
            var result = _iProductosServices.GetProductosByID(id);

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult InsertPro (Productos productos)
        {

            bool existe = _iProductosServices.GetProductosByCodigo(productos.CodigoProducto);
            if (!existe)
            {
                Usuario usuario = (Usuario)Session["Usuario"];
                string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                productos.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
                productos.FechaRegistro = Convert.ToDateTime(fecha);
                productos.UsuarioCreo = usuario.Nombre + usuario.Apellido;
                _iProductosServices.InsertarPro(productos);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Existe", JsonRequestBehavior.AllowGet);
            }
        }

       
        public ActionResult EditPro(Productos productos)
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            productos.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
            productos.FechaModificacion = Convert.ToDateTime(fecha);
            productos.UsuarioEdito = usuario.Nombre  + usuario.Apellido;
            _iProductosServices.Edit(productos);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePro(int id)
        {
            _iProductosServices.Delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}