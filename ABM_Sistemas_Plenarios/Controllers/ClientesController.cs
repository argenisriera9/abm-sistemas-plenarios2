﻿using Models;
using Services.ClienteServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class ClientesController : Controller
    {
        private readonly IClienteServices _iclienteServices;
        
        public ClientesController(IClienteServices iclienteServices)
        {
            _iclienteServices = iclienteServices;
           
        }
        public ActionResult Index()
        {

            Empresa empresa = (Empresa)Session["Empresa"];
            return View();
        }

        public ActionResult GetClientes()
        {
           
            var result = _iclienteServices.GetClientes().Where(x=>x.IdEmpresa ==((Empresa)Session["Empresa"]).IdEmpresa);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetClienteById(int id)
        {
            var result = _iclienteServices.GetClientesByID(id);
            return Json(result, JsonRequestBehavior.AllowGet);


        }
        public ActionResult InsertarCliente(Clientes clientes)
        {
            string result = "OK";
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            clientes.UsuarioCreo = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            clientes.FechaRegistro = Convert.ToDateTime(fecha);
            clientes.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
            var codigoCliente = _iclienteServices.ValidarCodigo(clientes);
           
           
            if (codigoCliente != null)
            {

                if (codigoCliente.CodigoCliente == clientes.CodigoCliente)
                {
                    return Json("Codigo", JsonRequestBehavior.AllowGet);
                }
                else if (codigoCliente.RazonSocial == clientes.RazonSocial)
                {
                    return Json("Nombre", JsonRequestBehavior.AllowGet);
                }
                else  
                {
                    return Json("Documento", JsonRequestBehavior.AllowGet);
                }
            }
            else {

        
                _iclienteServices.InsertarClientes(clientes);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            
        }

        public ActionResult EditCliente (Clientes clientes)
        {
            string result = "OK";
             
            var codigoCliente = _iclienteServices.ValidarCodigo(clientes);


            if (codigoCliente != null)
            {

                if (codigoCliente.CodigoCliente == clientes.CodigoCliente)
                {
                    return Json("Codigo", JsonRequestBehavior.AllowGet);
                }
                else if (codigoCliente.RazonSocial == clientes.RazonSocial)
                {
                    return Json("Nombre", JsonRequestBehavior.AllowGet);
                }
                else  
                {
                    return Json("Documento", JsonRequestBehavior.AllowGet);
                }
            }

            else
            {
                
                 clientes.UsuarioEdito = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
                 clientes.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
                _iclienteServices.Edit(clientes);
                 return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            
        }
        public ActionResult DeleteCliente(int id)
        {

            string result = "OK";
            _iclienteServices.Delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
                
        }

    }
}