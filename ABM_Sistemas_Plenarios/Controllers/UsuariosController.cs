﻿using Models;
using PersimosMVC.Filters;
using Services.UsuariosServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly IUsuariosServices _IusuariosServices;
        public UsuariosController(IUsuariosServices IusuariosServices)
            
        {
            _IusuariosServices = IusuariosServices;
        }

        [AuthorizeUser]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            var result = _IusuariosServices.Get();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);

        }

         public ActionResult GetById(int id)
        {
            var result = _IusuariosServices.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Insert(Usuario usuario)
        {
            string result = "OK";
            Usuario UsuarioRegistro = (Usuario)Session["Usuario"];
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            usuario.FechaRegistro = Convert.ToDateTime(fecha);
            usuario.Activo = 1;
            usuario.UsuarioActivo = UsuarioRegistro.Nombre;
            _IusuariosServices.Insert(usuario);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(Usuario usuario)
        {
            string result = "OK";
            Usuario UsuarioEdito = (Usuario)Session["Usuario"];
            usuario.UsuarioActivo = UsuarioEdito.Nombre;
            _IusuariosServices.Edit(usuario);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            string result = "OK";
            _IusuariosServices.delete(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}