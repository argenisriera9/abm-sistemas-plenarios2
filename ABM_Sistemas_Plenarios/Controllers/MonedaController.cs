﻿using Models;
using Services.MonedaServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class MonedaController : Controller
    {
        private readonly IMonedaServices _ImonedaServices;
        public MonedaController(IMonedaServices ImonedaServices)
        {
            _ImonedaServices = ImonedaServices;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            var result = _ImonedaServices.Get();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetById(int id)
        {
            var result = _ImonedaServices.GetById(id);
           return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(Moneda moneda)
        {
            var Fecha = DateTime.Now.ToString("dd/MM/yyyyy hh:mm:ss");
            moneda.FechaModificacion = Convert.ToDateTime(Fecha);
            moneda.UsuarioModifico = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            _ImonedaServices.Edit(moneda);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Insert(Moneda moneda)
        {
            var Fecha = DateTime.Now.ToString("dd/MM/yyyyy hh:mm:ss");
            moneda.UsuarioRegistro = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            moneda.FechaRegistro = Convert.ToDateTime(Fecha);
            moneda.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
            _ImonedaServices.Insert(moneda);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            _ImonedaServices.Delelte(id);
            return Json("OK", JsonRequestBehavior.AllowGet);

        }
    }
}