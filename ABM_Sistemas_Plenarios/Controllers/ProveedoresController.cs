﻿using Models;
using Services.ProveedoresServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class ProveedoresController : Controller
    {
        private readonly IProveedoresServices _IProveedorServices;
        public ProveedoresController(IProveedoresServices IProveedorServices)
        {
            _IProveedorServices = IProveedorServices;
        }
        public ActionResult Index()
        {
            return View();
        }

         public ActionResult Get()
        {
            var result = _IProveedorServices.Get().
                Where( x => x.IdEmpresa == ((Empresa)Session["Empresa"]).IdEmpresa ).
                Where(x=>x.Activo == 1);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet); ; 
        }
        public ActionResult GetById(int id)
        {
            var result = _IProveedorServices.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(Proveedor proveedor)
        {
            proveedor.UsuarioEdito = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            var fecha = DateTime.Now.ToString("dd/MM/yyyyy hh:mm:ss");
            proveedor.FechaModificacion = Convert.ToDateTime(fecha);
            var Proveedor = _IProveedorServices.ValidarCodigo(proveedor);
            if (Proveedor != null)
            {

                if (Proveedor.Codigo == Proveedor.Codigo)
                {
                    return Json("Codigo", JsonRequestBehavior.AllowGet);
                }
                else if (Proveedor.NombreProveedor == Proveedor.NombreProveedor)
                {
                    return Json("Nombre", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Documento", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                var result = _IProveedorServices.Edit(proveedor);
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Insert(Proveedor proveedor)
        {
            var fecha = DateTime.Now.ToString("dd/MM/yyyyy H:mm:ss");
            proveedor.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
            proveedor.UsuarioCreo = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            proveedor.FechaRegistro = Convert.ToDateTime(fecha);
            var Proveedor = _IProveedorServices.ValidarCodigo(proveedor);
            if (Proveedor != null)
            {

                if (Proveedor.Codigo == proveedor.Codigo)
                {
                    return Json("Codigo", JsonRequestBehavior.AllowGet);
                }
                else if (Proveedor.NombreProveedor == proveedor.NombreProveedor)
                {
                    return Json("Nombre", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Documento", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                var result = _IProveedorServices.Insert(proveedor);
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult Delete(int id)
        {
            _IProveedorServices.Delete(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }



       
    }
}