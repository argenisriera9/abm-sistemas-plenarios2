﻿
using Models;
using Services.PerfilesServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class PerfilesController : Controller
    {
        private readonly IPerfilesServices _IPerfilesServices;
        public PerfilesController(IPerfilesServices IPerfilesServices)
        {
            _IPerfilesServices = IPerfilesServices;
        }
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult Get()
        {
            var result = _IPerfilesServices.Get();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetById(int id)
        {
            var result = _IPerfilesServices.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit (UsuarioRol usuarioRol)
        {
            _IPerfilesServices.Edit(usuarioRol);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Insert(UsuarioRol usuarioRol)
        {
             _IPerfilesServices.Insert(usuarioRol);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id )
        {
            _IPerfilesServices.Delete(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetPerfiles()
        {
            var result = _IPerfilesServices.Get();
            return Json(result , JsonRequestBehavior.AllowGet);
        }


    }
}