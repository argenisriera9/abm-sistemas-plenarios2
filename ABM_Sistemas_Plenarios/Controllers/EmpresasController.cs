﻿using ABM_Sistemas_Plenarios.Models;
using Models;
using Services.EmpresasServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{

    
    public class EmpresasController : Controller
    {
        string result = "OK";
        
      
        private readonly IEmpresaServices _IempresasServices;

        public EmpresasController(IEmpresaServices IempresasServices)

        {
            _IempresasServices = IempresasServices;
        }
        public ActionResult Index()
        {
            return View();
        }

       
        public ActionResult Inicio(int IdEmpresa)
        {
            try
            {
                var result = _IempresasServices.GetById(IdEmpresa);
                Session["Empresa"] = result;

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
           

        }
        public ActionResult Get()
        {
            var result = _IempresasServices.Get();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetById(int id=0)
        {

            if(id == 0)
            {
                Empresa empresa = (Empresa)Session["Empresa"];
                id = empresa.IdEmpresa;
            }
            var result = _IempresasServices.GetById(id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Insert(Empresa empresa)
        {
            Usuario osuario = (Usuario)Session["Usuario"];
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            UploadImages file = (UploadImages)Session["Upload"];
            empresa.Logo = file.ImageFile.FileName;
            empresa.FechaRegistro = Convert.ToDateTime(fecha);
            empresa.UsuarioCreo = osuario.Nombre;
            empresa.Activo = 1;
            _IempresasServices.Insert(empresa);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InsertarFirst(Empresa empresa)
        {
            Usuario osuario = (Usuario)Session["Usuario"];
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            UploadImages file = (UploadImages)Session["Upload"];
            empresa.Logo = file.ImageFile.FileName;
            empresa.FechaRegistro = Convert.ToDateTime(fecha);
            empresa.UsuarioCreo = osuario.Nombre;


            int id =_IempresasServices.Insert(empresa);
            return Json(id, JsonRequestBehavior.AllowGet);

        
        }
        public ActionResult Edit(Empresa empresa)
        {
            _IempresasServices.Edit(empresa);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult delete(int id)
        {
            _IempresasServices.detele(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Upload( UploadImages file)
        {
            if (file != null)
            {
                try
                {
                    string Filename = Path.GetFileNameWithoutExtension(file.ImageFile.FileName);
                    string extension = Path.GetExtension(file.ImageFile.FileName);

                    if (extension == ".jpg" || extension == ".PNG" || extension==".png")

                    {
                        //Filename = Filename + DateTime.Now.ToString("ddmmyyyyy") + extension;
                        //file.path = "/ImagenesEmpresa/" + Filename + extension;
                        file.ImageFile.SaveAs(Server.MapPath("/ImagenesEmpresa/" + Filename + extension));


                        Session["Upload"] = file;


                        return Json("OK", JsonRequestBehavior.AllowGet);
                    }
                    else return Json("Error", JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    string r = "No se pudo cargar el archivo" + e;
                    return Json(r, JsonRequestBehavior.AllowGet);
                }
            }
            else return  Json("null", JsonRequestBehavior.AllowGet);

        }
    }
}