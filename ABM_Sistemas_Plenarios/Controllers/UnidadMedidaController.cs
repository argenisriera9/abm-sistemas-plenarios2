﻿using Models;
using Services.UnidadMedidaServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Controllers
{
    public class UnidadMedidaController : Controller
    {
        private readonly IUnidadMedidaService _IUnidadMedidadService;
        public UnidadMedidaController(IUnidadMedidaService IunidadMedidadService)
        {
            _IUnidadMedidadService = IunidadMedidadService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            var result = _IUnidadMedidadService.Get();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetById(int id)
        {
            var result = _IUnidadMedidadService.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Insert(UnidadMedida unidadMedida)
        {
            var Fecha = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
            unidadMedida.UsuarioRegistro = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            unidadMedida.IdEmpresa = ((Empresa)Session["Empresa"]).IdEmpresa;
            unidadMedida.FechaRegistro = Convert.ToDateTime(Fecha);
            var result = _IUnidadMedidadService.Insert(unidadMedida);
            return Json("OK", JsonRequestBehavior.AllowGet);
                
        }

        public ActionResult Edit(UnidadMedida unidadMedida)
        {
            var Fecha = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
            unidadMedida.UsuarioModifico = ((Usuario)Session["Usuario"]).Nombre + ((Usuario)Session["Usuario"]).Apellido;
            unidadMedida.FechaModificacion = Convert.ToDateTime(Fecha);
            var result = _IUnidadMedidadService.Edit(unidadMedida);

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            _IUnidadMedidadService.delete(id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}