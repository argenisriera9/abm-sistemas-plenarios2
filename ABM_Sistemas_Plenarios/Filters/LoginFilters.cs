﻿using ABM_Sistemas_Plenarios.Controllers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABM_Sistemas_Plenarios.Filters
{
    public class LoginFilters:ActionFilterAttribute
    {
        Usuario _usuario;
        Empresa _empresa;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);

                _usuario = (Usuario)HttpContext.Current.Session["usuario"];

                if (_usuario == null)
                {

                    if (filterContext.Controller is AccessController == false)
                    {
                        filterContext.HttpContext.Response.Redirect("/Access/Login");
                    }



                }

              



            }
            catch (Exception e)
            {
                filterContext.Result = new RedirectResult("~/Access/Login");
            }

        }
    }
}