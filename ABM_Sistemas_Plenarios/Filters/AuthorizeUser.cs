﻿using ConexionBD;
using Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersimosMVC.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple =false)]
    public class AuthorizeUser : AuthorizeAttribute
    {
        private Usuario oUsuario;
        private Conexion_BD db = new Conexion_BD();
        private int Flag;

        public AuthorizeUser(int flag = 0)
        {
            this.Flag = flag;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            try
            {
                oUsuario = (Usuario)HttpContext.Current.Session["usuario"];


                if (oUsuario != null)
                {
                    if (oUsuario.Flag != 1)
                    {

                        filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation");
                    }
                }
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Error/UnauthorizedOperation?operacion");
            }
        }

        //public string getNombreDeOperacion(int idOperacion)
        //{
        //    var ope = from op in db.operaciones
        //              where op.id == idOperacion
        //              select op.nombre;
        //    String nombreOperacion;
        //    try
        //    {
        //        nombreOperacion = ope.First();
        //    }
        //    catch (Exception)
        //    {
        //        nombreOperacion = "";
        //    }
        //    return nombreOperacion;
        //}

        //public string getNombreDelModulo(int? idModulo)
        //{
        //    var modulo = from m in db.modulo
        //                 where m.id == idModulo
        //                 select m.nombre;
        //    String nombreModulo;
        //    try
        //    {
        //        nombreModulo = modulo.First();
        //    }
        //    catch (Exception)
        //    {
        //        nombreModulo = "";
        //    }
        //    return nombreModulo;
        //}

    }
}