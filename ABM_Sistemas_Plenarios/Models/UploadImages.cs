﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABM_Sistemas_Plenarios.Models
{
    public class UploadImages
    {

        public string path { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }
    }
}