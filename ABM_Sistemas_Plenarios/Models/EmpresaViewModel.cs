﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABM_Sistemas_Plenarios.Models
{
    public class EmpresaViewModel
    {
        public int IdEmpresa { get; set; }

        public bool EsPrincipal { get; set; }

        public string Nombre { get; set; }

        public string Correo { get; set; }

        public string Web { get; set; }

        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string UsuarioElimino { get; set; }

        public string UsuarioActivo { get; set; }

        public string UsuarioCreo { get; set; }

        public string UsuarioEdito { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public DateTime? FechaEliminacion { get; set; }

        public string Logo { get; set; }

        public List<Empresa> Empresa { get; set; }


        

    }
}