﻿function clearProfImprov() {
    $("#textDescProfImprov").parent().attr("class", "form-group");
    $("#textDescProfImprov").parent().children("span").text("");

    $("#textInstituProfImprov").parent().attr("class", "form-group");
    $("#textInstituProfImprov").parent().children("span").text("");

    $("#selectTypeProfImprov").parent().attr("class", "form-group");
    $("#selectTypeProfImprov").parent().children("span").text("");

    $("#selectCountryProfImprov").parent().attr("class", "form-group");
    $("#selectCountryProfImprov").parent().children("span").text("");

    $("#seleStartProfImprov").parent().attr("class", "form-group");
    $("#seleStartProfImprov").parent().children("span").text("");

    $("#seleEndProfImprov").parent().attr("class", "form-group");
    $("#seleEndProfImprov").parent().children("span").text("");

    $("#yearStartProfImprov").parent().attr("class", "form-group");
    $("#yearStartProfImprov").parent().children("span").text("");

    $("#yearEndProfImprov").parent().attr("class", "form-group");
    $("#yearEndProfImprov").parent().children("span").text("");

}

function validateProfImprov() {
    var error = false;

    clearProfImprov();

    if ($("#selectTypeProfImprov").val() == null || $("#selectTypeProfImprov").val() == 0) {
        $("#selectTypeProfImprov").parent().attr("class", "form-group has-error");
        $("#selectTypeProfImprov").parent().children("span").text("Por favor seleccione un tipo");
        error = true;
    }

    if ($("#textDescProfImprov").val() == null || $("#textDescProfImprov").val().length == 0) {
        $("#textDescProfImprov").parent().attr("class", "form-group has-error");
        $("#textDescProfImprov").parent().children("span").text("Por favor especifique una descripción");
        error = true;
    }

    if ($("#textInstituProfImprov").val() == null || $("#textInstituProfImprov").val().length == 0) {
        $("#textInstituProfImprov").parent().attr("class", "form-group has-error");
        $("#textInstituProfImprov").parent().children("span").text("Por favor especifique una institución");
        error = true;
    }

    if ($("#selectCountryProfImprov").val() == null || $("#selectCountryProfImprov").val() == 0) {
        $("#selectCountryProfImprov").parent().attr("class", "form-group has-error");
        $("#selectCountryProfImprov").parent().children("span").text("Por favor seleccione un país");
        error = true;
    }


    if ($("#seleStartProfImprov").val() == null || $("#seleStartProfImprov").val() == 0) {
        $("#seleStartProfImprov").parent().attr("class", "form-group has-error");
        $("#seleStartProfImprov").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#yearStartProfImprov").val().length == 0 || $("#yearStartProfImprov").val() == null) {
        $("#yearStartProfImprov").parent().attr("class", "form-group has-error");
        $("#yearStartProfImprov").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#seleEndProfImprov").val() == null || $("#seleEndProfImprov").val() == 0) {
        $("#seleEndProfImprov").parent().attr("class", "form-group has-error");
        $("#seleEndProfImprov").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#yearEndProfImprov").val().length == 0 || $("#yearEndProfImprov").val() == null) {
        $("#yearEndProfImprov").parent().attr("class", "form-group has-error");
        $("#yearEndProfImprov").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#yearEndProfImprov").val().length != 0 &&  $("#seleStartProfImprov").val() != 0) {
        var start = "01-" + $("#seleStartProfImprov").val() + "-" + $("#yearStartProfImprov").val();
        var end = "01-" + $("#seleEndProfImprov").val() + "-" + $("#yearEndProfImprov").val();

        if (!validateDate(start, end)) {
            $("#seleStartProfImprov").parent().attr("class", "form-group has-error");
            $("#seleStartProfImprov").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function validateOthersDi() {
    var error = false;

    clearOthersDi();

    if ($("#seleTypeOthersDi").val() == null || $("#seleTypeOthersDi").val() == 0) {
        $("#seleTypeOthersDi").parent().attr("class", "form-group has-error");
        $("#seleTypeOthersDi").parent().children("span").text("Por favor seleccione el tipo de actividad");
        error = true;
    }

    return !error;
}

function clearOthersDi() {
    $("#seleTypeOthersDi").parent().attr("class", "form-group");
    $("#seleTypeOthersDi").parent().children("span").text("");

    $("#textObsOthersDi").parent().attr("class", "form-group");
    $("#textObsOthersDi").parent().children("span").text("");

}

function validateDistinct() {
    var error = false;

    clearDistinct();

    if ($("#seleTypeDistinct").val() == null || $("#seleTypeDistinct").val() == 0) {
        $("#seleTypeDistinct").parent().attr("class", "form-group has-error");
        $("#seleTypeDistinct").parent().children("span").text("Por favor seleccione el tipo de distinción");
        error = true;
    }

    if ($("#textDescDistinct").val() == null || $("#textDescDistinct").val().length == 0) {
        $("#textDescDistinct").parent().attr("class", "form-group has-error");
        $("#textDescDistinct").parent().children("span").text("Por favor especifique una descripción");
        error = true;
    }

    return !error;
}

function clearDistinct() {
    $("#seleTypeDistinct").parent().attr("class", "form-group");
    $("#seleTypeDistinct").parent().children("span").text("");

    $("#textDescDistinct").parent().attr("class", "form-group");
    $("#textDescDistinct").parent().children("span").text("");

}

function validateMedia() {
    var error = false;

    clearMedia();

    if ($("#seleTypeMedia").val() == null || $("#seleTypeMedia").val() == 0) {
        $("#seleTypeMedia").parent().attr("class", "form-group has-error");
        $("#seleTypeMedia").parent().children("span").text("Por favor seleccione el tipo de difusión");
        error = true;
    }

    if ($("#textDescMedia").val() == null || $("#textDescMedia").val().length == 0) {
        $("#textDescMedia").parent().attr("class", "form-group has-error");
        $("#textDescMedia").parent().children("span").text("Por favor especifique una descripción");
        error = true;
    }

    if ($("#textCountMedia").val() == null || $("#textCountMedia").val().length == 0) {
        $("#textCountMedia").parent().attr("class", "form-group has-error");
        $("#textCountMedia").parent().children("span").text("Por favor especifique una cantidad");
        error = true;
    }

    return !error;
}

function clearMedia() {
    $("#seleTypeMedia").parent().attr("class", "form-group");
    $("#seleTypeMedia").parent().children("span").text("");

    $("#textDescMedia").parent().attr("class", "form-group");
    $("#textDescMedia").parent().children("span").text("");

    $("#textCountMedia").parent().attr("class", "form-group");
    $("#textCountMedia").parent().children("span").text("");

}

function validateEvents() {
    var error = false;

    clearEvents();

    if ($("#seleTypeEvent").val() == null || $("#seleTypeEvent").val() == 0) {
        $("#seleTypeEvent").parent().attr("class", "form-group has-error");
        $("#seleTypeEvent").parent().children("span").text("Por favor seleccione un tipo de evento");
        error = true;
    }

    if ($("#textTitleEvent").val() == null || $("#textTitleEvent").val().length == 0) {
        $("#textTitleEvent").parent().attr("class", "form-group has-error");
        $("#textTitleEvent").parent().children("span").text("Por favor especifique el título del evento");
        error = true;
    }

    if ($("#textHoursEvent").val() == null || $("#textHoursEvent").val().length == 0) {
        $("#textHoursEvent").parent().attr("class", "form-group has-error");
        $("#textHoursEvent").parent().children("span").text("Por favor especifique el número de horas");
        error = true;
    }

    if ($("#seleStartEvent").val() == null || $("#seleStartEvent").val() == 0) {
        $("#seleStartEvent").parent().attr("class", "form-group has-error");
        $("#seleStartEvent").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textStartEvent").val().length == 0 || $("#textStartEvent").val() == null) {
        $("#textStartEvent").parent().attr("class", "form-group has-error");
        $("#textStartEvent").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#seleEndEvent").val() == null || $("#seleEndEvent").val() == 0) {
        $("#seleEndEvent").parent().attr("class", "form-group has-error");
        $("#seleEndEvent").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textEndEvent").val().length == 0 || $("#textEndEvent").val() == null) {
        $("#textEndEvent").parent().attr("class", "form-group has-error");
        $("#textEndEvent").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#textEndEvent").val().length != 0 && $("#seleStartEvent").val() != 0) {
        var start = "01-" + $("#seleStartEvent").val() + "-" + $("#textStartEvent").val();
        var end = "01-" + $("#seleEndEvent").val() + "-" + $("#textEndEvent").val();

        if (!validateDate(start, end)) {
            $("#seleStartEvent").parent().attr("class", "form-group has-error");
            $("#seleStartEvent").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearEvents() {
    $("#seleTypeEvent").parent().attr("class", "form-group");
    $("#seleTypeEvent").parent().children("span").text("");

    $("#textTitleEvent").parent().attr("class", "form-group");
    $("#textTitleEvent").parent().children("span").text("");

    $("#textHoursEvent").parent().attr("class", "form-group");
    $("#textHoursEvent").parent().children("span").text("");

    $("#seleStartEvent").parent().attr("class", "form-group");
    $("#seleStartEvent").parent().children("span").text("");

    $("#seleEndEvent").parent().attr("class", "form-group");
    $("#seleEndEvent").parent().children("span").text("");

    $("#textStartEvent").parent().attr("class", "form-group");
    $("#textStartEvent").parent().children("span").text("");

    $("#textEndEvent").parent().attr("class", "form-group");
    $("#textEndEvent").parent().children("span").text("");

    $("#checkActualEvent").prop('checked', false);

}

function validateConsul() {
    var error = false;

    clearConsul();

    if ($("#textDescConsul").val() == null || $("#textDescConsul").val().length == 0) {
        $("#textDescConsul").parent().attr("class", "form-group has-error");
        $("#textDescConsul").parent().children("span").text("Por favor especifique una descripción");
        error = true;
    }

    if ($("#textClienConsul").val() == null || $("#textClienConsul").val().length == 0) {
        $("#textClienConsul").parent().attr("class", "form-group has-error");
        $("#textClienConsul").parent().children("span").text("Por favor especifique una institución o cliente");
        error = true;
    }

    if ($("#textHoursConsul").val() == null || $("#textHoursConsul").val().length == 0) {
        $("#textHoursConsul").parent().attr("class", "form-group has-error");
        $("#textHoursConsul").parent().children("span").text("Por favor especifique el número de horas");
        error = true;
    }

    if ($("#seleStartConsul").val() == null || $("#seleStartConsul").val() == 0) {
        $("#seleStartConsul").parent().attr("class", "form-group has-error");
        $("#seleStartConsul").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textStartConsul").val().length == 0 || $("#textStartConsul").val() == null) {
        $("#textStartConsul").parent().attr("class", "form-group has-error");
        $("#textStartConsul").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#textEndConsul").val().length != 0 &&  $("#seleStartConsul").val() != 0) {
        var start = "01-" + $("#seleStartConsul").val() + "-" + $("#textStartConsul").val();
        var end = "01-" + $("#seleEndConsul").val() + "-" + $("#textEndConsul").val();

        if (!validateDate(start, end)) {
            $("#seleStartConsul").parent().attr("class", "form-group has-error");
            $("#seleStartConsul").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearConsul() {
    $("#textDescConsul").parent().attr("class", "form-group");
    $("#textDescConsul").parent().children("span").text("");

    $("#textClienConsul").parent().attr("class", "form-group");
    $("#textClienConsul").parent().children("span").text("");

    $("#textHoursConsul").parent().attr("class", "form-group");
    $("#textHoursConsul").parent().children("span").text("");

    $("#seleStartConsul").parent().attr("class", "form-group");
    $("#seleStartConsul").parent().children("span").text("");

    $("#seleEndConsul").parent().attr("class", "form-group");
    $("#seleEndConsul").parent().children("span").text("");

    $("#textStartConsul").parent().attr("class", "form-group");
    $("#textStartConsul").parent().children("span").text("");

    $("#textEndConsul").parent().attr("class", "form-group");
    $("#textEndConsul").parent().children("span").text("");

}

function validateSocialRespo() {
    var error = false;

    clearSocialRespo();

    if ($("#textTitleSocialRespo").val() == null || $("#textTitleSocialRespo").val().length == 0) {
        $("#textTitleSocialRespo").parent().attr("class", "form-group has-error");
        $("#textTitleSocialRespo").parent().children("span").text("Por favor especifique el título");
        error = true;
    }

    if ($("#seleStartSocialRespo").val() == null || $("#seleStartSocialRespo").val() == 0) {
        $("#seleStartSocialRespo").parent().attr("class", "form-group has-error");
        $("#seleStartSocialRespo").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textStartSocialRespo").val().length == 0 || $("#textStartSocialRespo").val() == null) {
        $("#textStartSocialRespo").parent().attr("class", "form-group has-error");
        $("#textStartSocialRespo").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#seleEndSocialRespo").val() == null || $("#seleEndSocialRespo").val() == 0) {
        $("#seleEndSocialRespo").parent().attr("class", "form-group has-error");
        $("#seleEndSocialRespo").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textEndSocialRespo").val().length == 0 || $("#textEndSocialRespo").val() == null) {
        $("#textEndSocialRespo").parent().attr("class", "form-group has-error");
        $("#textEndSocialRespo").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#textEndSocialRespo").val().length != 0 || $("#seleStartSocialRespo").val() != 0) {
        var start = "01-" + $("#seleStartSocialRespo").val() + "-" + $("#textStartSocialRespo").val();
        var end = "01-" + $("#seleEndSocialRespo").val() + "-" + $("#textEndSocialRespo").val();

        if (!validateDate(start, end)) {
            $("#seleStartSocialRespo").parent().attr("class", "form-group has-error");
            $("#seleStartSocialRespo").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearSocialRespo() {
    $("#textTitleSocialRespo").parent().attr("class", "form-group");
    $("#textTitleSocialRespo").parent().children("span").text("");

    $("#seleStartSocialRespo").parent().attr("class", "form-group");
    $("#seleStartSocialRespo").parent().children("span").text("");

    $("#seleEndSocialRespo").parent().attr("class", "form-group");
    $("#seleEndSocialRespo").parent().children("span").text("");

    $("#textStartSocialRespo").parent().attr("class", "form-group");
    $("#textStartSocialRespo").parent().children("span").text("");

    $("#textEndSocialRespo").parent().attr("class", "form-group");
    $("#textEndSocialRespo").parent().children("span").text("");

}

function validateInstDeve() {
    var error = false;

    clearInstDeve();

    if ($("#textTitleInstDeve").val() == null || $("#textTitleInstDeve").val().length == 0) {
        $("#textTitleInstDeve").parent().attr("class", "form-group has-error");
        $("#textTitleInstDeve").parent().children("span").text("Por favor especifique el título");
        error = true;
    }

    if ($("#selectCategoryInstDeve").val() == null || $("#selectCategoryInstDeve").val() == 0) {
        $("#selectCategoryInstDeve").parent().attr("class", "form-group has-error");
        $("#selectCategoryInstDeve").parent().children("span").text("Por favor seleccione una categoría");
        error = true;
    }

    if ($("#yearInstDeve").val().length == 0 && $("#seleMonthInstDeve").val() != 0) {
        $("#yearInstDeve").parent().attr("class", "form-group has-error");
        $("#yearInstDeve").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    return !error;
}

function clearInstDeve() {
    $("#textTitleInstDeve").parent().attr("class", "form-group");
    $("#textTitleInstDeve").parent().children("span").text("");

    $("#selectCategoryInstDeve").parent().attr("class", "form-group");
    $("#selectCategoryInstDeve").parent().children("span").text("");

    $("#seleMonthInstDeve").parent().attr("class", "form-group");
    $("#seleMonthInstDeve").parent().children("span").text("");

    $("#yearInstDeve").parent().attr("class", "form-group");
    $("#yearInstDeve").parent().children("span").text("");

    $("#textObsInstDeve").parent().attr("class", "form-group");
    $("#textObsInstDeve").parent().children("span").text("");

}

function validateCommitte() {
    var error = false;

    clearCommitte();

    if ($("#seleCommitte").val() == null || $("#seleCommitte").val() == 0) {
        $("#seleCommitte").parent().attr("class", "form-group has-error");
        $("#seleCommitte").parent().children("span").text("Por favor seleccione un comité");
        error = true;
    }

    if ($("#seleStartCommitte").val() == null || $("#seleStartCommitte").val() == 0) {
        $("#seleStartCommitte").parent().attr("class", "form-group has-error");
        $("#seleStartCommitte").parent().children("span").text("Por favor seleccione tipo de comité");
        error = true;
    }

    if ($('input:radio[name=typePartiCommitte]:checked').val() == undefined) {
        $("input:radio[name=typePartiCommitte]").parent().parent().parent().attr("class", "form-group has-error");
        $("input:radio[name=typePartiCommitte]").parent().parent().parent().children("span").text("Por favor seleccione una participación");
        error = true;
    }

    if ($("#seleStartCommitte").val() == null || $("#seleStartCommitte").val() == 0) {
        $("#seleStartCommitte").parent().attr("class", "form-group has-error");
        $("#seleStartCommitte").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textStartCommitte").val() == null || $("#textStartCommitte").val() == 0) {
        $("#textStartCommitte").parent().attr("class", "form-group has-error");
        $("#textStartCommitte").parent().children("span").text("Por favor indique un año");
        error = true;
    }

    if ($("#textEndCommitte").val().length != 0 && $("#seleEndCommitte").val() != 0) {
        var start = "01-" + $("#seleStartCommitte").val() + "-" + $("#textStartCommitte").val();
        var end = "01-" + $("#seleEndCommitte").val() + "-" + $("#textEndCommitte").val();

        if (!validateDate(start, end)) {
            $("#seleEndCommitte").parent().attr("class", "form-group has-error");
            $("#seleEndCommitte").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearCommitte() {
    $("#seleCommitte").parent().attr("class", "form-group");
    $("#seleCommitte").parent().children("span").text("");

    $("#typePartiCommitte").parent().attr("class", "form-group");
    $("#typePartiCommitte").parent().children("span").text("");

    $("#seleStartCommitte").parent().attr("class", "form-group");
    $("#seleStartCommitte").parent().children("span").text("");

    $("#textStartCommitte").parent().attr("class", "form-group");
    $("#textStartCommitte").parent().children("span").text("");

    $("#seleEndCommitte").parent().attr("class", "form-group");
    $("#seleEndCommitte").parent().children("span").text("");

    $("#textEndCommitte").parent().attr("class", "form-group");
    $("#textEndCommitte").parent().children("span").text("");

    $("input:radio[name=typePartiCommitte]").parent().parent().parent().attr("class", "form-group");
    $("input:radio[name=typePartiCommitte]").parent().parent().parent().children("span").text("");

}

function validateProf() {
    var error = false;

    clearProf();

    if ($("#textObsProf").val() == null || $("#textObsProf").val().length == 0) {
        $("#textObsProf").parent().attr("class", "form-group has-error");
        $("#textObsProf").parent().children("span").text("Por favor indique una descripción");
        error = true;
    }

    return !error;
}

function clearProf() {
    $("#textObsProf").parent().attr("class", "form-group");
    $("#textObsProf").parent().children("span").text("");
}

function validateExec() {
    var error = false;

    clearExec();

    if ($("#textObsExec").val() == null || $("#textObsExec").val().length == 0) {
        $("#textObsExec").parent().attr("class", "form-group has-error");
        $("#textObsExec").parent().children("span").text("Por favor indique una descripción");
        error = true;
    }

    return !error;
}

function clearExec() {
    $("#textObsExec").parent().attr("class", "form-group");
    $("#textObsExec").parent().children("span").text("");
}

function validateGov() {
    var error = false;

    clearGov();

    if ($("#seleOrgGov").val() == null || $("#seleOrgGov").val() == 0) {
        $("#seleOrgGov").parent().attr("class", "form-group has-error");
        $("#seleOrgGov").parent().children("span").text("Por favor seleccione un órgano de co-gobierno");
        error = true;
    }

    if ($("#seleStartGov").val() == null || $("#seleStartGov").val() == 0) {
        $("#seleStartGov").parent().attr("class", "form-group has-error");
        $("#seleStartGov").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textStartGov").val() == null || $("#textStartGov").val() == 0) {
        $("#textStartGov").parent().attr("class", "form-group has-error");
        $("#textStartGov").parent().children("span").text("Por favor indique un año");
        error = true;
    }


    if ($("#seleEndGov").val() == null || $("#seleEndGov").val() == 0) {
        $("#seleEndGov").parent().attr("class", "form-group has-error");
        $("#seleEndGov").parent().children("span").text("Por favor seleccione un mes");
        error = true;
    }

    if ($("#textEndGov").val() == null || $("#textEndGov").val() == 0) {
        $("#textEndGov").parent().attr("class", "form-group has-error");
        $("#textEndGov").parent().children("span").text("Por favor indique un año");
        error = true;
    }


    if ($("#textEndGov").val().length != 0 && $("#seleEndGov").val() != 0) {
        var start = "01-" + $("#seleStartGov").val() + "-" + $("#textStartGov").val();
        var end = "01-" + $("#seleEndGov").val() + "-" + $("#textEndGov").val();

        if (!validateDate(start, end)) {
            $("#seleEndGov").parent().attr("class", "form-group has-error");
            $("#seleEndGov").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearGov() {
    $("#seleOrgGov").parent().attr("class", "form-group");
    $("#seleOrgGov").parent().children("span").text("");

    $("#seleStartGov").parent().attr("class", "form-group");
    $("#seleStartGov").parent().children("span").text("");

    $("#textStartGov").parent().attr("class", "form-group");
    $("#textStartGov").parent().children("span").text("");

    $("#seleEndGov").parent().attr("class", "form-group");
    $("#seleEndGov").parent().children("span").text("");

    $("#textEndGov").parent().attr("class", "form-group");
    $("#textEndGov").parent().children("span").text("");

    $("#textObsGov").parent().attr("class", "form-group");
    $("#textObsGov").parent().children("span").text("");
}

function validateEditAcaCenter() {
    var error = false;

    clearEditAcaCenter();

    if ($("#seleEditAcaCenter").val() == null || $("#seleEditAcaCenter").val() == 0) {
        $("#seleEditAcaCenter").parent().attr("class", "form-group has-error");
        $("#seleEditAcaCenter").parent().children("span").text("Por favor seleccione un tipo de centro");
        error = true;
    }

    if ($("#seleStartEditAcaCenter").val() == null || $("#seleStartEditAcaCenter").val() == 0) {
        $("#seleStartEditAcaCenter").parent().attr("class", "form-group has-error");
        $("#seleStartEditAcaCenter").parent().children("span").text("Por favor seleccione una mes");
        error = true;
    }

    if ($("#textStartEditAcaCenter").val() == null || $("#textStartEditAcaCenter").val() == 0) {
        $("#textStartEditAcaCenter").parent().attr("class", "form-group has-error");
        $("#textStartEditAcaCenter").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    if ($("#seleEndEditAcaCenter").val() == 0) {
        $("#textEndEditAcaCenter").val("");
    }

    if (($("#textEndEditAcaCenter").val() == null || $("#textEndEditAcaCenter").val().length == 0) && $("#seleEndEditAcaCenter").val() != 0) {
        $("#seleEndEditAcaCenter").parent().attr("class", "form-group has-error");
        $("#seleEndEditAcaCenter").parent().children("span").text("Por favor indique un año de culiminación");
        error = true;
    }

    if ($("#textEndEditAcaCenter").val().length != 0 && $("#seleEndEditAcaCenter").val() != 0) {
        var start = "01-" + $("#seleStartEditAcaCenter").val() + "-" + $("#textStartEditAcaCenter").val();
        var end = "01-" + $("#seleEndEditAcaCenter").val() + "-" + $("#textEndEditAcaCenter").val();

        if (!validateDate(start, end)) {
            $("#seleEndEditAcaCenter").parent().attr("class", "form-group has-error");
            $("#seleEndEditAcaCenter").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearEditAcaCenter() {
    $("#seleEditAcaCenter").parent().attr("class", "form-group");
    $("#seleEditAcaCenter").parent().children("span").text("");

    $("#seleStartEditAcaCenter").parent().attr("class", "form-group");
    $("#seleStartEditAcaCenter").parent().children("span").text("");

    $("#textStartEditAcaCenter").parent().attr("class", "form-group");
    $("#textStartEditAcaCenter").parent().children("span").text("");

    $("#seleEndEditAcaCenter").parent().attr("class", "form-group");
    $("#seleEndEditAcaCenter").parent().children("span").text("");

    $("#textEndEditAcaCenter").parent().attr("class", "form-group");
    $("#textEndEditAcaCenter").parent().children("span").text("");

    $("#textObsEditAcaCenter").parent().attr("class", "form-group");
    $("#textObsEditAcaCenter").parent().children("span").text("");
}


function validateNewAcaCenter() {
    var error = false;

    clearNewAcaCenter();

    if ($("#seleAcaCenter").val() == null || $("#seleAcaCenter").val() == 0) {
        $("#seleAcaCenter").parent().attr("class", "form-group has-error");
        $("#seleAcaCenter").parent().children("span").text("Por favor seleccione un tipo de centro");
        error = true;
    }

    if ($("#seleStartAcaCenter").val() == null || $("#seleStartAcaCenter").val() == 0) {
        $("#seleStartAcaCenter").parent().attr("class", "form-group has-error");
        $("#seleStartAcaCenter").parent().children("span").text("Por favor seleccione una mes");
        error = true;
    }

    if ($("#textStartAcaCenter").val() == null || $("#textStartAcaCenter").val() == 0) {
        $("#textStartAcaCenter").parent().attr("class", "form-group has-error");
        $("#textStartAcaCenter").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    if ($("#seleEndAcaCenter").val() == 0) {
        $("#textEndAcaCenter").val("");
    }

    if (($("#textEndAcaCenter").val() == null || $("#textEndAcaCenter").val().length == 0) && $("#seleEndAcaCenter").val() != 0) {
        $("#seleEndAcaCenter").parent().attr("class", "form-group has-error");
        $("#seleEndAcaCenter").parent().children("span").text("Por favor indique un año de culiminación");
        error = true;
    }

    if ($("#textEndAcaCenter").val().length != 0 && $("#seleEndAcaCenter").val() != 0) {
        var start = "01-" + $("#seleStartAcaCenter").val() + "-" + $("#textStartAcaCenter").val();
        var end = "01-" + $("#seleEndAcaCenter").val() + "-" + $("#textEndAcaCenter").val();

        if (!validateDate(start, end)) {
            $("#seleEndAcaCenter").parent().attr("class", "form-group has-error");
            $("#seleEndAcaCenter").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearNewAcaCenter() {
    $("#seleAcaCenter").parent().attr("class", "form-group");
    $("#seleAcaCenter").parent().children("span").text("");

    $("#seleStartAcaCenter").parent().attr("class", "form-group");
    $("#seleStartAcaCenter").parent().children("span").text("");

    $("#textStartAcaCenter").parent().attr("class", "form-group");
    $("#textStartAcaCenter").parent().children("span").text("");

    $("#seleEndAcaCenter").parent().attr("class", "form-group");
    $("#seleEndAcaCenter").parent().children("span").text("");

    $("#textEndAcaCenter").parent().attr("class", "form-group");
    $("#textEndAcaCenter").parent().children("span").text("");

    $("#textObsAcaCenter").parent().attr("class", "form-group");
    $("#textObsAcaCenter").parent().children("span").text("");
}


function validateEditAcademic() {
    var error = false;

    clearEditAcademic();

    if ($("#seleProgramEditAcademic").val() == null || $("#seleProgramEditAcademic").val() == 0) {
        $("#seleProgramEditAcademic").parent().attr("class", "form-group has-error");
        $("#seleProgramEditAcademic").parent().children("span").text("Por favor seleccione un programa");
        error = true;
    }

    if ($("#seleStartEditAcademic").val() == null || $("#seleStartEditAcademic").val() == 0) {
        $("#seleStartEditAcademic").parent().attr("class", "form-group has-error");
        $("#seleStartEditAcademic").parent().children("span").text("Por favor seleccione una mes");
        error = true;
    }

    if ($("#textStartEditAcademic").val() == null || $("#textStartEditAcademic").val() == 0) {
        $("#textStartEditAcademic").parent().attr("class", "form-group has-error");
        $("#textStartEditAcademic").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    if ($("#seleEndEditAcademic").val() == 0) {
        $("#textEndEditAcademic").val("");
    }

    if (($("#textEndEditAcademic").val() == null || $("#textEndEditAcademic").val().length == 0) && $("#seleEndEditAcademic").val() != 0) {
        $("#seleEndEditAcademic").parent().attr("class", "form-group has-error");
        $("#seleEndEditAcademic").parent().children("span").text("Por favor indique un año de culiminación");
        error = true;
    }

    if ($("#textEndEditAcademic").val().length != 0 && $("#seleEndEditAcademic").val() != 0) {
        var start = "01-" + $("#seleStartEditAcademic").val() + "-" + $("#textStartEditAcademic").val();
        var end = "01-" + $("#seleEndEditAcademic").val() + "-" + $("#textEndEditAcademic").val();

        if (!validateDate(start, end)) {
            $("#seleEndEditAcademic").parent().attr("class", "form-group has-error");
            $("#seleEndEditAcademic").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearEditAcademic() {
    $("#seleProgramEditAcademic").parent().attr("class", "form-group");
    $("#seleProgramEditAcademic").parent().children("span").text("");

    $("#seleStartEditAcademic").parent().attr("class", "form-group");
    $("#seleStartEditAcademic").parent().children("span").text("");

    $("#textStartEditAcademic").parent().attr("class", "form-group");
    $("#textStartEditAcademic").parent().children("span").text("");

    $("#seleEndEditAcademic").parent().attr("class", "form-group");
    $("#seleEndEditAcademic").parent().children("span").text("");

    $("#textEndEditAcademic").parent().attr("class", "form-group");
    $("#textEndEditAcademic").parent().children("span").text("");

    $("#textObsEditAcademic").parent().attr("class", "form-group");
    $("#textObsEditAcademic").parent().children("span").text("");
}

function validateNewAcademic() {
    var error = false;

    clearNewAcademic();

    if ($("#seleProgramAcademic").val() == null || $("#seleProgramAcademic").val() == 0) {
        $("#seleProgramAcademic").parent().attr("class", "form-group has-error");
        $("#seleProgramAcademic").parent().children("span").text("Por favor seleccione un programa");
        error = true;
    }

    if ($("#seleStartAcademic").val() == null || $("#seleStartAcademic").val() == 0) {
        $("#seleStartAcademic").parent().attr("class", "form-group has-error");
        $("#seleStartAcademic").parent().children("span").text("Por favor seleccione una mes");
        error = true;
    }

    if ($("#textStartAcademic").val() == null || $("#textStartAcademic").val() == 0) {
        $("#textStartAcademic").parent().attr("class", "form-group has-error");
        $("#textStartAcademic").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    if ($("#seleEndAcademic").val() == 0) {
        $("#textEndAcademic").val("");
    }

    if (($("#textEndAcademic").val() == null || $("#textEndAcademic").val().length == 0) && $("#seleEndAcademic").val() != 0) {
        $("#seleEndAcademic").parent().attr("class", "form-group has-error");
        $("#seleEndAcademic").parent().children("span").text("Por favor indique un año de culiminación");
        error = true;
    }

    if ($("#textEndAcademic").val().length != 0 && $("#seleEndAcademic").val() != 0) {
        var start = "01-" + $("#seleStartAcademic").val() + "-" + $("#textStartAcademic").val();
        var end = "01-" + $("#seleEndAcademic").val() + "-" + $("#textEndAcademic").val();

        if (!validateDate(start, end)) {
            $("#seleEndAcademic").parent().attr("class", "form-group has-error");
            $("#seleEndAcademic").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    return !error;
}

function clearNewAcademic() {
    $("#seleProgramAcademic").parent().attr("class", "form-group");
    $("#seleProgramAcademic").parent().children("span").text("");

    $("#seleStartAcademic").parent().attr("class", "form-group");
    $("#seleStartAcademic").parent().children("span").text("");

    $("#textStartAcademic").parent().attr("class", "form-group");
    $("#textStartAcademic").parent().children("span").text("");

    $("#seleEndAcademic").parent().attr("class", "form-group");
    $("#seleEndAcademic").parent().children("span").text("");

    $("#textEndAcademic").parent().attr("class", "form-group");
    $("#textEndAcademic").parent().children("span").text("");

    $("#textObsAcademic").parent().attr("class", "form-group");
    $("#textObsAcademic").parent().children("span").text("");
}

function validateEditSpecialProjects() {
    var error = false;

    clearEditSpecialProjects();

    if ($("#textNameEditProject").val() == null || $("#textNameEditProject").val().length == 0) {
        $("#textNameEditProject").parent().attr("class", "form-group has-error");
        $("#textNameEditProject").parent().children("span").text("Por favor indique el título del proyecto");
        error = true;
    }

    if ($("#seleCategoryEditProject").val() == null || $("#seleCategoryEditProject").val() == 0) {
        $("#seleCategoryEditProject").parent().attr("class", "form-group has-error");
        $("#seleCategoryEditProject").parent().children("span").text("Por favor seleccione una categoría");
        error = true;
    }

    if (($("#textStartEditProject").val() == null || $("#textStartEditProject").val().length == 0) && $("#seleStartEditProject").val() != 0) {
        $("#seleStartEditProject").parent().attr("class", "form-group has-error");
        $("#seleStartEditProject").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    return !error;
}

function clearEditSpecialProjects() {
    $("#textNameEditProject").parent().attr("class", "form-group");
    $("#textNameEditProject").parent().children("span").text("");

    $("#seleCategoryEditProject").parent().attr("class", "form-group");
    $("#seleCategoryEditProject").parent().children("span").text("");

    $("#seleStartEditProject").parent().attr("class", "form-group");
    $("#seleStartEditProject").parent().children("span").text("");

    $("#textStartEditProject").parent().attr("class", "form-group");
    $("#textStartEditProject").parent().children("span").text("");

    $("#textObservationsEditProject").parent().attr("class", "form-group");
    $("#textObservationsEditProject").parent().children("span").text("");

}

function validateNewSpecialProjects() {
    var error = false;

    clearNewSpecialProjects();

    if ($("#textNameProject").val() == null || $("#textNameProject").val().length == 0) {
        $("#textNameProject").parent().attr("class", "form-group has-error");
        $("#textNameProject").parent().children("span").text("Por favor indique el título del proyecto");
        error = true;
    }

    if ($("#seleCategoryProject").val() == null || $("#seleCategoryProject").val() == 0) {
        $("#seleCategoryProject").parent().attr("class", "form-group has-error");
        $("#seleCategoryProject").parent().children("span").text("Por favor seleccione una categoría");
        error = true;
    }

    if (($("#textStartProject").val() == null || $("#textStartProject").val().length == 0) && $("#seleStartProject").val() != 0) {
        $("#seleStartProject").parent().attr("class", "form-group has-error");
        $("#seleStartProject").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    return !error;
}

function clearNewSpecialProjects() {
    $("#textNameProject").parent().attr("class", "form-group");
    $("#textNameProject").parent().children("span").text("");

    $("#seleCategoryProject").parent().attr("class", "form-group");
    $("#seleCategoryProject").parent().children("span").text("");

    $("#seleStartProject").parent().attr("class", "form-group");
    $("#seleStartProject").parent().children("span").text("");

    $("#textStartProject").parent().attr("class", "form-group");
    $("#textStartProject").parent().children("span").text("");

    $("#textObservationsProject").parent().attr("class", "form-group");
    $("#textObservationsProject").parent().children("span").text("");

}

function validateEditInvestigations() {
    var error = false;

    clearEditInvestigations();

    if ($("#seleAreaEditInves").val() == null || $("#seleAreaEditInves").val() == 0) {
        $("#seleAreaEditInves").parent().attr("class", "form-group has-error");
        $("#seleAreaEditInves").parent().children("span").text("Por favor seleccione un área");
        error = true;
    }

    if ($("#seleSubAreaEditInves").val() == null || $("#seleSubAreaEditInves").val() == 0) {
        $("#seleSubAreaEditInves").parent().attr("class", "form-group has-error");
        $("#seleSubAreaEditInves").parent().children("span").text("Por favor seleccione una sub área");
        error = true;
    }

    if ($("#seleStartEditInves").val() == null || $("#seleStartEditInves").val() == 0) {
        $("#seleStartEditInves").parent().attr("class", "form-group has-error");
        $("#seleStartEditInves").parent().children("span").text("Por favor seleccione una mes");
        error = true;
    }

    if ($("#textStartEditInves").val() == null || $("#textStartEditInves").val() == 0) {
        $("#seleStartEditInves").parent().attr("class", "form-group has-error");
        $("#seleStartEditInves").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    if ($("#seleEndEditInves").val() == 0) {
        $("#textEndEditInves").val("");
    }

    if (($("#textEndEditInves").val() == null || $("#textEndEditInves").val().length == 0) && $("#seleEndInves").val() != 0) {
        $("#seleEndEditInves").parent().attr("class", "form-group has-error");
        $("#seleEndEditInves").parent().children("span").text("Por favor indique un año de culiminación");
        error = true;
    }

    if ($("#textEndEditInves").val().length != 0 && $("#seleEndEditInves").val() != 0) {
        var start = "01-" + $("#seleStartEditInves").val() + "-" + $("#textStartEditInves").val();
        var end = "01-" + $("#seleEndEditInves").val() + "-" + $("#textEndEditInves").val();

        if (!validateDate(start, end)) {
            $("#seleEndEditInves").parent().attr("class", "form-group has-error");
            $("#seleEndEditInves").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    if ($("#textNameEditInves").val() == null || $("#textNameEditInves").val().length == 0) {
        $("#textNameEditInves").parent().attr("class", "form-group has-error");
        $("#textNameEditInves").parent().children("span").text("Por favor indique el nombre del proyecto");
        error = true;
    }

    if ($("#textRespoEditInves").val() == null || $("#textRespoEditInves").val().length == 0) {
        $("#textRespoEditInves").parent().attr("class", "form-group has-error");
        $("#textRespoEditInves").parent().children("span").text("Por favor indique un responsable");
        error = true;
    }

    return !error;
}

function clearEditInvestigations() {
    $("#seleAreaEditInves").parent().attr("class", "form-group");
    $("#seleAreaEditInves").parent().children("span").text("");

    $("#seleSubAreaEditInves").parent().attr("class", "form-group");
    $("#seleSubAreaEditInves").parent().children("span").text("");

    $("#textNameEditInves").parent().attr("class", "form-group");
    $("#textNameEditInves").parent().children("span").text("");

    $("#seleStartEditInves").parent().attr("class", "form-group");
    $("#seleStartEditInves").parent().children("span").text("");

    $("#seleEndEditInves").parent().attr("class", "form-group");
    $("#seleEndEditInves").parent().children("span").text("");

    $("#textRespoEditInves").parent().attr("class", "form-group");
    $("#textRespoEditInves").parent().children("span").text("");
}


function validateNewInvestigations() {
    var error = false;

    clearNewInvestigations();

    if ($("#seleAreaInves").val() == null || $("#seleAreaInves").val() == 0) {
        $("#seleAreaInves").parent().attr("class", "form-group has-error");
        $("#seleAreaInves").parent().children("span").text("Por favor seleccione un área");
        error = true;
    }

    if ($("#seleSubAreaInves").val() == null || $("#seleSubAreaInves").val() == 0) {
        $("#seleSubAreaInves").parent().attr("class", "form-group has-error");
        $("#seleSubAreaInves").parent().children("span").text("Por favor seleccione una sub área");
        error = true;
    }

    if ($("#seleStartInves").val() == null || $("#seleStartInves").val()  == 0) {
        $("#seleStartInves").parent().attr("class", "form-group has-error");
        $("#seleStartInves").parent().children("span").text("Por favor seleccione una mes");
        error = true;
    }

    if ($("#textStartInves").val() == null || $("#textStartInves").val() == 0) {
        $("#seleStartInves").parent().attr("class", "form-group has-error");
        $("#seleStartInves").parent().children("span").text("Por favor indique un año de inicio");
        error = true;
    }

    if ($("#seleEndInves").val() == 0) {
        $("#textEndInves").val("");
    }

    if ( ($("#textEndInves").val() == null || $("#textEndInves").val().length == 0) && $("#seleEndInves").val() != 0) {
        $("#seleEndInves").parent().attr("class", "form-group has-error");
        $("#seleEndInves").parent().children("span").text("Por favor indique un año de culiminación");
        error = true;
    }

    if ($("#textEndInves").val().length != 0 && $("#seleEndInves").val() != 0) {
        var start = "01-" + $("#seleStartInves").val() + "-" + $("#textStartInves").val();
        var end = "01-" + $("#seleEndInves").val() + "-" + $("#textEndInves").val();

        if (!validateDate(start, end)) {
            $("#seleEndInves").parent().attr("class", "form-group has-error");
            $("#seleEndInves").parent().children("span").text("Por favor verifique el rango de fechas");
            error = true;
        }
    }

    if ($("#textNameInves").val() == null || $("#textNameInves").val().length == 0) {
        $("#textNameInves").parent().attr("class", "form-group has-error");
        $("#textNameInves").parent().children("span").text("Por favor indique el nombre del proyecto");
        error = true;
    }

    if ($("#textRespoInves").val() == null || $("#textRespoInves").val().length == 0) {
        $("#textRespoInves").parent().attr("class", "form-group has-error");
        $("#textRespoInves").parent().children("span").text("Por favor indique un responsable");
        error = true;
    }

    return !error;
}

function clearNewInvestigations() {
    $("#seleAreaInves").parent().attr("class", "form-group");
    $("#seleAreaInves").parent().children("span").text("");

    $("#seleSubAreaInves").parent().attr("class", "form-group");
    $("#seleSubAreaInves").parent().children("span").text("");

    $("#textNameInves").parent().attr("class", "form-group");
    $("#textNameInves").parent().children("span").text("");

    $("#seleStartInves").parent().attr("class", "form-group");
    $("#seleStartInves").parent().children("span").text("");

    $("#seleEndInves").parent().attr("class", "form-group");
    $("#seleEndInves").parent().children("span").text("");

    $("#textRespoInves").parent().attr("class", "form-group");
    $("#textRespoInves").parent().children("span").text("");
}

function clearEditOthersPubs() {
    $("#seleAuthorshipTypeOthersPubsEdit").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeOthersPubsEdit").parent().children("span").text("");

    $("#textTitleOthersPubsEdit").parent().attr("class", "form-group");
    $("#textTitleOthersPubsEdit").parent().children("span").text("");

    $("#texEditorialOthersPubsEdit").parent().attr("class", "form-group");
    $("#texEditorialOthersPubsEdit").parent().children("span").text("");

    $("#textCountryOthersPubsEdit").parent().attr("class", "form-group");
    $("#textCountryOthersPubsEdit").parent().children("span").text("");

    $("#textPagesOthersPubsEdit").parent().attr("class", "form-group");
    $("#textPagesOthersPubsEdit").parent().children("span").text("");

    $("#textVolumenOthersPubsEdit").parent().attr("class", "form-group");
    $("#textVolumenOthersPubsEdit").parent().children("span").text("");

    $("#textNumberOthersPubsEdit").parent().attr("class", "form-group");
    $("#textNumberOthersPubsEdit").parent().children("span").text("");

    $("#textYearOthersPubsEdit").parent().attr("class", "form-group");
    $("#textYearOthersPubsEdit").parent().children("span").text("");

    $("#textObsOthersPubsEdit").parent().attr("class", "form-group");
    $("#textObsOthersPubsEdit").parent().children("span").text("");
}

function validateEditOthersPubs() {
    var error = false;

    clearEditOthersPubs();


    if ($("#seleAuthorshipTypeOthersPubsEdit").val() == null || $("#seleAuthorshipTypeOthersPubsEdit").val() == 0) {
        $("#seleAuthorshipTypeOthersPubsEdit").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeOthersPubsEdit").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if ($("#textTitleOthersPubsEdit").val() == null || $("#textTitleOthersPubsEdit").val().length == 0) {
        $("#textTitleOthersPubsEdit").parent().attr("class", "form-group has-error");
        $("#textTitleOthersPubsEdit").parent().children("span").text("Por favor indique el título");
        error = true;
    }

    if ($("#texEditorialOthersPubsEdit").val() == null || $("#texEditorialOthersPubsEdit").val().length == 0) {
        $("#texEditorialOthersPubsEdit").parent().attr("class", "form-group has-error");
        $("#texEditorialOthersPubsEdit").parent().children("span").text("Por favor indique una editorial o institución");
        error = true;
    }

    if ($("#textCountryOthersPubsEdit").val() == null || $("#textCountryOthersPubsEdit").val() == 0) {
        $("#textCountryOthersPubsEdit").parent().attr("class", "form-group has-error");
        $("#textCountryOthersPubsEdit").parent().children("span").text("Por favor indique un país");
        error = true;
    }

    if ($("#textYearOthersPubsEdit").val() == null || $("#textYearOthersPubsEdit").val().length == 0) {
        $("#textYearOthersPubsEdit").parent().attr("class", "form-group has-error");
        $("#textYearOthersPubsEdit").parent().children("span").text("Por favor indique el año de publicación");
        error = true;
    }

    return !error;
}

function validateNewOthersPubs() {
    var error = false;

    clearNewOthersPubs();


    if ($("#seleAuthorshipTypeOthersPubs").val() == null || $("#seleAuthorshipTypeOthersPubs").val() == 0) {
        $("#seleAuthorshipTypeOthersPubs").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeOthersPubs").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if ($("#textTitleOthersPubs").val() == null || $("#textTitleOthersPubs").val().length == 0) {
        $("#textTitleOthersPubs").parent().attr("class", "form-group has-error");
        $("#textTitleOthersPubs").parent().children("span").text("Por favor indique el título");
        error = true;
    }

    if ($("#texEditorialOthersPubs").val() == null || $("#texEditorialOthersPubs").val().length == 0) {
        $("#texEditorialOthersPubs").parent().attr("class", "form-group has-error");
        $("#texEditorialOthersPubs").parent().children("span").text("Por favor indique una editorial o institución");
        error = true;
    }

    if ($("#textCountryOthersPubs").val() == null || $("#textCountryOthersPubs").val() == 0) {
        $("#textCountryOthersPubs").parent().attr("class", "form-group has-error");
        $("#textCountryOthersPubs").parent().children("span").text("Por favor indique un país");
        error = true;
    }

    if ($("#textYearOthersPubs").val() == null || $("#textYearOthersPubs").val().length == 0) {
        $("#textYearOthersPubs").parent().attr("class", "form-group has-error");
        $("#textYearOthersPubs").parent().children("span").text("Por favor indique el año de publicación");
        error = true;
    }

    return !error;
}


function clearNewOthersPubs() {
    $("#seleAuthorshipTypeOthersPubs").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeOthersPubs").parent().children("span").text("");

    $("#textTitleOthersPubs").parent().attr("class", "form-group");
    $("#textTitleOthersPubs").parent().children("span").text("");

    $("#texEditorialOthersPubs").parent().attr("class", "form-group");
    $("#texEditorialOthersPubs").parent().children("span").text("");

    $("#textCountryOthersPubs").parent().attr("class", "form-group");
    $("#textCountryOthersPubs").parent().children("span").text("");

    $("#textPagesOthersPubs").parent().attr("class", "form-group");
    $("#textPagesOthersPubs").parent().children("span").text("");

    $("#textVolumenOthersPubs").parent().attr("class", "form-group");
    $("#textVolumenOthersPubs").parent().children("span").text("");

    $("#textNumberOthersPubs").parent().attr("class", "form-group");
    $("#textNumberOthersPubs").parent().children("span").text("");

    $("#textYearOthersPubs").parent().attr("class", "form-group");
    $("#textYearOthersPubs").parent().children("span").text("");

    $("#textObsOthersPubs").parent().attr("class", "form-group");
    $("#textObsOthersPubs").parent().children("span").text("");
}

function clearEditPublicationsObservations() {
    $("#textObsPublicationsEdit").parent().attr("class", "form-group");
    $("#textObsPublicationsEdit").parent().children("span").text("");

}

function validateEditPublicationsObservations() {
    var error = false;

    clearEditPublicationsObservations();

    if ($("#textObsPublicationsEdit").val() == null || $("#textObsPublicationsEdit").val().length == 0) {
        $("#textObsPublicationsEdit").parent().attr("class", "form-group has-error");
        $("#textObsPublicationsEdit").parent().children("span").text("Por favor agregue una observación");
        error = true;
    }
    return !error;
}


function clearNewPublicationsObservations() {
    $("#textObsPublications").parent().attr("class", "form-group");
    $("#textObsPublications").parent().children("span").text("");

}

function validateNewPublicationsObservations() {
    var error = false;

    clearNewPublicationsObservations();

    if ($("#textObsPublications").val() == null || $("#textObsPublications").val().length == 0) {
        $("#textObsPublications").parent().attr("class", "form-group has-error");
        $("#textObsPublications").parent().children("span").text("Por favor agregue una observación");
        error = true;
    }
    return !error;
}

function validateEditInnovations() {
    var error = false;

    clearEditInnovations();

    if ($("#seleInnovationTypeEdit").val() == null || $("#seleInnovationTypeEdit").val() == 0) {
        $("#seleInnovationTypeEdit").parent().attr("class", "form-group has-error");
        $("#seleInnovationTypeEdit").parent().children("span").text("Por favor seleccione un tipo de innovación");
        error = true;
    }

    if ($("#textObsInnovationEdit").val() == null || $("#textObsInnovationEdit").val().length == 0) {
        $("#textObsInnovationEdit").parent().attr("class", "form-group has-error");
        $("#textObsInnovationEdit").parent().children("span").text("Por favor agregue una observación");
        error = true;
    }

    return !error;
}



function clearEditInnovations() {
    $("#seleInnovationTypeEdit").parent().attr("class", "form-group");
    $("#seleInnovationTypeEdit").parent().children("span").text("");

    $("#textObsInnovationEdit").parent().attr("class", "form-group");
    $("#textObsInnovationEdit").parent().children("span").text("");

}


function validateNewInnovations() {
    var error = false;

    clearNewInnovations();

    if ($("#seleInnovationType").val() == null || $("#seleInnovationType").val() == 0) {
        $("#seleInnovationType").parent().attr("class", "form-group has-error");
        $("#seleInnovationType").parent().children("span").text("Por favor seleccione un tipo de innovación");
        error = true;
    }

    if ($("#textObsInnovation").val() == null || $("#textObsInnovation").val().length == 0) {
        $("#textObsInnovation").parent().attr("class", "form-group has-error");
        $("#textObsInnovation").parent().children("span").text("Por favor agregue una observación");
        error = true;
    }

    return !error;
}

function clearNewInnovations() {
    $("#seleInnovationType").parent().attr("class", "form-group");
    $("#seleInnovationType").parent().children("span").text("");

    $("#textObsInnovation").parent().attr("class", "form-group");
    $("#textObsInnovation").parent().children("span").text("");

}

function validateEditCases() {
    var error = false;

    clearEditCases();


    if ($("#seleAuthorshipTypeCasesEdit").val() == null || $("#seleAuthorshipTypeCasesEdit").val() == 0) {
        $("#seleAuthorshipTypeCasesEdit").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeCasesEdit").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if (parseInt($("#anio").val()) > 2009) {
        if ($("#selePublicationTypeCasesEdit").val() == null || $("#selePublicationTypeCasesEdit").val() == 0) {
            $("#selePublicationTypeCasesEdit").parent().attr("class", "form-group has-error");
            $("#selePublicationTypeCasesEdit").parent().children("span").text("Por favor seleccione la descripción de la publicación");
            error = true;
        }
    }

    if ($("#textCasesTitleEdit").val() == null || $("#textCasesTitleEdit").val().length == 0) {
        $("#textCasesTitleEdit").parent().attr("class", "form-group has-error");
        $("#textCasesTitleEdit").parent().children("span").text("Por favor indique el título");
        error = true;
    }

    if ($("#textCasesEditorialEdit").val() == null || $("#textCasesEditorialEdit").val().length == 0) {
        $("#textCasesEditorialEdit").parent().attr("class", "form-group has-error");
        $("#textCasesEditorialEdit").parent().children("span").text("Por favor indique la editorial");
        error = true;
    }

    if ($("#seleCountryTypeCasesEdit").val() == null || $("#seleCountryTypeCasesEdit").val() == 0) {
        $("#seleCountryTypeCasesEdit").parent().attr("class", "form-group has-error");
        $("#seleCountryTypeCasesEdit").parent().children("span").text("Por favor seleccione el país");
        error = true;
    }


    if ($("#textYearCasesEdit").val() == null || $("#textYearCasesEdit").val() == 0) {
        $("#textYearCasesEdit").parent().attr("class", "form-group has-error");
        $("#textYearCasesEdit").parent().children("span").text("Por favor indique el año");
        error = true;
    }


    if ($("#textAsignaturesCasesEdit").val() == null || $("#textAsignaturesCasesEdit").val().length == 0) {
        $("#textAsignaturesCasesEdit").parent().attr("class", "form-group has-error");
        $("#textAsignaturesCasesEdit").parent().children("span").text("Por favor indique las materias o cursos donde usó el caso");
        error = true;
    }

    return !error;
}

function clearEditCases() {
    $("#seleAuthorshipTypeCasesEdit").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeCasesEdit").parent().children("span").text("");

    $("#selePublicationTypeCasesEdit").parent().attr("class", "form-group");
    $("#selePublicationTypeCasesEdit").parent().children("span").text("");

    $("#textCasesTitleEdit").parent().attr("class", "form-group");
    $("#textCasesTitleEdit").parent().children("span").text("");

    $("#textCasesEditorialEdit").parent().attr("class", "form-group");
    $("#textCasesEditorialEdit").parent().children("span").text("");

    $("#seleCountryTypeCasesEdit").parent().attr("class", "form-group");
    $("#seleCountryTypeCasesEdit").parent().children("span").text("");

    $("#textYearCasesEdit").parent().attr("class", "form-group");
    $("#textYearCasesEdit").parent().children("span").text("");

    $("#textAsignaturesCasesEdit").parent().attr("class", "form-group");
    $("#textAsignaturesCasesEdit").parent().children("span").text("");

    $("#textObsCasesEdit").parent().attr("class", "form-group");
    $("#textObsCasesEdit").parent().children("span").text("");

}

function validateNewCases() {
    var error = false;

    clearNewCases();


    if ($("#seleAuthorshipTypeCases").val() == null || $("#seleAuthorshipTypeCases").val() == 0) {
        $("#seleAuthorshipTypeCases").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeCases").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if (parseInt($("#anio").val()) > 2009) {
        if ($("#selePublicationTypeCases").val() == null || $("#selePublicationTypeCases").val() == 0) {
            $("#selePublicationTypeCases").parent().attr("class", "form-group has-error");
            $("#selePublicationTypeCases").parent().children("span").text("Por favor seleccione la descripción de la publicación");
            error = true;
        }
    }

    if ($("#textCasesTitle").val() == null || $("#textCasesTitle").val().length == 0) {
        $("#textCasesTitle").parent().attr("class", "form-group has-error");
        $("#textCasesTitle").parent().children("span").text("Por favor indique el título");
        error = true;
    }

    if ($("#textCasesEditorial").val() == null || $("#textCasesEditorial").val().length == 0) {
        $("#textCasesEditorial").parent().attr("class", "form-group has-error");
        $("#textCasesEditorial").parent().children("span").text("Por favor indique la editorial");
        error = true;
    }

    if ($("#seleCountryTypeCases").val() == null || $("#seleCountryTypeCases").val() == 0) {
        $("#seleCountryTypeCases").parent().attr("class", "form-group has-error");
        $("#seleCountryTypeCases").parent().children("span").text("Por favor seleccione el país");
        error = true;
    }


    if ($("#textYearCases").val() == null || $("#textYearCases").val() == 0) {
        $("#textYearCases").parent().attr("class", "form-group has-error");
        $("#textYearCases").parent().children("span").text("Por favor indique el año");
        error = true;
    }


    if ($("#textAsignaturesCases").val() == null || $("#textAsignaturesCases").val().length == 0) {
        $("#textAsignaturesCases").parent().attr("class", "form-group has-error");
        $("#textAsignaturesCases").parent().children("span").text("Por favor indique las materias o cursos donde usó el caso");
        error = true;
    }

    return !error;
}

function clearNewCases() {
    $("#seleAuthorshipTypeCases").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeCases").parent().children("span").text("");

    $("#selePublicationTypeCases").parent().attr("class", "form-group");
    $("#selePublicationTypeCases").parent().children("span").text("");

    $("#textCasesTitle").parent().attr("class", "form-group");
    $("#textCasesTitle").parent().children("span").text("");

    $("#textCasesEditorial").parent().attr("class", "form-group");
    $("#textCasesEditorial").parent().children("span").text("");

    $("#seleCountryTypeCases").parent().attr("class", "form-group");
    $("#seleCountryTypeCases").parent().children("span").text("");

    $("#textYearCases").parent().attr("class", "form-group");
    $("#textYearCases").parent().children("span").text("");

    $("#textAsignaturesCases").parent().attr("class", "form-group");
    $("#textAsignaturesCases").parent().children("span").text("");

    $("#textObsCases").parent().attr("class", "form-group");
    $("#textObsCases").parent().children("span").text("");

}

function validateEditMagazines() {
    var error = false;

    clearEditMagazines();


    if ($("#seleAuthorshipTypeMagEdit").val() == null || $("#seleAuthorshipTypeMagEdit").val() == 0) {
        $("#seleAuthorshipTypeMagEdit").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeMagEdit").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if ($("#textMagazineTitleEdit").val() == null || $("#textMagazineTitleEdit").val().length == 0) {
        $("#textMagazineTitleEdit").parent().attr("class", "form-group has-error");
        $("#textMagazineTitleEdit").parent().children("span").text("Por favor indique el título");
        error = true;
    }

    if ($("#textMagazineNameEdit").val() == null || $("#textMagazineNameEdit").val().length == 0) {
        $("#textMagazineNameEdit").parent().attr("class", "form-group has-error");
        $("#textMagazineNameEdit").parent().children("span").text("Por favor indique el nombre");
        error = true;
    }

    if ($("#textYearMagazineEdit").val() == null || $("#textYearMagazineEdit").val() == 0) {
        $("#textYearMagazineEdit").parent().attr("class", "form-group has-error");
        $("#textYearMagazineEdit").parent().children("span").text("Por favor indique el año");
        error = true;
    }

    if ($("#textCountryMagazineEdit").val() == null || $("#textCountryMagazineEdit").val().length == 0) {
        $("#textCountryMagazineEdit").parent().attr("class", "form-group has-error");
        $("#textCountryMagazineEdit").parent().children("span").text("Por favor indique el país");
        error = true;
    }

    if ($("#textAuthorsMagazineEdit").val() == null || $("#textAuthorsMagazineEdit").val().length == 0) {
        $("#textAuthorsMagazineEdit").parent().attr("class", "form-group has-error");
        $("#textAuthorsMagazineEdit").parent().children("span").text("Por favor indique un autor");
        error = true;
    }

    if (parseInt($("#anio").val()) < 2010) {
        if ($("#seleMagazineTypeEdit").val() == null || $("#seleMagazineTypeEdit").val() == 0) {
            $("#seleMagazineTypeEdit").parent().attr("class", "form-group has-error");
            $("#seleMagazineTypeEdit").parent().children("span").text("Por favor seleccione el tipo de revista");
            error = true;
        }

        if ($("#selePublicationTypeMagEdit").val() == null || $("#selePublicationTypeMagEdit").val() == 0) {
            $("#selePublicationTypeMagEdit").parent().attr("class", "form-group has-error");
            $("#selePublicationTypeMagEdit").parent().children("span").text("Por favor seleccione el tipo de publicación");
            error = true;
        }

        if ($("#seleEditorialTypeMagEdit").val() == null || $("#seleEditorialTypeMagEdit").val() == 0) {
            $("#seleEditorialTypeMagEdit").parent().attr("class", "form-group has-error");
            $("#seleEditorialTypeMagEdit").parent().children("span").text("Por favor seleccione el tipo de editorial");
            error = true;
        }

    }

    if (parseInt($("#anio").val()) > 2009) {
        if ($("#selePublicationMagazineEdit").val() == null || $("#selePublicationMagazineEdit").val() == 0) {
            $("#selePublicationMagazineEdit").parent().attr("class", "form-group has-error");
            $("#selePublicationMagazineEdit").parent().children("span").text("Por favor seleccione la descripción de la publicación");
            error = true;
        }
    }

    return !error;
}

function clearEditMagazines() {
    $("#selePublicationMagazineEdit").parent().attr("class", "form-group");
    $("#selePublicationMagazineEdit").parent().children("span").text("");

    $("#seleMagazineTypeEdit").parent().attr("class", "form-group");
    $("#seleMagazineTypeEdit").parent().children("span").text("");

    $("#selePublicationTypeMagEdit").parent().attr("class", "form-group");
    $("#selePublicationTypeMagEdit").parent().children("span").text("");

    $("#seleEditorialTypeMagEdit").parent().attr("class", "form-group");
    $("#seleEditorialTypeMagEdit").parent().children("span").text("");

    $("#seleAuthorshipTypeMagEdit").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeMagEdit").parent().children("span").text("");

    $("#textMagazineTitleEdit").parent().attr("class", "form-group");
    $("#textMagazineTitleEdit").parent().children("span").text("");

    $("#textMagazineNameEdit").parent().attr("class", "form-group");
    $("#textMagazineNameEdit").parent().children("span").text("");

    $("#textVolumenEdit").parent().attr("class", "form-group");
    $("#textVolumenEdit").parent().children("span").text("");

    $("#textNumberEdit").parent().attr("class", "form-group");
    $("#textNumberEdit").parent().children("span").text("");

    $("#textYearMagazineEdit").parent().attr("class", "form-group");
    $("#textYearMagazineEdit").parent().children("span").text("");

    $("#textPagesMagazineEdit").parent().attr("class", "form-group");
    $("#textPagesMagazineEdit").parent().children("span").text("");

    $("#textCountryMagazineEdit").parent().attr("class", "form-group");
    $("#textCountryMagazineEdit").parent().children("span").text("");

    $("#textAuthorsMagazineEdit").parent().attr("class", "form-group");
    $("#textAuthorsMagazineEdit").parent().children("span").text("");

    $("#textObsMagazineEdit").parent().attr("class", "form-group");
    $("#textObsMagazineEdit").parent().children("span").text("");

}

function validateNewMagazines() {
    var error = false;

    clearNewMagazines();

    if ($("#seleAuthorshipTypeMag").val() == null || $("#seleAuthorshipTypeMag").val() == 0) {
        $("#seleAuthorshipTypeMag").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeMag").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if ($("#textMagazineTitle").val() == null || $("#textMagazineTitle").val().length == 0) {
        $("#textMagazineTitle").parent().attr("class", "form-group has-error");
        $("#textMagazineTitle").parent().children("span").text("Por favor indique el título");
        error = true;
    }

    if ($("#textMagazineName").val() == null || $("#textMagazineName").val().length == 0) {
        $("#textMagazineName").parent().attr("class", "form-group has-error");
        $("#textMagazineName").parent().children("span").text("Por favor indique el nombre");
        error = true;
    }

    if ($("#textYearMagazine").val() == null || $("#textYearMagazine").val() == 0) {
        $("#textYearMagazine").parent().attr("class", "form-group has-error");
        $("#textYearMagazine").parent().children("span").text("Por favor indique el año");
        error = true;
    }

    if ($("#textCountryMagazine").val() == null || $("#textCountryMagazine").val().length == 0) {
        $("#textCountryMagazine").parent().attr("class", "form-group has-error");
        $("#textCountryMagazine").parent().children("span").text("Por favor indique el país");
        error = true;
    }

    if ($("#textAuthorsMagazine").val() == null || $("#textAuthorsMagazine").val().length == 0) {
        $("#textAuthorsMagazine").parent().attr("class", "form-group has-error");
        $("#textAuthorsMagazine").parent().children("span").text("Por favor indique un autor");
        error = true;
    }

    if (parseInt($("#anio").val()) < 2010) {
        if ($("#seleMagazineType").val() == null || $("#seleMagazineType").val() == 0) {
            $("#seleMagazineType").parent().attr("class", "form-group has-error");
            $("#seleMagazineType").parent().children("span").text("Por favor seleccione el tipo de revista");
            error = true;
        }

        if ($("#selePublicationTypeMag").val() == null || $("#selePublicationTypeMag").val() == 0) {
            $("#selePublicationTypeMag").parent().attr("class", "form-group has-error");
            $("#selePublicationTypeMag").parent().children("span").text("Por favor seleccione el tipo de publicación");
            error = true;
        }

        if ($("#seleEditorialTypeMag").val() == null || $("#seleEditorialTypeMag").val() == 0) {
            $("#seleEditorialTypeMag").parent().attr("class", "form-group has-error");
            $("#seleEditorialTypeMag").parent().children("span").text("Por favor seleccione el tipo de editorial");
            error = true;
        }

    }

    if (parseInt($("#anio").val()) > 2009) {
        if ($("#selePublicationMagazine").val() == null || $("#selePublicationMagazine").val() == 0) {
            $("#selePublicationMagazine").parent().attr("class", "form-group has-error");
            $("#selePublicationMagazine").parent().children("span").text("Por favor seleccione la descripción de la publicación");
            error = true;
        }
    }

    return !error;
}

function clearNewMagazines() {
    $("#selePublicationMagazine").parent().attr("class", "form-group");
    $("#selePublicationMagazine").parent().children("span").text("");

    $("#seleMagazineType").parent().attr("class", "form-group");
    $("#seleMagazineType").parent().children("span").text("");

    $("#selePublicationTypeMag").parent().attr("class", "form-group");
    $("#selePublicationTypeMag").parent().children("span").text("");

    $("#seleEditorialTypeMag").parent().attr("class", "form-group");
    $("#seleEditorialTypeMag").parent().children("span").text("");

    $("#seleAuthorshipTypeMag").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeMag").parent().children("span").text("");

    $("#textMagazineTitle").parent().attr("class", "form-group");
    $("#textMagazineTitle").parent().children("span").text("");

    $("#textMagazineName").parent().attr("class", "form-group");
    $("#textMagazineName").parent().children("span").text("");

    $("#textVolumen").parent().attr("class", "form-group");
    $("#textVolumen").parent().children("span").text("");

    $("#textNumber").parent().attr("class", "form-group");
    $("#textNumber").parent().children("span").text("");

    $("#textYearMagazine").parent().attr("class", "form-group");
    $("#textYearMagazine").parent().children("span").text("");

    $("#textPagesMagazine").parent().attr("class", "form-group");
    $("#textPagesMagazine").parent().children("span").text("");

    $("#textCountryMagazine").parent().attr("class", "form-group");
    $("#textCountryMagazine").parent().children("span").text("");

    $("#textAuthorsMagazine").parent().attr("class", "form-group");
    $("#textAuthorsMagazine").parent().children("span").text("");

    $("#textObsMagazine").parent().attr("class", "form-group");
    $("#textObsMagazine").parent().children("span").text("");

}

function clearEditBooks() {
    $("#selePublicationTypeEdit").parent().attr("class", "form-group");
    $("#selePublicationTypeEdit").parent().children("span").text("");

    $("#seleAuthorshipTypeEdit").parent().attr("class", "form-group");
    $("#seleAuthorshipTypeEdit").parent().children("span").text("");

    $("#textBookTitleEdit").parent().attr("class", "form-group");
    $("#textBookTitleEdit").parent().children("span").text("");

    $("#textEditorialEdit").parent().attr("class", "form-group");
    $("#textEditorialEdit").parent().children("span").text("");

    $("#textBookYearEdit").parent().attr("class", "form-group");
    $("#textBookYearEdit").parent().children("span").text("");

    $("#textCountryEdit").parent().attr("class", "form-group");
    $("#textCountryEdit").parent().children("span").text("");

    $("#textAuthorsEdit").parent().attr("class", "form-group");
    $("#textAuthorsEdit").parent().children("span").text("");

    $("#textPagesEdit").parent().attr("class", "form-group");
    $("#textPagesEdit").parent().children("span").text("");

    $("#seleEditorialTypeEdit").parent().attr("class", "form-group");
    $("#seleEditorialTypeEdit").parent().children("span").text("");

    $("#seleBookTypeEdit").parent().attr("class", "form-group");
    $("#seleBookTypeEdit").parent().children("span").text("");

    $("#selePublicationDescriptionEdit").parent().attr("class", "form-group");
    $("#selePublicationDescriptionEdit").parent().children("span").text("");

    $("#textTitleChapterEdit").parent().attr("class", "form-group");
    $("#textTitleChapterEdit").parent().children("span").text("");

    $("#textAuthorChapterEdit").parent().attr("class", "form-group");
    $("#textAuthorChapterEdit").parent().children("span").text("");

    $("#textPagesChapterEdit").parent().attr("class", "form-group");
    $("#textPagesChapterEdit").parent().children("span").text("");

}

function validateEditBooks() {
    var error = false;

    clearEditBooks();

    if ($("#selePublicationTypeEdit").val() == null || $("#selePublicationTypeEdit").val().length == 0) {
        $("#selePublicationTypeEdit").parent().attr("class", "form-group has-error");
        $("#selePublicationTypeEdit").parent().children("span").text("Por favor seleccione un tipo de publicación");
        error = true;
    }


    if ($("#seleAuthorshipTypeEdit").val() == null || $("#seleAuthorshipTypeEdit").val().length == 0) {
        $("#seleAuthorshipTypeEdit").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipTypeEdit").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if ($("#textBookTitleEdit").val() == null || $("#textBookTitleEdit").val().length == 0) {
        $("#textBookTitleEdit").parent().attr("class", "form-group has-error");
        $("#textBookTitleEdit").parent().children("span").text("Por favor indique el título del libro");
        error = true;
    }

    if ($("#textEditorialEdit").val() == null || $("#textEditorialEdit").val().length == 0) {
        $("#textEditorialEdit").parent().attr("class", "form-group has-error");
        $("#textEditorialEdit").parent().children("span").text("Por favor indique una editorial");
        error = true;
    }


    if ($("#textBookYearEdit").val() == null || $("#textBookYearEdit").val().length == 0 && !$("#checkForthEdit").is(':checked')) {
        $("#textBookYearEdit").parent().attr("class", "form-group has-error");
        $("#textBookYearEdit").parent().children("span").text("Por favor indique un año de publicación");
        error = true;
    }

    if ($("#textCountryEdit").val() == null || $("#textCountryEdit").val().length == 0) {
        $("#textCountryEdit").parent().attr("class", "form-group has-error");
        $("#textCountryEdit").parent().children("span").text("Por favor indique una editorial");
        error = true;
    }

    if ($("#textAuthorsEdit").val() == null || $("#textAuthorsEdit").val().length == 0) {
        $("#textAuthorsEdit").parent().attr("class", "form-group has-error");
        $("#textAuthorsEdit").parent().children("span").text("Por favor indique un autor");
        error = true;
    }

    if ($("#textPagesEdit").val() == null || $("#textPagesEdit").val().length == 0) {
        $("#textPagesEdit").parent().attr("class", "form-group has-error");
        $("#textPagesEdit").parent().children("span").text("Por favor indique las páginas");
        error = true;
    }

    if (parseInt($("#anio").val()) < 2010) {
        if ($("#seleEditorialTypeEdit").val() == null || $("#seleEditorialTypeEdit").val().length == 0) {
            $("#seleEditorialTypeEdit").parent().attr("class", "form-group has-error");
            $("#seleEditorialTypeEdit").parent().children("span").text("Por favor seleccione el tipo de editorial");
            error = true;
        }

        if ($("#seleBookTypeEdit").val() == null || $("#seleBookTypeEdit").val().length == 0) {
            $("#seleBookTypeEdit").parent().attr("class", "form-group has-error");
            $("#seleBookTypeEdit").parent().children("span").text("Por favor seleccione el tipo de libro");
            error = true;
        }
    }

    if (parseInt($("#anio").val()) > 2009) {
        if ($("#selePublicationDescriptionEdit").val() == null || $("#selePublicationDescriptionEdit").val().length == 0) {
            $("#selePublicationDescriptionEdit").parent().attr("class", "form-group has-error");
            $("#selePublicationDescriptionEdit").parent().children("span").text("Por favor seleccione la descripción de la publicación");
            error = true;
        }
    }

    return !error;
}

function validateNewBooks() {
    var error = false;

    clearNewBooks();

    if ($("#selePublicationType").val() == null || $("#selePublicationType").val().length == 0) {
        $("#selePublicationType").parent().attr("class", "form-group has-error");
        $("#selePublicationType").parent().children("span").text("Por favor seleccione un tipo de publicación");
        error = true;
    }


    if ($("#seleAuthorshipType").val() == null || $("#seleAuthorshipType").val().length == 0) {
        $("#seleAuthorshipType").parent().attr("class", "form-group has-error");
        $("#seleAuthorshipType").parent().children("span").text("Por favor seleccione un tipo de autoría");
        error = true;
    }

    if ($("#textBookTitle").val() == null || $("#textBookTitle").val().length == 0) {
        $("#textBookTitle").parent().attr("class", "form-group has-error");
        $("#textBookTitle").parent().children("span").text("Por favor indique el título del libro");
        error = true;
    }

    if ($("#textEditorial").val() == null || $("#textEditorial").val().length == 0) {
        $("#textEditorial").parent().attr("class", "form-group has-error");
        $("#textEditorial").parent().children("span").text("Por favor indique una editorial");
        error = true;
    }


    if ($("#textBookYear").val() == null || $("#textBookYear").val().length == 0 && !$("#checkForth").is(':checked')) {
        $("#textBookYear").parent().attr("class", "form-group has-error");
        $("#textBookYear").parent().children("span").text("Por favor indique un año de publicación");
        error = true;
    }

    if ($("#textCountry").val() == null || $("#textCountry").val().length == 0) {
        $("#textCountry").parent().attr("class", "form-group has-error");
        $("#textCountry").parent().children("span").text("Por favor indique una editorial");
        error = true;
    }

    if ($("#textAuthors").val() == null || $("#textAuthors").val().length == 0) {
        $("#textAuthors").parent().attr("class", "form-group has-error");
        $("#textAuthors").parent().children("span").text("Por favor indique un autor");
        error = true;
    }

    if ($("#textPages").val() == null || $("#textPages").val().length == 0) {
        $("#textPages").parent().attr("class", "form-group has-error");
        $("#textPages").parent().children("span").text("Por favor indique las páginas");
        error = true;
    }

    if(parseInt($("#anio").val()) < 2010){
        if ($("#seleEditorialType").val() == null || $("#seleEditorialType").val().length == 0) {
            $("#seleEditorialType").parent().attr("class", "form-group has-error");
            $("#seleEditorialType").parent().children("span").text("Por favor seleccione el tipo de editorial");
            error = true;
        }

        if ($("#seleBookType").val() == null || $("#seleBookType").val().length == 0) {
            $("#seleBookType").parent().attr("class", "form-group has-error");
            $("#seleBookType").parent().children("span").text("Por favor seleccione el tipo de libro");
            error = true;
        }
    }

    if (parseInt($("#anio").val()) > 2009) {
        if ($("#selePublicationDescription").val() == null || $("#selePublicationDescription").val().length == 0) {
            $("#selePublicationDescription").parent().attr("class", "form-group has-error");
            $("#selePublicationDescription").parent().children("span").text("Por favor seleccione la descripción de la publicación");
            error = true;
        }
    }

    return !error;
}

function clearNewBooks() {
    $("#selePublicationType").parent().attr("class", "form-group");
    $("#selePublicationType").parent().children("span").text("");

    $("#seleAuthorshipType").parent().attr("class", "form-group");
    $("#seleAuthorshipType").parent().children("span").text("");

    $("#textBookTitle").parent().attr("class", "form-group");
    $("#textBookTitle").parent().children("span").text("");

    $("#textEditorial").parent().attr("class", "form-group");
    $("#textEditorial").parent().children("span").text("");

    $("#textBookYear").parent().attr("class", "form-group");
    $("#textBookYear").parent().children("span").text("");

    $("#textCountry").parent().attr("class", "form-group");
    $("#textCountry").parent().children("span").text("");

    $("#textAuthors").parent().attr("class", "form-group");
    $("#textAuthors").parent().children("span").text("");

    $("#textPages").parent().attr("class", "form-group");
    $("#textPages").parent().children("span").text("");

    $("#seleEditorialType").parent().attr("class", "form-group");
    $("#seleEditorialType").parent().children("span").text("");

    $("#seleBookType").parent().attr("class", "form-group");
    $("#seleBookType").parent().children("span").text("");

    $("#selePublicationDescription").parent().attr("class", "form-group");
    $("#selePublicationDescription").parent().children("span").text("");

    $("#textTitleChapter").parent().attr("class", "form-group");
    $("#textTitleChapter").parent().children("span").text("");

    $("#textAuthorChapter").parent().attr("class", "form-group");
    $("#textAuthorChapter").parent().children("span").text("");

    $("#textPagesChapter").parent().attr("class", "form-group");
    $("#textPagesChapter").parent().children("span").text("");

}

function clearEditObservationsTeaching() {
    $("#editObservationsTeacher").parent().attr("class", "form-group");
    $("#editObservationsTeacher").parent().children("span").text("");
}

function validateEditObservationsTeaching() {
    var error = false;

    clearEditObservationsTeaching();

    if ($("#editObservationsTeacher").val() == null || $("#editObservationsTeacher").val().length == 0) {
        $("#editObservationsTeacher").parent().attr("class", "form-group has-error");
        $("#editObservationsTeacher").parent().children("span").text("Por favor ingrese alguna observación");
        error = true;
    }

    return !error;
}

function clearNewObservationsTeaching() {
    $("#newObservationsTeaching").parent().attr("class", "form-group");
    $("#newObservationsTeaching").parent().children("span").text("");
}

function validateNewObservationsTeaching() {
    var error = false;

    clearNewObservationsTeaching();

    if ($("#newObservationsTeaching").val() == null || $("#newObservationsTeaching").val().length == 0) {
        $("#newObservationsTeaching").parent().attr("class", "form-group has-error");
        $("#newObservationsTeaching").parent().children("span").text("Por favor ingrese alguna observación");
        error = true;
    }

    return !error;
}

function clearEditValidateActivity() {
    $("#editTypeActivity").parent().attr("class", "form-group");
    $("#editTypeActivity").parent().children("span").text("");
}

function validateEditActivity() {
    var error = false;

    clearEditValidateActivity();

    if ($("#editTypeActivity").val() == null || $("#editTypeActivity").val().length == 0) {
        $("#editTypeActivity").parent().attr("class", "form-group has-error");
        $("editTypeActivity").parent().children("span").text("Por favor seleccione el tipo de actividad");
        error = true;
    }

    return !error;
}

function clearValidateActivity() {
    $("#newTypeActivity").parent().attr("class", "form-group");
    $("#newTypeActivity").parent().children("span").text("");
}

function validateActivity() {
    var error = false;

    clearValidateActivity();

    if ($("#newTypeActivity").val() == null || $("#newTypeActivity").val().length == 0) {
        $("#newTypeActivity").parent().attr("class", "form-group has-error");
        $("#newTypeActivity").parent().children("span").text("Por favor seleccione el tipo de actividad");
        error = true;
    }

    return !error;
}

function validateOthers() {
    var error = false;

    clearValidateOthers();

    if ($("#selectTeaching").val() == null || $("#selectTeaching").val().length == 0) {
        $("#selectTeaching").parent().attr("class", "form-group has-error");
        $("#selectTeaching").parent().children("span").text("Por favor seleccione la docencia");
        error = true;
    }

    if ($("#selectCountry").val() == null || $("#selectCountry").val().length == 0) {
        $("#selectCountry").parent().attr("class", "form-group has-error");
        $("#selectCountry").parent().children("span").text("Por favor seleccione el país");
        error = true;
    }

    if ($("#textCity").val() == null || $("#textCity").val().length == 0 || /^\s+$/.test($("#textCity").val())) {
        $("#textCity").parent().attr("class", "form-group has-error");
        $("#textCity").parent().children("span").text("Por favor ingrese el nombre de la ciudad");
        error = true;
    }

    if ($("#textInstitution").val() == null || $("#textInstitution").val().length == 0 || /^\s+$/.test($("#textInstitution").val())) {
        $("#textInstitution").parent().attr("class", "form-group has-error");
        $("#textInstitution").parent().children("span").text("Por favor ingrese el nombre de la institución");
        error = true;
    }

    if ($("#textProgram").val() == null || $("#textProgram").val().length == 0 || /^\s+$/.test($("#textProgram").val())) {
        $("#textProgram").parent().attr("class", "form-group has-error");
        $("#textProgram").parent().children("span").text("Por favor ingrese el programa o actividad");
        error = true;
    }

    if ($("#textAsignature").val() == null || $("#textAsignature").val().length == 0 || /^\s+$/.test($("#textAsignature").val())) {
        $("#textAsignature").parent().attr("class", "form-group has-error");
        $("#textAsignature").parent().children("span").text("Por favor ingrese la materia");
        error = true;
    }

    if ($("#textHours").val() == null || $("#textHours").val().length == 0 || /^\s+$/.test($("#textHours").val()) || !/^([0-9])*$/.test($("#textHours").val())) {
        $("#textHours").parent().attr("class", "form-group has-error");
        $("#textHours").parent().children("span").text("Por favor ingrese la cantidad de horas");
        error = true;
    }

    if ($("#textStart").val() == null || $("#textStart").val().length == 0 || /^\s+$/.test($("#textStart").val())) {
        $("#textStart").parent().attr("class", "form-group has-error");
        $("#textStart").parent().children("span").text("Por favor ingrese una fecha de inicio");
        error = true;
    }

    if ($("#textEnd").val() != "") {
        if (!validateDate($("#textStart").val(), $("#textEnd").val())) {
            $("#textStart").parent().attr("class", "form-group has-error");
            $("#textStart").parent().children("span").text("Por favor verifique el rango de fecha especificado");
            error = true;
        }
    }

    return !error;
}

function clearValidateOthers() {
    $("#selectTeaching").parent().attr("class", "form-group");
    $("#selectTeaching").parent().children("span").text("");

    $("#selectCountry").parent().attr("class", "form-group");
    $("#selectCountry").parent().children("span").text("");

    $("#textCity").parent().attr("class", "form-group");
    $("#textCity").parent().children("span").text("");

    $("#textInstitution").parent().attr("class", "form-group");
    $("#textInstitution").parent().children("span").text("");

    $("#textProgram").parent().attr("class", "form-group");
    $("#textProgram").parent().children("span").text("");

    $("#textAsignature").parent().attr("class", "form-group");
    $("#textAsignature").parent().children("span").text("");

    $("#textHours").parent().attr("class", "form-group");
    $("#textHours").parent().children("span").text("");

    $("#textStart").parent().attr("class", "form-group");
    $("#textStart").parent().children("span").text("");

}

function validateEditOthers() {
    var error = false;

    clearEditValidateOthers();

    if ($("#editTeaching").val() == null || $("#editTeaching").val().length == 0) {
        $("#editTeaching").parent().attr("class", "form-group has-error");
        $("#editTeaching").parent().children("span").text("Por favor seleccione la docencia");
        error = true;
    }

    if ($("#editCountry").val() == null || $("#editCountry").val().length == 0) {
        $("#editCountry").parent().attr("class", "form-group has-error");
        $("#editCountry").parent().children("span").text("Por favor seleccione la docencia");
        error = true;
    }

    if ($("#editCity").val() == null || $("#editCity").val().length == 0 || /^\s+$/.test($("#editCity").val())) {
        $("#editCity").parent().attr("class", "form-group has-error");
        $("#editCity").parent().children("span").text("Por favor ingrese el nombre de la ciudad");
        error = true;
    }

    if ($("#editInstitution").val() == null || $("#editInstitution").val().length == 0 || /^\s+$/.test($("#editInstitution").val())) {
        $("#editInstitution").parent().attr("class", "form-group has-error");
        $("#editInstitution").parent().children("span").text("Por favor ingrese el nombre de la institución");
        error = true;
    }

    if ($("#editProgram").val() == null || $("#editProgram").val().length == 0 || /^\s+$/.test($("#editProgram").val())) {
        $("#editProgram").parent().attr("class", "form-group has-error");
        $("#editProgram").parent().children("span").text("Por favor ingrese el programa o actividad");
        error = true;
    }

    if ($("#editAsignature").val() == null || $("#editAsignature").val().length == 0 || /^\s+$/.test($("#editAsignature").val())) {
        $("#editAsignature").parent().attr("class", "form-group has-error");
        $("#editAsignature").parent().children("span").text("Por favor ingrese la materia");
        error = true;
    }

    if ($("#editHours").val() == null || $("#editHours").val().length == 0 || /^\s+$/.test($("#editHours").val()) || !/^([0-9])*$/.test($("#textHours").val())) {
        $("#editHours").parent().attr("class", "form-group has-error");
        $("#editHours").parent().children("span").text("Por favor ingrese la cantidad de horas");
        error = true;
    }

    if ($("#editStart").val() == null || $("#editStart").val().length == 0 || /^\s+$/.test($("#editStart").val())) {
        $("#editStart").parent().attr("class", "form-group has-error");
        $("#editStart").parent().children("span").text("Por favor ingrese una fecha de inicio");
        error = true;
    }

    if ($("#editEnd").val() != "") {
        if (!validateDate($("#editStart").val(), $("#editEnd").val())) {
            $("#editStart").parent().attr("class", "form-group has-error");
            $("#editStart").parent().children("span").text("Por favor verifique el rango de fecha especificado");
            error = true;
        }
    }

    return !error;
}

function clearEditValidateOthers() {
    $("#editTeaching").parent().attr("class", "form-group");
    $("#editTeaching").parent().children("span").text("");

    $("#editCountry").parent().attr("class", "form-group");
    $("#editCountry").parent().children("span").text("");

    $("#editCity").parent().attr("class", "form-group");
    $("#editCity").parent().children("span").text("");

    $("#editInstitution").parent().attr("class", "form-group");
    $("#editInstitution").parent().children("span").text("");

    $("#editProgram").parent().attr("class", "form-group");
    $("#editProgram").parent().children("span").text("");

    $("#editAsignature").parent().attr("class", "form-group");
    $("#editAsignature").parent().children("span").text("");

    $("#editHours").parent().attr("class", "form-group");
    $("#editHours").parent().children("span").text("");

    $("#editStart").parent().attr("class", "form-group");
    $("#editStart").parent().children("span").text("");

}
