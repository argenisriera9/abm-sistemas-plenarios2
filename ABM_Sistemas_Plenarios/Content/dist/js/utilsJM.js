﻿//thanks: http://javascript.nwbox.com/cursor_position/

function isAlphaSpace(evt, object,maxLenght) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[a-zA-Z0-9\sáéóúñÁÉÍÓÚÑ]/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    } else {
        //Maximo de caracteres
        if (object.value.length + 1 > maxLenght) {
            object.value = object.value.slice(0, object.maxLength);
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
    }
}

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if (number.length > 1 && charCode == 46) {
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
        return false;
    }
}

function validateDate(dateStart, dateEnd) {
    valuesStart = dateStart.split("-");
    valuesEnd = dateEnd.split("-");

    // Verificamos que la fecha no sea posterior a la actual
    var Start = new Date(valuesStart[2], (valuesStart[1] - 1), valuesStart[0]);
    var End = new Date(valuesEnd[2], (valuesEnd[1] - 1), valuesEnd[0]);
    if (Start > End) {
        return false;
    }
    return true;
}

function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

function isNumeric(evt,object) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    } else {
        //Maximo de caracteres
        if (object.value.length + 1 > object.maxLength) {
            object.value = object.value.slice(0, object.maxLength);
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }
}