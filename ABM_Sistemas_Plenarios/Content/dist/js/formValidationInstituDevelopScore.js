﻿function validateCommis() {
    var error = false;

    clearCommis();

    if ($("#selectTypeCommis").val() == null || $("#selectTypeCommis").val() == 0) {
        $("#selectTypeCommis").parent().attr("class", "form-group has-error");
        $("#selectTypeCommis").parent().children("span").text("Por favor seleccione un tipo de comisión");
        error = true;
    }

    if ($("#commisScore").val() == null || $("#commisScore").val().length == 0) {
        $("#commisScore").parent().attr("class", "form-group has-error");
        $("#commisScore").parent().children("span").text("Por favor especifique una intensidad");
        error = true;
    }

    return !error;
}

function clearCommis() {
    $("#selectTypeCommis").parent().attr("class", "form-group");
    $("#selectTypeCommis").parent().children("span").text("");

    $("#commisScore").parent().attr("class", "form-group");
    $("#commisScore").parent().children("span").text("");

}

function validateOActi() {
    var error = false;

    clearOActi();

    if ($("#selectTypeOActi").val() == null || $("#selectTypeOActi").val() == 0) {
        $("#selectTypeOActi").parent().attr("class", "form-group has-error");
        $("#selectTypeOActi").parent().children("span").text("Por favor seleccione un tipo de otra actividad");
        error = true;
    }

    if ($("#oActiScore").val() == null || $("#oActiScore").val().length == 0) {
        $("#oActiScore").parent().attr("class", "form-group has-error");
        $("#oActiScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}


function clearOActi() {
    $("#selectTypeOActi").parent().attr("class", "form-group");
    $("#selectTypeOActi").parent().children("span").text("");

    $("#oActiScore").parent().attr("class", "form-group");
    $("#oActiScore").parent().children("span").text("");

}

function validatePro() {
    var error = false;

    clearPro();

    if ($("#selectTypeActi").val() == null || $("#selectTypeActi").val() == 0) {
        $("#selectTypeActi").parent().attr("class", "form-group has-error");
        $("#selectTypeActi").parent().children("span").text("Por favor seleccione un tipo de actividad");
        error = true;
    }

    if ($("#selectTypeIndo").val() == null || $("#selectTypeIndo").val() == "") {
        $("#selectTypeIndo").parent().attr("class", "form-group has-error");
        $("#selectTypeIndo").parent().children("span").text("Por favor seleccione una índole");
        error = true;
    }

    if ($("#proScore").val() == null || $("#proScore").val().length == 0) {
        $("#proScore").parent().attr("class", "form-group has-error");
        $("#proScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}

function clearPro() {
    $("#selectTypeAct").parent().attr("class", "form-group");
    $("#selectTypeAct").parent().children("span").text("");


    $("#selectTypeIndo").parent().attr("class", "form-group");
    $("#selectTypeIndo").parent().children("span").text("");

    $("#proScore").parent().attr("class", "form-group");
    $("#proScore").parent().children("span").text("");

}


function validateGov() {
    var error = false;

    clearGov();

    if ($("#selectTypeGov").val() == null || $("#selectTypeGov").val() == 0) {
        $("#selectTypeGov").parent().attr("class", "form-group has-error");
        $("#selectTypeGov").parent().children("span").text("Por favor seleccione un tipo de órgano co gobierno");
        error = true;
    }

    if ($("#govScore").val() == null || $("#govScore").val().length == 0) {
        $("#govScore").parent().attr("class", "form-group has-error");
        $("#govScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}


function clearGov() {
    $("#selectTypeGov").parent().attr("class", "form-group");
    $("#selectTypeGov").parent().children("span").text("");

    $("#govScore").parent().attr("class", "form-group");
    $("#govScore").parent().children("span").text("");

}

function validateEvent() {
    var error = false;

    clearEvent();

    if ($("#selectTypeParti").val() == null || $("#selectTypeParti").val() == 0) {
        $("#selectTypeParti").parent().attr("class", "form-group has-error");
        $("#selectTypeParti").parent().children("span").text("Por favor seleccione un tipo de participación");
        error = true;
    }

    if ($("#selectTypeEvent").val() == null || $("#selectTypeEvent").val() == 0) {
        $("#selectTypeEvent").parent().attr("class", "form-group has-error");
        $("#selectTypeEvent").parent().children("span").text("Por favor seleccione un tipo de evento");
        error = true;
    }

    if ($("#eventScore").val() == null || $("#eventScore").val().length == 0) {
        $("#eventScore").parent().attr("class", "form-group has-error");
        $("#eventScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}


function clearEvent() {
    $("#selectTypeEvent").parent().attr("class", "form-group");
    $("#selectTypeEvent").parent().children("span").text("");


    $("#selectTypeParti").parent().attr("class", "form-group");
    $("#selectTypeParti").parent().children("span").text("");

    $("#eventScore").parent().attr("class", "form-group");
    $("#eventScore").parent().children("span").text("");

}

function validateDistin() {
    var error = false;

    clearDistin();

    if ($("#selectTypeDistin").val() == null || $("#selectTypeDistin").val() == 0) {
        $("#selectTypeDistin").parent().attr("class", "form-group has-error");
        $("#selectTypeDistin").parent().children("span").text("Por favor seleccione un tipo de distinción");
        error = true;
    }

    if ($("#distinScore").val() == null || $("#distinScore").val().length == 0) {
        $("#distinScore").parent().attr("class", "form-group has-error");
        $("#distinScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}


function clearDistin() {
    $("#selectTypeDistin").parent().attr("class", "form-group");
    $("#selectTypeDistin").parent().children("span").text("");

    $("#distinScore").parent().attr("class", "form-group");
    $("#distinScore").parent().children("span").text("");

}

function validateEventsCoo() {
    var error = false;

    clearEventsCoo();

    if ($("#selectTypeEventsCoo").val() == null || $("#selectTypeEventsCoo").val() == 0) {
        $("#selectTypeEventsCoo").parent().attr("class", "form-group has-error");
        $("#selectTypeEventsCoo").parent().children("span").text("Por favor seleccione un tipo de evento");
        error = true;
    }

    if ($("#eventsCooScore").val() == null || $("#eventsCooScore").val().length == 0) {
        $("#eventsCooScore").parent().attr("class", "form-group has-error");
        $("#eventsCooScore").parent().children("span").text("Por favor especifique una intensidad");
        error = true;
    }

    return !error;
}


function clearEventsCoo() {
    $("#selectTypeEventsCoo").parent().attr("class", "form-group");
    $("#selectTypeEventsCoo").parent().children("span").text("");

    $("#eventsCooScore").parent().attr("class", "form-group");
    $("#eventsCooScore").parent().children("span").text("");

}

function validateAcademic() {
    var error = false;

    clearAcademic();

    if ($("#selectAcademicCenters").val() == null || $("#selectAcademicCenters").val() == 0) {
        $("#selectAcademicCenters").parent().attr("class", "form-group has-error");
        $("#selectAcademicCenters").parent().children("span").text("Por favor seleccione un centro");
        error = true;
    }

    if ($("#selectAcademicParti").val() == null || $("#selectAcademicParti").val() == 0) {
        $("#selectAcademicParti").parent().attr("class", "form-group has-error");
        $("#selectAcademicParti").parent().children("span").text("Por favor seleccione un tipo de participación");
        error = true;
    }

    if ($("#academicScore").val() == null || $("#academicScore").val().length == 0) {
        $("#academicScore").parent().attr("class", "form-group has-error");
        $("#academicScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}


function clearAcademic() {
    $("#selectAcademicCenters").parent().attr("class", "form-group");
    $("#selectAcademicCenters").parent().children("span").text("");

    $("#selectAcademicParti").parent().attr("class", "form-group");
    $("#selectAcademicParti").parent().children("span").text("");

    $("#academicScore").parent().attr("class", "form-group");
    $("#academicScore").parent().children("span").text("");

}

function validateCommitt() {
    var error = false;

    clearCommitt();

    if ($("#selectWinCommittYear").val() == null || $("#selectWinCommittYear").val() == 0) {
        $("#selectWinCommittYear").parent().attr("class", "form-group has-error");
        $("#selectWinCommittYear").parent().children("span").text("Por favor seleccione un año");
        error = true;
    }

    if ($("#selectCommitt").val() == null || $("#selectCommitt").val() == 0) {
        $("#selectCommitt").parent().attr("class", "form-group has-error");
        $("#selectCommitt").parent().children("span").text("Por favor seleccione un centro");
        error = true;
    }

    if ($("#committScore").val() == null || $("#committScore").val().length == 0) {
        $("#committScore").parent().attr("class", "form-group has-error");
        $("#committScore").parent().children("span").text("Por favor especifique una intensidad");
        error = true;
    }

    return !error;
}

function clearCommitt() {
    $("#selectWinCommittYear").parent().attr("class", "form-group");
    $("#selectWinCommittYear").parent().children("span").text("");

    $("#selectCommitt").parent().attr("class", "form-group");
    $("#selectCommitt").parent().children("span").text("");

    $("#committScore").parent().attr("class", "form-group");
    $("#committScore").parent().children("span").text("");

}

function validateCenters() {
    var error = false;

    clearCenters();

    if ($("#selectCenters").val() == null || $("#selectCenters").val() == 0) {
        $("#selectCenters").parent().attr("class", "form-group has-error");
        $("#selectCenters").parent().children("span").text("Por favor seleccione un centro");
        error = true;
    }

    if ($("#centersScore").val() == null || $("#centersScore").val().length == 0) {
        $("#centersScore").parent().attr("class", "form-group has-error");
        $("#centersScore").parent().children("span").text("Por favor especifique un puntaje");
        error = true;
    }

    return !error;
}

function clearCenters() {
    $("#selectCenters").parent().attr("class", "form-group");
    $("#selectCenters").parent().children("span").text("");

    $("#centersScore").parent().attr("class", "form-group");
    $("#centersScore").parent().children("span").text("");

}