﻿function validateCalification() {
    var error = false;

    clearCalification();

    if ($("#selectActivityCalification").val() == null || $("#selectActivityCalification").val() == 0) {
        $("#selectActivityCalification").parent().attr("class", "form-group has-error");
        $("#selectActivityCalification").parent().children("span").text("Por favor seleccione una actividad");
        error = true;
    }

    if ($("#selectDeliveryCalification").val() == null || $("#selectDeliveryCalification").val() == 0) {
        $("#selectDeliveryCalification").parent().attr("class", "form-group has-error");
        $("#selectDeliveryCalification").parent().children("span").text("Por favor seleccione una calificación");
        error = true;
    }

    return !error;
}

function clearCalification() {
    $("#selectActivityCalification").parent().attr("class", "form-group");
    $("#selectActivityCalification").parent().children("span").text("");

    $("#selectDeliveryCalification").parent().attr("class", "form-group");
    $("#selectDeliveryCalification").parent().children("span").text("");

}

function clearMaterial() {
    $("#selectActivityMaterial").parent().attr("class", "form-group");
    $("#selectActivityMaterial").parent().children("span").text("");
}

function validateMaterial() {
    var error = false;

    clearMaterial();

    if ($("#selectActivityMaterial").val() == null || $("#selectActivityMaterial").val() == 0) {
        $("#selectActivityMaterial").parent().attr("class", "form-group has-error");
        $("#selectActivityMaterial").parent().children("span").text("Por favor seleccione una actividad");
        error = true;
    }

    return !error;
}