﻿/*
Funcion:        location_bar
Descripcion:    Muestra ubicacion del modulo/opcion en que se encuentra ubicado el usuario
                Usa el formato MODULO|OPCION 1|OPCION 2| ... OPCION N
                Ejemplo: Adminostrador|Gestión de opciones
*/
function location_bar(location) {
    var html = "";
    var aBar = location.split("|");
    if (aBar[0] != "") {
        html += "<li><a href=\"#\"><i class=\"fa fa-dashboard\"></i>" + aBar[0] + "</a></li>";
        if (aBar.length > 1) {
            for (i = 1; i < aBar.length; i++) {
                html += "<li class=\"active\">" + aBar[i] + "</li>\n";
            }
        }
        $("#dashboard").html(html);
    }
}

function parseJsonDate(jsonDate) {

    var dateString = jsonDate.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    month = '0'.repeat(2 - month.toString().length) + month;
    var day = '0'.repeat(2 - currentTime.getDate().toString().length) + currentTime.getDate();
    var year = currentTime.getFullYear();
    var hh = '0'.repeat(2 - currentTime.getHours().toString().length) + currentTime.getHours();
    var mm = '0'.repeat(2 - currentTime.getMinutes().toString().length) + currentTime.getMinutes();
    var ss = '0'.repeat(2 - currentTime.getSeconds().toString().length) + currentTime.getSeconds();
    var date = day + "/" + month + "/" + year + " "+ hh + ":" + mm + ":" + ss;
    return date;

}

function windowSuccess(message, onConfirm) {
    var fClose = function () {
        modal.modal("hide");
    };
    var modal = $("#modal-success");
    modal.modal("show");
    $("#windowSuccessBody").empty().append(message);
    $("#windowSuccessOk").one('click', onConfirm);
    $("#windowSuccessOk").one('click', fClose);
}

function windowInfo(message, onConfirm) {
    var fClose = function () {
        modal.modal("hide");
    };
    var modal = $("#modal-info");
    modal.modal("show");
    $("#windowInfoBody").empty().append(message);
    $("#windowInfoOk").one('click', onConfirm);
    $("#windowInfoOk").one('click', fClose);
}

function windowConfirm(message, onConfirm) {
    var fClose = function () {
        modal.modal("hide");
    };
    var modal = $("#modal-warning");
    modal.modal("show");
    $("#windowConfirmMessage").empty().append(message);
    $("#windowConfirmOk").one('click', onConfirm);
    $("#windowConfirmOk").one('click', fClose);
    $("#windowConfirmCancel").one("click", fClose);
}

function windowError(message, onConfirm) {
    var fClose = function () {
        modal.modal("hide");
    };
    var modal = $("#modal-danger");
    modal.modal("show");
    $("#windowErrorMessage").empty().append(message);
    $("#windowErrorOk").one('click', onConfirm);
    $("#windowErrorOk").one('click', fClose);
}