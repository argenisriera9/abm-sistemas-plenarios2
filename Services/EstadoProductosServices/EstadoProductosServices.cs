﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.EstadoProductosServices
{
    public class EstadoProductosServices : IEstadoProductosServices
    {
        private readonly Conexion_BD db;

        public EstadoProductosServices(Conexion_BD _db)

        {
            db = _db;
        }

        public EstadoProductos GetById(int id)
        {
            var result = db.EstadoProductos.Find(id);
            return result;
        }

        public List<EstadoProductos> GetEstadoPro()
        {
            var result = db.EstadoProductos.ToList();
            return result;
        }

        public int InserEstadoPro(EstadoProductos estado)
        {
            var result = db.EstadoProductos.Add(estado);
            db.SaveChanges();
            return result.IdEstado;
        }
        public int delete(int id)
        {
            var result = db.EstadoProductos.Find(id);
            db.EstadoProductos.Remove(result);
            db.SaveChanges();
            return result.IdEstado;
        }

        public int Edit(EstadoProductos estado)
        {
            var result = db.EstadoProductos.FirstOrDefault(x => x.IdEstado == estado.IdEstado);
            result.IdEstado = estado.IdEstado;
            result.Nombre = estado.Nombre;
            db.SaveChanges();
            return result.IdEstado;
        }

    }
}
