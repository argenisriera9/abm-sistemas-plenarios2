﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.EstadoProductosServices
{
   public  interface IEstadoProductosServices
    {

        List<EstadoProductos> GetEstadoPro();
        EstadoProductos GetById(int id);
        int InserEstadoPro(EstadoProductos estado);
       

        int Edit(EstadoProductos estado);

        int delete(int id);
    }
}
