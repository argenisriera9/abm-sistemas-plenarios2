﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.MonedaServices
{
    public class MonedaServices : IMonedaServices
    {
        private readonly Conexion_BD db;
        public MonedaServices(Conexion_BD _db)
        {
            db = _db;
        }

        public List<Moneda> Get()
        {
           return db.Moneda.Where(x => x.Activo == 1).ToList();
        }

        public Moneda GetById(int id)
        {
            return db.Moneda.Find(id);
        }

        public int Insert(Moneda moneda)
        {
            var result = db.Moneda.Add(moneda);
            db.SaveChanges();
            return result.IdMoneda;
        }

    
        public int Edit(Moneda moneda)
        {
            var result = db.Moneda.FirstOrDefault(x => x.IdMoneda == moneda.IdMoneda);
            result.IdMoneda = moneda.IdMoneda;
            result.NombreMoneda = moneda.NombreMoneda;
            result.UsuarioModifico = moneda.UsuarioModifico;
            result.Abreviatura = moneda.Abreviatura;
            result.FechaModificacion = moneda.FechaModificacion;
            db.SaveChanges();
            return result.IdMoneda;
        }
        public int Delelte(int id)
        {
            var result = db.Moneda.FirstOrDefault(x=>x.IdMoneda==id);
            result.Activo = 0;
            db.SaveChanges();
            return result.IdMoneda;

        }
    }
}
