﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.MonedaServices
{
    public interface IMonedaServices
    {

        List<Moneda> Get();
        Moneda GetById(int id);
        int Insert(Moneda moneda);
        int Edit(Moneda moneda);

        int Delelte(int id);
    }
}
