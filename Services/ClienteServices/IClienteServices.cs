﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ClienteServices
{
    public interface IClienteServices
    {
        List<Clientes> GetClientes();
        Clientes GetClientesByID(int id);
        int  InsertarClientes(Clientes clientes);
        int Edit(Clientes clientes);

        int Delete(int id);

         Clientes ValidarCodigo(Clientes clientes);
    }
}
