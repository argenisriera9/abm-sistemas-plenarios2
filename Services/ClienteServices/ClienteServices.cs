﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ClienteServices
{
    public class ClienteServices : IClienteServices
    {
        private readonly Conexion_BD db;

        public ClienteServices(Conexion_BD _db)
        {
            db = _db;
        }

        public List<Clientes> GetClientes()
        {
            var result = db.Clientes.Where(x=>x.Activo==1).ToList();

            return result;
        }

        public Clientes GetClientesByID(int id)
        {
            var result = db.Clientes.Find(id);
            return result;
        }
        public int Edit(Clientes clientes)
        {
            var result = db.Clientes.FirstOrDefault(x => x.IdCliente == clientes.IdCliente);
            result.CodigoCliente = clientes.CodigoCliente;
            result.RazonSocial = clientes.RazonSocial;
            result.Telefono = clientes.Telefono;
            result.NumeroDocumento = clientes.NumeroDocumento;
            result.IdCliente = clientes.IdCliente;
            result.EmailCliente = clientes.EmailCliente;
            result.UsuarioEdito = clientes.UsuarioEdito;
           
            
            db.SaveChanges();
            return result.IdCliente;
        }
        public int Delete(int id)
        {
          
            var result = (from c in db.Clientes where c.IdCliente == id select c).FirstOrDefault();
            result.Activo = 0;
           
           
            db.SaveChanges();
            return result.IdCliente;
        }

        public int InsertarClientes(Clientes clientes)
        {
            db.Clientes.Add(clientes);
            db.SaveChanges();
            return clientes.IdCliente;
        }

        public Clientes ValidarCodigo(Clientes clientes)
        {
            int idempresa = clientes.IdEmpresa;
            if (clientes.IdCliente == 0)
            {
                var Cliente = (from c in db.Clientes
                               where c.Activo == 1 && c.IdEmpresa == idempresa && (c.NumeroDocumento == clientes.NumeroDocumento || c.RazonSocial == clientes.RazonSocial || c.CodigoCliente == clientes.CodigoCliente)
                               select c).FirstOrDefault();
                return Cliente;
            }
            else
            {
                var Cliente = (from c in db.Clientes
                               where c.Activo == 1 && c.IdCliente!=clientes.IdCliente && c.IdEmpresa == idempresa && (c.NumeroDocumento == clientes.NumeroDocumento || c.RazonSocial == clientes.RazonSocial || c.CodigoCliente == clientes.CodigoCliente)
                               select c).FirstOrDefault();
                return Cliente;
            }
           
                
        }
    }
}
