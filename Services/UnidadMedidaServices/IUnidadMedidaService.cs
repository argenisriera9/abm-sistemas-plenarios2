﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.UnidadMedidaServices
{
    public interface IUnidadMedidaService
    {
        List<UnidadMedida> Get();
        UnidadMedida GetById(int id);
        int Edit(UnidadMedida unidadMedida);
        int Insert(UnidadMedida unidadMedida);
        int delete(int id);
    }
}
