﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.UnidadMedidaServices
{
    public class UnidadMedidaService : IUnidadMedidaService
    {
        private readonly Conexion_BD db;
        public UnidadMedidaService(Conexion_BD _db)
        {
            db = _db;
        }
        public List<UnidadMedida> Get()
        {
            var result = db.unidadMedida.Where(x=>x.Activo==1).ToList();
            return result;
        }

        public UnidadMedida GetById(int id)
        {
            
            return db.unidadMedida.Find(id);
        }

        public int Insert(UnidadMedida unidadMedida)
        {

            var result = db.unidadMedida.Add(unidadMedida);
            db.SaveChanges();
            return result.IdUnidadMedida;
        }
        public int Edit(UnidadMedida unidadMedida)
        {
            var result = db.unidadMedida.FirstOrDefault(x => x.IdUnidadMedida == unidadMedida.IdUnidadMedida);
            result.IdUnidadMedida = unidadMedida.IdUnidadMedida;
            result.CodigoUnidadMedida = unidadMedida.CodigoUnidadMedida;
            result.abreviatura = unidadMedida.abreviatura;
            result.NombreUnidadMedida = unidadMedida.NombreUnidadMedida;
            db.SaveChanges();
            return result.IdUnidadMedida;
        }
        public int delete(int id)
        {
            var result = db.unidadMedida.FirstOrDefault(x => x.IdUnidadMedida == id);
            result.Activo = 0;
            db.SaveChanges();
            return result.IdUnidadMedida;
        }

       

    }
}
