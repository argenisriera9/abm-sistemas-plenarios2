﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.EstadoDespachoServices
{
    public interface IEstadoDespachoServices
    {

        List<EstadoDespacho> Get(int id);

        EstadoDespacho GeById(int id);

        int Edit(EstadoDespacho despacho);

        int Insert(EstadoDespacho despacho);

        int delete(int id);



    }
}
