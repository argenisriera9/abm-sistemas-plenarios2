﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.EstadoDespachoServices
{
    public class EstadoDespachoServices : IEstadoDespachoServices
    {

        private readonly Conexion_BD db;
        public EstadoDespachoServices(Conexion_BD _db)
        {
            db = _db;
        }

        public List<EstadoDespacho> Get(int id)
        {
           
            var result = db.EstadoDespacho.Include("Clientes").Include("EstadoProductos").Include("Productos").Where(x => x.IdUsuario == id).ToList();
            return result;
        }
        public EstadoDespacho GeById(int id)
        {

            var result = db.EstadoDespacho.Include("Clientes").Include("EstadoProductos").Include("Productos").FirstOrDefault(x=>x.IdEstadoDespacho==id);
            return result;
        }
        public int Insert(EstadoDespacho despacho)
        {
            var result = db.EstadoDespacho.Add(despacho);
            db.SaveChanges();
            return result.IdEstadoDespacho;
        }
        public int delete(int id)
        {
            var result = db.EstadoDespacho.Find(id);
            db.EstadoDespacho.Remove(result);
            db.SaveChanges();
            return result.IdEstadoDespacho;

        }

        public int Edit(EstadoDespacho despacho)
        {

            
                var result = db.EstadoDespacho.FirstOrDefault(x => x.IdEstadoDespacho == despacho.IdEstadoDespacho);
            
                result.IdCliente = despacho.IdCliente;
                result.IdProducto = despacho.IdProducto;
                result.IdEstado = despacho.IdEstado;
                result.Observacion = despacho.Observacion;
                result.Recibido = despacho.Recibido;
                result.Salida = despacho.Salida;
                result.IdEstadoDespacho = despacho.IdEstadoDespacho;
              
                db.SaveChanges();
                return despacho.IdEstadoDespacho;
           
              
            
        }

    }
}
