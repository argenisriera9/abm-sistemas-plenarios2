﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace Services.PersonasServices
{


    public class PersonasServices : IPersonasServices
    {
        private readonly Conexion_BD _db;

        public PersonasServices(Conexion_BD db)
        {
            _db = db;
        }

        public List<Personas> GetPersonas()
        {
            var personas = _db.Personas.ToList();
            return personas;

        }
        public int Insertar(Personas personas)
        {
            var insertar = _db.Personas.Add(personas);
            _db.SaveChanges();
            return insertar.PersonaID;
        }


        public Personas GetPersonasByID(int id)
        {
            var result = _db.Personas.Find(id);

            return result;
        }
        public List<Personas> GetPersonasByNombre(string nombre)
        {
            var result = (from c in _db.Personas where c.Nombre.Contains(nombre)
                         select c).ToList() ;



            return result;
        }

        public int Modificar(Personas persona)
        {
            var result = _db.Personas.FirstOrDefault(x => x.PersonaID == persona.PersonaID);
            result.Nombre = persona.Nombre;
            result.CreditoMaximo = persona.CreditoMaximo;
            result.FechaNacimiento = persona.FechaNacimiento;
            _db.SaveChanges();
            return persona.PersonaID;
        }
        public int delete(int id)
        {

            var delete= _db.Personas.Single(x => x.PersonaID == id);
            _db.Personas.Remove(delete);
            _db.SaveChanges();
           
            return delete.PersonaID;
            
        }

    }
}

