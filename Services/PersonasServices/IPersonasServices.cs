﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.PersonasServices
{
    public interface IPersonasServices
    {
        List<Personas> GetPersonas();
        Personas GetPersonasByID(int id);
        int Insertar(Personas personas);
        int Modificar(Personas  persona);
        List<Personas> GetPersonasByNombre(string nombre);
        int delete(int id);

    }
}
