﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ProductosServices
{
    public class ProductosServices : IProductoServices
    {

        private readonly Conexion_BD db;

        public ProductosServices(Conexion_BD _db)
        {
            db = _db;
        }
        public List<Productos> GetProductos()
        {
            var result = db.Productos.Where(x=>x.Activo==1).ToList();
            return result;
        }

        public Productos GetProductosByID(int id)
        {
            var result = db.Productos.Find(id);
            return result;
        }
        public Productos GetProductosByIdReq(int id)
        {
            var result = db.Productos.FirstOrDefault(x=>x.IdProducto ==id);
           
            return result;
        }


        public bool GetProductosByCodigo(string Codigo)
        {
            var result = (from p in db.Productos where p.CodigoProducto.Contains(Codigo) select p).ToList(); 
            if(result.Count== 0)
            {
             return false;
            }
            else
            {
                return true;
            }

            
                
                
        }

        public int InsertarPro(Productos productos)
        {
            var result = db.Productos.Add(productos);
            db.SaveChanges();
            return result.IdProducto;
        }

     

        public int Delete(int id)
        {
           var result = db.Productos.Find(id);
            db.Productos.Remove(result);
            db.SaveChanges();

            return result.IdProducto;
        }

        public int Edit(Productos productos)
        {
            var result = db.Productos.FirstOrDefault(x => x.IdProducto == productos.IdProducto);
            result.IdProducto = productos.IdProducto;
            result.NombreProducto = productos.NombreProducto;
            result.CodigoProducto = productos.CodigoProducto;
            db.SaveChanges();
            return result.IdProducto;
        }

       
    }
}
