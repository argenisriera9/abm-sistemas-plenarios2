﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ProductosServices
{
   public  interface IProductoServices
    {

        List<Productos> GetProductos();

        int InsertarPro(Productos productos);
        Productos GetProductosByID(int id);
        bool GetProductosByCodigo(string Codigo);
        int Edit(Productos productos);

        int Delete(int id);
        Productos GetProductosByIdReq(int id);

    }
}
