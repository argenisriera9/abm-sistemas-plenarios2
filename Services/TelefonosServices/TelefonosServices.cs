﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.TelefonosServices
{
    public class TelefonosServices : ITelefonosServices
    {

        private readonly Conexion_BD _db;

        public TelefonosServices(Conexion_BD db)
        {
            _db = db;
        }
        public List<Telefonos> GetTelefonos(int id)
        {
            var result = _db.Telefonos.Where(x => x.PersonaID == id).ToList();

            return result;
        }

        public int InsertarTel(Telefonos telefonos)
        {
            var result = _db.Telefonos.Add(telefonos);
            _db.SaveChanges();

            return result.TelefonoID;
        }

        public Telefonos GetById(int id)
        {
            var result = _db.Telefonos.Find(id);
           

            return result;
        }

        public int delete(int id)

        {
            var result = _db.Telefonos.Single(x => x.TelefonoID == id);
            _db.Telefonos.Remove(result);
            _db.SaveChanges();
            return result.TelefonoID;
        }

        public int ModificarTel(Telefonos telefonos)
        {
            var result = _db.Telefonos.FirstOrDefault(x=>x.TelefonoID==telefonos.TelefonoID);
            result.Telefono = telefonos.Telefono;
            _db.SaveChanges();
            return result.TelefonoID;

        }

       
    }
}
