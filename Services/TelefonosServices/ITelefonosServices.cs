﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.TelefonosServices
{
    public interface ITelefonosServices
    {

        List<Telefonos> GetTelefonos(int id);

        int InsertarTel(Telefonos telefonos);

        int delete(int id);

        Telefonos GetById(int id);

        int ModificarTel(Telefonos telefonos);
    }
}
