﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.EmpresasServices
{
    public class EmpresaServices : IEmpresaServices
    {
        private readonly Conexion_BD db;

        public EmpresaServices(Conexion_BD _db)
        {
            db = _db;
        }

        public List<Empresa> Get()
        {

            var result = db.Empresa.Where(x => x.Activo == 1).ToList();

            return result;
        }

        public Empresa GetById(int id)
        {
            var result = db.Empresa.Find(id);
            return result;
        }

        public bool GetByNombre(string Nombre)
        {
            var result = db.Empresa.Where(x => x.Nombre == Nombre).ToList();
            if(result.Count==0)
            {
                return false;
            }
            return true;
        }

        public int Insert(Empresa empresa)
        {
            
            empresa.Activo = 1;
            
            var result = db.Empresa.Add(empresa);
            db.SaveChanges();
            return result.IdEmpresa;
        }

        

        public int Edit(Empresa empresa)
        {
       
            var result = db.Empresa.FirstOrDefault(x => x.IdEmpresa == empresa.IdEmpresa);
            result.IdEmpresa = empresa.IdEmpresa;
            result.Nombre = empresa.Nombre;
            result.Telefono = empresa.Telefono;
            result.Web = empresa.Web;
            result.NumeroDocumento = empresa.NumeroDocumento;
            result.UsuarioEdito = empresa.UsuarioEdito;
            result.FechaModificacion = empresa.FechaModificacion;
            db.SaveChanges();
            return result.IdEmpresa;
            
        }

        public int detele(int id)
        {
            var result = db.Empresa.Find(id);
            db.Empresa.Remove(result);
            db.SaveChanges();
            return result.IdEmpresa;
        }
    }
}
