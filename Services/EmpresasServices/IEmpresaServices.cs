﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.EmpresasServices
{
   public interface IEmpresaServices
    {

        List<Empresa> Get();

        Empresa GetById(int id);
        bool  GetByNombre(string Nombre);
        int Insert(Empresa empresa);

        int Edit(Empresa empresa);

         int detele(int id);
    }
}
