﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ProveedoresServices
{
    public interface IProveedoresServices
    {
        List<Proveedor> Get();
        Proveedor GetById(int id);

        int Edit(Proveedor proveedor);

        int Insert(Proveedor provedor);

        int Delete(int id);

        Proveedor ValidarCodigo(Proveedor proveedor);
    }
}
