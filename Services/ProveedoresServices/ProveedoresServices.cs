﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ProveedoresServices
{
    public class ProveedoresServices : IProveedoresServices
    {

        private readonly Conexion_BD db;
        public ProveedoresServices(Conexion_BD _db)
        {
            db = _db;
        }
        public List<Proveedor> Get()
        {
            var result = db.Proveedores.ToList();
            return result;
        }

        public Proveedor GetById(int id)
        {
            var result = db.Proveedores.Find(id);
            return result;
        }

        public int Insert(Proveedor provedor)
        {
            var result = db.Proveedores.Add(provedor);
            db.SaveChanges();
            return result.IdProveedor;
        }

      
        public int Edit(Proveedor proveedor)
        {
            var result = db.Proveedores.FirstOrDefault(x => x.IdProveedor == proveedor.IdProveedor);
            result.IdProveedor = proveedor.IdProveedor;
            result.NombreProveedor = proveedor.NombreProveedor;
            result.Telefono = proveedor.Telefono;
            result.WebSite = proveedor.WebSite;
            result.NumeroDocumento = proveedor.NumeroDocumento;
            
            db.SaveChanges();
            return result.IdProveedor;
        }
        public int Delete(int id)
        {
            var result = db.Proveedores.Find(id);
            result.Activo = 0;
            db.SaveChanges();
            return result.IdProveedor;
        }

        public Proveedor ValidarCodigo(Proveedor proveedor)
        {
            int idempresa = proveedor.IdEmpresa;
            if (proveedor.IdProveedor == 0)
            {
                var Proveedor = (from c in db.Proveedores
                               where c.Activo == 1 && c.IdEmpresa == idempresa && (c.NumeroDocumento == proveedor.NumeroDocumento || c.NombreProveedor == proveedor.NombreProveedor || c.Codigo == proveedor.Codigo)
                               select c).FirstOrDefault();
                return Proveedor;
            }
            else
            {
                var Proveedor = (from c in db.Proveedores
                               where c.Activo == 1 && c.IdProveedor != proveedor.IdProveedor && c.IdEmpresa == idempresa && (c.NumeroDocumento == proveedor.NumeroDocumento || c.NombreProveedor == proveedor.NombreProveedor || c.Codigo == proveedor.Codigo)
                               select c).FirstOrDefault();
                return Proveedor;
            }


        }
    }
}
