﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.UsuariosServices
{
   public interface IUsuariosServices
    {

        List<Usuario> Get();

        Usuario GetById(int id);
        int Insert(Usuario usuario);

        int Edit(Usuario usuario);

        int delete(int id);
         
    }

}
