﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.UsuariosServices
{
    public class UsuariosServices : IUsuariosServices
    {
        private readonly Conexion_BD db;
        public UsuariosServices(Conexion_BD _db)
        {
            db=_db;
            
        }
        
        public List<Usuario> Get()
        {
            var result = db.Usuarios.Where(x=>x.Activo==1).ToList();
            return result;
        }

        public Usuario GetById(int id)
        {
            var result = db.Usuarios.Include("UsuarioRol").Where(x=>x.IdUsuario==id).FirstOrDefault();
            return result;
        }

        public int Insert(Usuario usuario)
        {
            var result = db.Usuarios.Add(usuario);
            db.SaveChanges();
            return result.IdUsuario;
        }
        public int delete(int id)
        {
            var inactivo = (from c in db.Usuarios where c.IdUsuario == id select c).FirstOrDefault();
            inactivo.Activo = 0;
           
            db.SaveChanges();
            return inactivo.IdUsuario;

        }

        public int Edit(Usuario usuario)
        {
           
            string fecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            var result = db.Usuarios.FirstOrDefault(x => x.IdUsuario == usuario.IdUsuario);
            result.IdUsuario = usuario.IdUsuario;
            result.Nombre = usuario.Nombre;
            result.Correo = usuario.Correo;
            result.Password = usuario.Password;
            result.Apellido = usuario.Apellido;
            result.IdRol = usuario.IdRol;
            result.UsuarioActivo = usuario.UsuarioActivo;
            result.FechaModificacion = Convert.ToDateTime(fecha);
            
            db.SaveChanges();
            return result.IdUsuario;
        }

    }
}
