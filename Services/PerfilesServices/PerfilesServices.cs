﻿using ConexionBD;
using Models;
using Services.PerfilesServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.PerfilesServices
{
    public class PerfilesServices : IPerfilesServices
    {
        public readonly Conexion_BD db;

        public PerfilesServices(Conexion_BD _db)
        {
            db= _db;
        }
        

        public List<UsuarioRol> Get()
        {
            var result = db.UsuarioRol.Where(x=>x.Activo==1).ToList();
            return result;
        }

        public UsuarioRol GetById(int id)
        {
            var result = db.UsuarioRol.Find(id);
            return result;
        }

        public int Insert(UsuarioRol usuarioRol)
        {
            var result = db.UsuarioRol.Add(usuarioRol);
            db.SaveChanges();
            return result.IdRol;
        }
        public int Delete(int id)
        {
            UsuarioRol usuarioRol = new UsuarioRol();

            var inactivo = (from c in db.UsuarioRol where c.IdRol == id select c).FirstOrDefault();
            inactivo.Activo = 0;

      
            db.SaveChanges();
            return inactivo.IdRol;
        }

        public int Edit(UsuarioRol usuarioRol)
        {
            var result = db.UsuarioRol.FirstOrDefault(x => x.IdRol == usuarioRol.IdRol);
            result.IdRol = usuarioRol.IdRol;
            result.NombreRol = usuarioRol.NombreRol;
            result.Activo = usuarioRol.Activo;
            db.SaveChanges();
            return result.IdRol;
           
        }
    }
}
