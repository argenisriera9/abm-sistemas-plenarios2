﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.PerfilesServices
{
    public interface IPerfilesServices
    {
        List<UsuarioRol> Get();
        UsuarioRol GetById(int id);

        int Insert(UsuarioRol usuarioRol);

        int Edit(UsuarioRol usuarioRol);

        int Delete(int id);

    }
}
