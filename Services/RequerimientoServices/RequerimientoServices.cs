﻿using ConexionBD;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.RequerimientoServices
{
    public class RequerimientoServices : IRequerimientoServices
    {
        private readonly Conexion_BD db;
        public RequerimientoServices(Conexion_BD _db)
        {
            db = _db;
        }
        public List<Requerimiento> Get()
        {
            var result = db.Requerimiento.Include("Proveedor").Include("Moneda").ToList();
            return result;
        }

        public Requerimiento GetById(int id)
        {
            return db.Requerimiento.Include("RequerimientoDet").Where(x => x.IdRequerimiento == id).FirstOrDefault();
        }

        public int Insert(Requerimiento requerimiento)
        {
            throw new NotImplementedException();
        }
       
        public int Edit(Requerimiento requerimiento)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }


    }
}
