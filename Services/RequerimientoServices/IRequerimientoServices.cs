﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.RequerimientoServices
{
    public interface IRequerimientoServices
    {

        List<Requerimiento> Get();

        Requerimiento GetById(int id);
        int Insert(Requerimiento requerimiento);

        int Edit(Requerimiento requerimiento);

        int Delete(int id);
    }
}
